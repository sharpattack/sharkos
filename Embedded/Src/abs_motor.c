#include "abs_motor.h"
#include "main.h"
#include "tmc/ic/TMC4671/TMC4671.h"
#include "tmc/ic/TMC6100/TMC6100.h"

#define ABS_N_MOTOR 2
#define ABS_N_MAX_STEP_MOTOR 10

#define STATE_NOTHING_TO_DO 0
#define STATE_START_INIT 1
#define STATE_WAIT_INIT_TIME 2
#define STATE_ESTIMATE_OFFSET 3

static uint8_t initState[ABS_N_MOTOR] = {STATE_START_INIT, STATE_START_INIT};
// static uint16_t initTime[ABS_N_MOTOR] = {0};    // Useless for mode 2
static int16_t hallPhi_e[ABS_N_MOTOR][2] = {0}; // indexed by motor index and old/new
static int16_t hallActualCoarseOffset[ABS_N_MOTOR];
static uint16_t lastPhiESelection[ABS_N_MOTOR];
static uint8_t UNUSED_PARAMETER = 0;

enum CalibrationType
{
    CALIBRATION_TYPE_OPEN_LOOP = 0,
    CALIBRATION_TYPE_HALL = 2
};

void abs_motor_init(uint8_t motor)
{

    // Motor setup
    if (motor == 0)
    {
        HAL_GPIO_WritePin(TMC_CTRL_0_GPIO_Port, TMC_CTRL_0_Pin, GPIO_PIN_SET); // 3.5Mbits/s
        HAL_GPIO_WritePin(TMC_DRV_0_GPIO_Port, TMC_DRV_0_Pin, GPIO_PIN_SET);
        HAL_GPIO_WritePin(TMC_EN_0_GPIO_Port, TMC_EN_0_Pin, GPIO_PIN_SET);
    }
    else if (motor == 1)
    {
        HAL_GPIO_WritePin(TMC_CTRL_1_GPIO_Port, TMC_CTRL_1_Pin, GPIO_PIN_SET);
        HAL_GPIO_WritePin(TMC_DRV_1_GPIO_Port, TMC_DRV_1_Pin, GPIO_PIN_SET);
        HAL_GPIO_WritePin(TMC_EN_1_GPIO_Port, TMC_EN_1_Pin, GPIO_PIN_SET);
    }

    tmc4671_setTargetTorque_raw(motor, 0);
    // full bridge
    tmc6100_writeInt(motor, 0, 0);
    // weak current gate drive
    tmc6100_writeInt(motor, 10, 0);

    // Motor type &  PWM configuration
    tmc4671_writeInt(motor, TMC4671_MOTOR_TYPE_N_POLE_PAIRS, 0x00030004);
    tmc4671_writeInt(motor, TMC4671_PWM_POLARITIES, 0x00000000);
    tmc4671_writeInt(motor, TMC4671_PWM_MAXCNT, 0x00000F9F);
    tmc4671_writeInt(motor, TMC4671_PWM_BBM_H_BBM_L, 0x00002828);
    tmc4671_writeInt(motor, TMC4671_PWM_SV_CHOP, 0x00000007);

    // ADC configuration //i0 i2 i1
    tmc4671_writeInt(motor, TMC4671_ADC_I_SELECT, 0x18000100);
    tmc4671_writeInt(motor, TMC4671_dsADC_MCFG_B_MCFG_A, 0x00100010);
    tmc4671_writeInt(motor, TMC4671_dsADC_MCLK_A, 0x20000000);
    tmc4671_writeInt(motor, TMC4671_dsADC_MCLK_B, 0x00000000);
    tmc4671_writeInt(motor, TMC4671_dsADC_MDEC_B_MDEC_A, 0x000004B0);

    tmc4671_writeInt(motor, TMC4671_ADC_I0_SCALE_OFFSET, 0x003C8105);
    tmc4671_writeInt(motor, TMC4671_ADC_I1_SCALE_OFFSET, 0x003C820F);

    // Digital hall settings
    tmc4671_writeInt(motor, TMC4671_HALL_MODE, 0x00001001);
    tmc4671_writeInt(motor, TMC4671_HALL_PHI_E_PHI_M_OFFSET, 0x55540000); // might be useless

    // ABN encoder settings
    tmc4671_writeInt(motor, TMC4671_ABN_DECODER_MODE, 0x00001000);
    tmc4671_writeInt(motor, TMC4671_ABN_DECODER_PPR, 0x00001000);
    tmc4671_writeInt(motor, TMC4671_ABN_DECODER_COUNT, 0); // decoder count
    tmc4671_writeInt(motor, TMC4671_ABN_DECODER_PHI_E_PHI_M_OFFSET, 0x00000000);

    // Limits
    tmc4671_writeInt(motor, TMC4671_PID_TORQUE_FLUX_LIMITS, 0x7FFF);

    // PI settings
    tmc4671_writeInt(motor, TMC4671_PID_TORQUE_P_TORQUE_I, 0x05DC1388);
    tmc4671_writeInt(motor, TMC4671_PID_FLUX_P_FLUX_I, 0x05DC1388);
    tmc4671_writeInt(motor, TMC4671_PID_TORQUE_FLUX_TARGET, 0x00000000);

    tmc4671_writeInt(motor, TMC4671_ABN_DECODER_COUNT, 0x00000000);

    // Feedback selection
    tmc4671_writeInt(motor, TMC4671_PHI_E_SELECTION, TMC4671_PHI_E_ABN);
    tmc4671_writeInt(motor, TMC4671_VELOCITY_SELECTION, TMC4671_VELOCITY_PHI_E_SELECTION); // Previously 9

    // Switch to torque mode
    tmc4671_writeInt(motor, TMC4671_MODE_RAMP_MODE_MOTION, 0x80000001);
    // Stop
    tmc4671_writeInt(motor, TMC4671_PID_TORQUE_FLUX_TARGET, 0x00000000);
}

void abs_motor_state_machine_step(uint8_t motor)
{
    /* Some of the values are useless since we will actually use CALIBRATION_TYPE_HALL (Mode 2) therefore some
     parameters like voltage, timing related are arbitrary and based on previous code
     In case you need some value:
     initWaitTime= 3000
     startVoltage = 0x00003A98
     */

    tmc4671_periodicJob(motor, HAL_GetTick(), CALIBRATION_TYPE_HALL, &initState[motor], (uint16_t)UNUSED_PARAMETER,
                        (uint16_t *)&UNUSED_PARAMETER, (uint16_t)UNUSED_PARAMETER, &hallPhi_e[motor][1],
                        &hallPhi_e[motor][0], &hallActualCoarseOffset[motor], &lastPhiESelection[motor],
                        (uint32_t *)&UNUSED_PARAMETER, (int16_t *)&UNUSED_PARAMETER);
}

void abs_motor_set_torque(uint8_t motor, float torque)
{
    tmc4671_setTargetTorque_raw(motor, (int32_t)torque);
}

void abs_motor_set_speed(uint8_t motor, float speed)
{
    tmc4671_setTargetVelocity(motor, (int32_t)speed);
}
