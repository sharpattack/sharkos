#include "abs_gpio.h"
#include "gpio.h"

void abs_gpio_write_pin(void *port, uint16_t pin, uint8_t state)
{
    HAL_GPIO_WritePin((GPIO_TypeDef *)port, pin, state);
}

bool abs_gpio_pin_start_match_interrupt(uint16_t pin)
{
    return pin == START_MATCH_INT_10_Pin;
}