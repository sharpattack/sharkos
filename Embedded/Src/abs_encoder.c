#include "abs_encoder.h"
#include "math.h"
#include "tim.h"

#define K_R ((float)2.0f * M_PI_2 / 4096.0f)

void abs_encoder_init(uint8_t encoder)
{
    if (encoder == 0)
    {
        HAL_TIM_Encoder_Start_IT(&htim2, TIM_CHANNEL_1);
        HAL_TIM_Encoder_Start_IT(&htim2, TIM_CHANNEL_2);
        TIM2->CNT = 0;
    }
    else
    {
        HAL_TIM_Encoder_Start_IT(&htim5, TIM_CHANNEL_1);
        HAL_TIM_Encoder_Start_IT(&htim5, TIM_CHANNEL_2);
        TIM5->CNT = 0;
    }
}

float abs_encoder_get_position(uint8_t encoder)
{
    if (encoder == 0)
    {
        return K_R * TIM2->CNT;
    }
    else
    {
        return K_R * TIM5->CNT;
    }
}

void abs_encoder_set_position(uint8_t encoder, float position)
{
    if (encoder == 0)
    {
        TIM2->CNT = position / K_R;
    }
    else
    {
        TIM5->CNT = position / K_R;
    }
}