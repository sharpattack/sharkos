#include "main.h"
#include "spi.h"
#include "tmc/ic/TMC4671/TMC4671.h"
#include "tmc/ic/TMC6100/TMC6100.h"

uint8_t tmc4671_readwriteByte(uint8_t motor, uint8_t data, uint8_t lastTransfer)
{
    uint8_t rx;
    HAL_StatusTypeDef status;

    if (motor == 0)
    {
        HAL_GPIO_WritePin(TMC_CTRL_0_GPIO_Port, TMC_CTRL_0_Pin, GPIO_PIN_RESET);
    }
    else
    {
        HAL_GPIO_WritePin(TMC_CTRL_1_GPIO_Port, TMC_CTRL_1_Pin, GPIO_PIN_RESET);
    }
    status = HAL_SPI_TransmitReceive(&hspi1, &data, &rx, 1, 10);
    if (status != HAL_OK)
    {
        asm("nop"); // TODO: Catch that
    }

    if (lastTransfer == true)
    {
        if (motor == 0)
        {
            HAL_GPIO_WritePin(TMC_CTRL_0_GPIO_Port, TMC_CTRL_0_Pin, GPIO_PIN_SET);
        }
        else
        {
            HAL_GPIO_WritePin(TMC_CTRL_1_GPIO_Port, TMC_CTRL_1_Pin, GPIO_PIN_SET);
        }
    }

    return rx;
}

uint8_t tmc6100_readwriteByte(uint8_t motor, uint8_t data, uint8_t lastTransfer)
{
    uint8_t rx;
    HAL_StatusTypeDef status;

    if (motor == 0)
    {
        HAL_GPIO_WritePin(TMC_DRV_0_GPIO_Port, TMC_DRV_0_Pin, GPIO_PIN_RESET);
    }
    else
    {
        HAL_GPIO_WritePin(TMC_DRV_1_GPIO_Port, TMC_DRV_1_Pin, GPIO_PIN_RESET);
    }
    status = HAL_SPI_TransmitReceive(&hspi1, &data, &rx, 1, 10);
    if (status != HAL_OK)
    {
        asm("nop"); // TODO: Catch that
    }

    if (lastTransfer == true)
    {
        if (motor == 0)
        {
            HAL_GPIO_WritePin(TMC_DRV_0_GPIO_Port, TMC_DRV_0_Pin, GPIO_PIN_SET);
        }
        else
        {
            HAL_GPIO_WritePin(TMC_DRV_1_GPIO_Port, TMC_DRV_1_Pin, GPIO_PIN_SET);
        }
    }

    return rx;
}

uint8_t tmc4671_isConnected(uint8_t motor)
{
    int32 whoAmI = 0x34363731; // 4671 (ascii) -> hexa
    if (tmc4671_readInt(motor, 0) == whoAmI)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}