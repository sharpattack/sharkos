#ifndef PORT_TMC_H
#define PORT_TMC_H
#include "stdint.h"

uint8_t tmc4671_readwriteByte(uint8_t motor, uint8_t data, uint8_t lastTransfer);
uint8_t tmc6100_readwriteByte(uint8_t motor, uint8_t data, uint8_t lastTransfer);
uint8_t tmc4671_isConnected(uint8_t motor);

#endif