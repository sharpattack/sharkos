[![Build Status](https://jenkins.madahindev.madadynns.ovh/buildStatus/icon?job=SharpAttack%2FSharkOS&style=flat-square)](https://jenkins.madahindev.madadynns.ovh/job/SharpAttack/job/SharkOS/)

# Présentation
Le but ici est juste de tester [MicroDDS](https://github.com/eProsima/Micro-XRCE-DDS-Client) tout en jouant un peu avec le FreeRTOS.

Les ports série suivant sont utilisés :
* USART3: Utilisé par MicroDDS.

Les tâches FreeRTOS suivantes sont définies :
* UART : Lis sur la sortie standard et renvoie iméditament ce qui a été lu.
* LED : Fait clignoter la LED LD2 à 1Hz

# Outils pour flasher

Par défaut, le projet s'attend à l'utilisation d'un JLINK, il est possible de convertir le logiciel interne des board nucléo en suivant ce [tutoriel](https://www.segger.com/products/debug-probes/j-link/models/other-j-links/st-link-on-board/) (uniqument sur windows)

# Auto formattage du code avant les commits

Si ce n'est pas déjà fait, installez l'outil pour les pre-commits

```bash
pip install pre-commit
pre-commit install
```

C'est tout.

# Compilation

Pour compiler :
```bash
cmake -B build -S . -GNinja
cmake --build build
```

Pour l'envoyer sur la board
```bash
cmake --build build -- flash
```

En cas d'erreur de permission refusée, il faut se donner les bonnes permissions (uniquement avec stlink)
```bash
sudo usermod -a -G dialout `whoami`
```

# Lire les logs

La lecture de logs n'est disponnible qu'avec le JLINK !

```bash
cmake --build build -- rtt
```

# Règle UDEV
Pour une expérience de développement optimum, créer le fichier `/etc/udev/rules.d/97-stm32.rules` avec ce contenu :
```udev
SUBSYSTEM=="tty", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374b", MODE="0660", SYMLINK+="nucleo-f446ze"
```

# Démarrer l'agent

```bash
# Terminal 1 : agent serial
MicroXRCEAgent serial -D /dev/nucleo-f446ze -d -b 921600 -v6
# Terminal 2 : agent udp
MicroXRCEAgent udp4 -p 8080 -d -v6
# Terminal 3 : hello subsrciber exemple
SubscribeHelloWorldClient 127.0.0.1 8080
```

Sauf si vous êtes sous MacOS ou que vous votez Pécress, vous pouvez tenter cette ligne pour l'agent DDS série.

```bash
MicroXRCEAgent serial -D /dev/cu.usbmodem1413403 -d -b 921600 -v6
```

# Montage
Pour accéder au port série UART1, il faut brancher un adaptateur sur les pins ci dessous. Les cercles de couleurs correspondent aux couleurs standards d'un adaptateur série.
![schema de montage de l'UART1](branchement_UART.png)

# Debug
Le logiciel [GNU screen](https://www.gnu.org/software/screen/) fonctionne particulièrement bien
pour lire le port de debug.
```bash
screen /dev/tty.usbserial-141320 115200
```
Astuce : Pour quitter screen, il faut faire `ctrl+A` puis écrire `:quit`


# Remote flashing

Pour flash en remote, il est possible d'utiliser la commande `ninja flash-remote`

Cette commande nécessite la définition des options `REMOTE_FLASH_USER` et `REMOTE_FLASH_ADDRESS` dans cmake

# Options de compilation pour CMAKE

Option                         | Défaut
-------------------------------|----------
CMAKE_BUILD_TYPE               | Release
DCMAKE_EXPORT_COMPILE_COMMANDS | OFF
LOG_LEVEL                      | 1 (info)
BUILD_DDS_PUBLISH_EXAMPLE      | NO
BUILD_DDS_SUBSCRIBE_EXAMPLE    | NO
REMOTE_FLASH_USER              | ""
REMOTE_FLASH_ADDRESS           | ""
FLASHER                        | JLINK (STLINK/JLINK)
CI_BUILD                       | NO
