#ifndef ABS_GPIO_H
#define ABS_GPIO_H
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

void abs_gpio_write_pin(void *port, uint16_t pin, uint8_t state);
bool abs_gpio_pin_start_match_interrupt(uint16_t pin);
#ifdef __cplusplus
}
#endif

#endif