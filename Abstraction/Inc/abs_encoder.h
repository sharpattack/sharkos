#ifndef ABS_ENCODER_H
#define ABS_ENCODER_H

#include "stdint.h"

#define ABS_N_ENCODER 2

#ifdef __cplusplus
extern "C"
{
#endif

void abs_encoder_init(uint8_t encoder);
float abs_encoder_get_position(uint8_t encoder);
void abs_encoder_set_position(uint8_t encoder, float position);

#ifdef __cplusplus
}
#endif

#endif