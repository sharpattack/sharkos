#ifndef ABS_MOTOR_H
#define ABS_MOTOR_H
#include "stdint.h"

#define ABS_N_MOTOR 2
#define ABS_N_MAX_STEP_MOTOR 10

#ifdef __cplusplus
extern "C"
{
#endif

    void abs_motor_init(uint8_t motor);
    void abs_motor_state_machine_step(uint8_t motor);
    void abs_motor_set_torque(uint8_t motor, float torque);
    void abs_motor_set_speed(uint8_t motor, float speed);
#ifdef __cplusplus
}
#endif

#endif