#ifndef ABS_TIMER_H
#define ABS_TIMER_H
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

void abs_timer_encoder_start_it(void *handler, uint32_t channel);
void abs_timer_base_start_it(void *handler, uint32_t channel);

#ifdef __cplusplus
}
#endif

#endif