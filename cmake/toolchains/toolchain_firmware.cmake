find_package(gcc-arm-none-eabi REQUIRED)

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

# specify cross compilers and tools
set(CMAKE_C_COMPILER ${ARM_GCC_PATH}/arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER ${ARM_GCC_PATH}/arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER ${ARM_GCC_PATH}/arm-none-eabi-gcc)
set(CMAKE_AR ${ARM_GCC_PATH}/arm-none-eabi-ar)
set(CMAKE_OBJCOPY ${ARM_GCC_PATH}/arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP ${ARM_GCC_PATH}/arm-none-eabi-objdump)
set(SIZE ${ARM_GCC_PATH}/arm-none-eabi-size)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(PLATFORM_NAME "LwIP")
set(CMAKE_BUILD_RPATH_USE_ORIGIN ON)
set(CMAKE_CXX_ARCHIVE_CREATE "<CMAKE_AR> -crD <TARGET> <LINK_FLAGS> <OBJECTS>")
set(CMAKE_CXX_ARCHIVE_APPEND "<CMAKE_AR> -rD <TARGET> <LINK_FLAGS> <OBJECTS>")
set(CMAKE_CXX_ARCHIVE_FINISH "<CMAKE_RANLIB> -D <TARGET>")

add_compile_definitions(ARM_MATH_CM4;ARM_MATH_MATRIX_CHECK;ARM_MATH_ROUNDING)
add_compile_options(-mfloat-abi=hard -mfpu=fpv4-sp-d16)
add_link_options(-mfloat-abi=hard -mfpu=fpv4-sp-d16)

add_compile_options(-mcpu=cortex-m4 -mthumb -mthumb-interwork)
add_compile_options(-ffunction-sections -fdata-sections -fno-common -fmessage-length=0)

# uncomment to mitigate c++17 absolute addresses warnings
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-register")

# compiler optimisation flag for bare metal
# See https://arobenko.github.io/bare_metal_cpp/ for details
add_compile_options(-nostdlib -fno-exceptions -fno-unwind-tables $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>)

add_link_options(-mcpu=cortex-m4 -mthumb -mthumb-interwork)

set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/STM32F446ZETX_FLASH.ld)

add_link_options(-T ${LINKER_SCRIPT})