#include "logs.h"

#include "portmacro.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_def.h"
#include "uavcan/sharp/network/Ready_1_0.h"
#include "uavcan/sharp/surimi/actions/MoveElevator_1_0.h"
#include "uavcan/sharp/surimi/elements/Elevator_1_0.h"
#include "uavcan/sharp/surimi/status/ElevatorStatus_1_0.h"

#include <Tasks/task_canard.h>
#include <Tasks/task_surimi.h>

#define ERROR_MARGIN 2
#define MAX_ELEVATORS 2

typedef enum
{
    Idle = uavcan_sharp_surimi_elements_Elevator_1_0_STATUS_IDLE,
    Working = uavcan_sharp_surimi_elements_Elevator_1_0_STATUS_WORKING,
    Stopped = uavcan_sharp_surimi_elements_Elevator_1_0_STATUS_STOPPED,
} ElevatorStatus;

bool initialized = false;
uavcan_sharp_surimi_elements_Elevator_1_0 elevators[MAX_ELEVATORS];

CANARD_PREPARE_SUBSCRIBE_FIXED_ID(uavcan_sharp_surimi_status_ElevatorStatus_1_0, 1);
CANARD_PREPARE_PUBLISH_FIXED_ID(uavcan_sharp_surimi_actions_MoveElevator_1_0);

bool Elevator_has_reached_position(Floor floor, int16_t heigth_mm)
{
    if (!initialized)
    {
        return false;
    }

    if (floor >= MAX_ELEVATORS)
    {
        return false;
    }
    uavcan_sharp_surimi_elements_Elevator_1_0 elevator = elevators[floor];
    if (elevator.height_mm == heigth_mm)
    {
        return true;
    }
    if (heigth_mm + ERROR_MARGIN >= elevator.height_mm && heigth_mm - ERROR_MARGIN <= elevator.height_mm)
    {
        if (elevator.status == Idle)
        {
            return true;
        }
    }
    return false;
}

BaseType_t Elevator_move_to(Floor floor, int16_t heigth_mm, TickType_t timeout)
{
    TickType_t start_time = HAL_GetTick();
    while (!initialized)
    {
        if (HAL_GetTick() - start_time > timeout)
        {
            return HAL_TIMEOUT;
        }
    }

    uavcan_sharp_surimi_actions_MoveElevator_1_0 action = {
        .floor = floor,
        .height_mm = heigth_mm,
    };
    CANARD_PUBLISH_MULTICAST_FIXED_ID(uavcan_sharp_surimi_actions_MoveElevator_1_0, CanardPriorityNominal, action);

    while (!Elevator_has_reached_position(floor, heigth_mm))
    {
        if (HAL_GetTick() - start_time > timeout)
        {
            return HAL_TIMEOUT;
        }
        sleep(1);
    }
    return HAL_OK;
}

_Noreturn void StartSurimiTask(void *parameters)
{
    (void)parameters;
    for (uint8_t i = 0; i < MAX_ELEVATORS; i++)
    {
        elevators[i].height_mm = 0;
        elevators[i].floor = i + 1;
        elevators[i].status = Idle;
    }

    CANARD_SUBSCRIBE_FIXED_ID(uavcan_sharp_surimi_status_ElevatorStatus_1_0);
    QueueHandle_t status = CANARD_GET_QUEUE_FIXED_ID(uavcan_sharp_surimi_status_ElevatorStatus_1_0);

    if (wait_for_feature(uavcan_sharp_network_Ready_1_0_FEATURE_ELEVATOR, 5000) != HAL_OK)
    {
        LOG_ERROR("Elevator is not responding, exiting task")
        for (;;)
        {
            vTaskDelete(NULL);
        }
    }

    LOG_INFO("Surimi task initialized")
    initialized = true;

    for (;;)
    {
        /* Update elevators position */
        uavcan_sharp_surimi_status_ElevatorStatus_1_0 current_status = {};
        while (xQueueReceive(status, &current_status, 0))
        {
            if (current_status.registered > MAX_ELEVATORS)
            {
                LOG_ERROR("%d elevators registered, max is %d", current_status.registered, MAX_ELEVATORS);
                continue;
            }
            for (uint8_t i = 0; i < current_status.registered; i++)
            {
                LOG_INFO("Elevator %d at %dmm", current_status.elevators[i].floor,
                         current_status.elevators[i].height_mm)
                elevators[i].height_mm = current_status.elevators[i].height_mm;
                elevators[i].status = current_status.elevators[i].status;
            }
        }
        sleep(100);
    }
}
