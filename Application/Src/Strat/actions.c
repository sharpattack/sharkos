#include "Strat/actions.h"
#include "logs.h"
#include "profile.h"
#include "shared_state.h"
#include "Strat/move.h"
#include <math.h>

uint16_t ACTIONS_NB = 0;
Action ACTIONS[ACTION_MAX];

void actions_add(char *name, float duration_s, uint16_t reward, Result (*func)(Side), bool critical)
{
    ACTIONS[ACTIONS_NB].name = name;
    ACTIONS[ACTIONS_NB].duration_s = duration_s;
    ACTIONS[ACTIONS_NB].reward = reward;
    ACTIONS[ACTIONS_NB].func = func;
    ACTIONS[ACTIONS_NB].performed = false;
    ACTIONS[ACTIONS_NB].tries_nb = 0;
    ACTIONS[ACTIONS_NB].critical = critical;

    ACTIONS_NB++;
}

Result calibration_distance_action(Side side)
{
    TRY_MOVE(move_to(0.0, 0.15, 20000));
    TRY_MOVE(move_to(0.0, 0.0, 20000));
    TRY_MOVE(move_to(0.0, 0.30, 20000));
    TRY_MOVE(move_to(0.0, 0.0, 20000));
    TRY_MOVE(move_to(0.0, 0.45, 20000));
    TRY_MOVE(move_to(0.0, 0.0, 20000));

    return ACTION_SUCCESS;
}

Result calibration_rotation_action(Side side)
{
    TRY_MOVE(rotate_of(M_PI / 6.0f, 10000));
    TRY_MOVE(rotate_of(-M_PI / 6.0f, 10000));
    TRY_MOVE(rotate_of(M_PI / 4.0f, 10000));
    TRY_MOVE(rotate_of(-M_PI / 4.0f, 10000));
    TRY_MOVE(rotate_of(M_PI / 2.0f, 10000));
    TRY_MOVE(rotate_of(-M_PI / 2.0f, 10000));
    TRY_MOVE(rotate_of(M_PI, 10000));
    TRY_MOVE(rotate_of(-M_PI, 10000));

    return ACTION_SUCCESS;
}

Result debug_action(Side side)
{
    TRY_MOVE(push_collect(30000));
    TRY_MOVE(place_garden_plant(20000));
    // TRY_MOVE(move_to(0.3, 1.5, 10000));
    // TRY_MOVE(move_to(0, 0, 10000));
    /* TRY_MOVE(take_plant(20000)); */
    // TRY_MOVE(take_plant(200000));
    return ACTION_SUCCESS;
}

Result tunning_action(Side side)
{
    TRY_MOVE(move_to(-0.0, .1, 10000));
    TRY_MOVE(move_to(1.0, .1, 10000));
    TRY_MOVE(move_to(1.0, 0.4, 10000));
    TRY_MOVE(move_to(.0, .0, 10000));
    TRY_MOVE(move_to(.5, .5, 10000));
    TRY_MOVE(move_to(.4, .3, 10000));
    TRY_MOVE(move_to(1.0, .3, 10000));
    TRY_MOVE(move_to(.1, .4, 10000));
    TRY_MOVE(move_to(.4, .5, 10000));
    TRY_MOVE(move_to(.0, .0, 10000));
    TRY_MOVE(rotate_to(.0, 10000));

    // TRY_MOVE(move_to(0.0, -0.0, 10000));
    // TRY_MOVE(rotate_of(M_PI, 800000));
    // TRY_MOVE(rotate_of(-M_PI, 800000));
    // TRY_MOVE(rotate_of(2*M_PI, 10000));
    // TRY_MOVE(rotate_of(2*M_PI, 10000));
    // TRY_MOVE(rotate_of(2*M_PI, 10000));
    // TRY_MOVE(rotate_of(2*M_PI, 10000));
    return ACTION_SUCCESS;
}

Result SOLAR1(Side side)
{

    if (side == BLUE_SIDE)
    {
        TRY_MOVE(rotate_solar_panel_blue(10000));
        TRY_MOVE(move_to(0.000, -0.225, 10000));
        TRY_MOVE(rotate_solar_panel_blue(10000));
        TRY_MOVE(move_to(0.000, -0.450, 10000));
        TRY_MOVE(rotate_solar_panel_blue(10000));
    }
    else
    {
        TRY_MOVE(move_to(-0.000, 0.025, 10000));
        TRY_MOVE(rotate_solar_panel_yellow(10000));
        TRY_MOVE(move_to(-0.000, 0.252, 10000));
        TRY_MOVE(rotate_solar_panel_yellow(10000));
        TRY_MOVE(move_to(-0.001, 0.477, 10000));
        TRY_MOVE(rotate_solar_panel_yellow(10000));
    }
    return ACTION_SUCCESS;
}

Result SOLAR2(Side side)
{

    if (side == BLUE_SIDE)
    {
        TRY_MOVE(move_to(0.000, -1.000, 10000));
        TRY_MOVE(rotate_solar_panel_blue(10000));
        TRY_MOVE(move_to(0.000, -1.220, 10000));
        TRY_MOVE(rotate_solar_panel_blue(10000));
        TRY_MOVE(move_to(0.000, -1.445, 10000));
        TRY_MOVE(rotate_solar_panel_blue(10000));
    }
    else
    {
        TRY_MOVE(move_to_safe(-0.000, 1.027, 10000));
        TRY_MOVE(rotate_solar_panel_yellow(10000));
        TRY_MOVE(move_to_safe(-0.000, 1.252, 10000));
        TRY_MOVE(rotate_solar_panel_yellow(10000));
        TRY_MOVE(move_to_safe(-0.000, 1.477, 10000));
        TRY_MOVE(rotate_solar_panel_yellow(10000));
    }
    return ACTION_SUCCESS;
}

Result COLLECT_PLANTS(Side side)
{
    if (side == BLUE_SIDE)
    {
        TRY_MOVE(move_to(0.137, -0.975, 10000));
        TRY_MOVE(rotate_to(5.497, 10000));
        TRY_MOVE(move_to(1.367, -0.123, 10000));
        TRY_MOVE(push_collect(15000));
    }
    else
    {
        TRY_MOVE(move_to(0.047, 1.849, 10000));
        TRY_MOVE(move_to(0.104, 1.050, 10000));
        TRY_MOVE(move_to(1.346, 0.155, 10000));
        TRY_MOVE(push_collect(15000));
    }

    return ACTION_SUCCESS;
}

Result PUT_PLANTS_IN_GARDEN(Side side)
{
    if (side == BLUE_SIDE)
    {
        TRY_MOVE(move_to(1.408, -0.480, 10000));
        TRY_MOVE(rotate_to(4.710, 10000));
        TRY_MOVE(move_to(1.617, -0.482, 10000));
        TRY_MOVE(place_garden_plant(15000));
    }
    else
    {
        TRY_MOVE(move_to(1.369, 0.498, 10000));
        TRY_MOVE(rotate_to(4.710, 10000));
        TRY_MOVE(move_to(1.616, 0.495, 10000));
        TRY_MOVE(place_garden_plant(15000));
    }
}

Result BACK_TO_BASE(Side side)
{
    if (side == BLUE_SIDE)
    {
        TRY_MOVE(move_to(1.434, -0.480, 10000));
        TRY_MOVE(move_to(1.105, -0.340, 10000));
        TRY_MOVE(move_to(0.786, -2.303, 10000));
    }
    else
    {
        TRY_MOVE(move_to(1.414, 0.499, 10000));
        TRY_MOVE(move_to(1.166, 0.402, 10000));
        TRY_MOVE(move_to(0.683, 2.351, 10000));
    }

    return ACTION_SUCCESS;
}

void actions_init()
{
#ifdef PROFILE_DEBUG_STRAT
    ACTION_REGISTER(debug_action, 20, 10, false);
#else
    ACTION_REGISTER(SOLAR1, 20, 15, false);
    ACTION_REGISTER(SOLAR2, 20, 15, false);
    ACTION_REGISTER(COLLECT_PLANTS, 15, 3, false);
    ACTION_REGISTER(PUT_PLANTS_IN_GARDEN, 15, 8, false);
    ACTION_REGISTER(BACK_TO_BASE, 5, 13, true);
    /* ACTION_REGISTER(debug_action, 20, 15, true); */
#endif
}
