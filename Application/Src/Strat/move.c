#include "Strat/move.h"
#include "Control/message.h"
#include "FreeRTOS.h"
#include "Strat/actions.h"
#include "Tasks/task_ctrl.h"
#include "Tasks/task_dds.h"
#include "Vision/vision.h"
#include "logs.h"
#include "portmacro.h"
#include "stm32f4xx_hal.h"
#include "tmc/helpers/Types.h"
#include "vision_objects.h"
#include <math.h>
#include <portmacro.h>
#include <projdefs.h>
#include <stdint.h>
#include <stm32f4xx_hal_def.h>
#include <string.h>

#define FLOOR1_MAX_HEIGHT_MM 135
#define FLOOR2_MAX_HEIGHT_MM 171

Result move_to_safe(float target_x, float target_y, TickType_t timeout)
{

    const uint32_t t0 = HAL_GetTick();
    const Result res = rotate_toward_safe(target_x, target_y, timeout);
    if (res != ACTION_SUCCESS)
    {
        return res;
    }
    const uint32_t rotate_toward_duration = HAL_GetTick() - t0;

    ctrl_move_to(target_x, target_y, SAFE_MODE);
    const bool success = ctrl_wait_move_completed(timeout - rotate_toward_duration);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("move timeout during rotate_to: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result move_to(float target_x, float target_y, TickType_t timeout)
{

    const uint32_t t0 = HAL_GetTick();
    const Result res = rotate_toward(target_x, target_y, timeout);
    if (res != ACTION_SUCCESS)
    {
        return res;
    }
    const uint32_t rotate_toward_duration = HAL_GetTick() - t0;

    ctrl_move_to(target_x, target_y, NORMAL_MODE);
    const bool success = ctrl_wait_move_completed(timeout - rotate_toward_duration);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("move timeout during rotate_to: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result move_of_safe(float target_x, float target_y, TickType_t timeout)
{

    ctrl_move_of(target_x, target_y, SAFE_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("move timeout during rotate_to_safe: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result move_of(float target_x, float target_y, TickType_t timeout)
{

    ctrl_move_of(target_x, target_y, NORMAL_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("move timeout during rotate_to: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result rotate_to_safe(float target_theta, TickType_t timeout)
{

    ctrl_rotate_to(target_theta, SAFE_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("rotate timeout during rotate_to: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result rotate_to(float target_theta, TickType_t timeout)
{

    ctrl_rotate_to(target_theta, NORMAL_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("rotate timeout during rotate_to: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result rotate_of_safe(float target_theta, TickType_t timeout)
{

    ctrl_rotate_of(target_theta, SAFE_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("rotate timeout during rotate_of_safe: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result rotate_of(float target_theta, TickType_t timeout)
{

    ctrl_rotate_of(target_theta, NORMAL_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("rotate timeout during rotate_of: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result rotate_toward_safe(float x, float y, TickType_t timeout)
{

    ctrl_rotate_toward(x, y, SAFE_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("rotate timeout during rotate_toward_safe: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result rotate_toward(float x, float y, TickType_t timeout)
{

    ctrl_rotate_toward(x, y, NORMAL_MODE);
    const bool success = ctrl_wait_move_completed(timeout);
    if (!success)
    {
        ctrl_abort();
        LOG_WARNING("rotate timeout during rotate_toward: abort");
        ctrl_wait_move_completed(WAIT_ABORT);
        return ACTION_ABORT;
    }

    return ACTION_SUCCESS;
}

Result push_collect(TickType_t timeout)
{
    /* Claws, solar panel and elevator are unavailable without canopen */
    return ACTION_SUCCESS;
}

Result take_plant(TickType_t timeout)
{
    /* Claws, solar panel and elevator are unavailable without canopen */
    return ACTION_SUCCESS;
}

Result place_garden_plant(TickType_t timeout)
{
    /* Claws, solar panel and elevator are unavailable without canopen */
    return ACTION_SUCCESS;
}

Result rotate_solar_panel_blue(TickType_t timeout)
{
    /* Claws, solar panel and elevator are unavailable without canopen */
}

Result rotate_solar_panel_yellow(TickType_t timeout)
{
    /* Claws, solar panel and elevator are unavailable without canopen */
}

Result up_floor1_elevator(TickType_t timeout)
{
    /* Claws, solar panel and elevator are unavailable without canopen */
}
