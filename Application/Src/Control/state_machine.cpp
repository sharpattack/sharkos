#include "Control//ctrlr.h"
#include "Tasks/task_ctrl.h"
#include "abs_gpio.h"
#include "event_groups.h"
#include "logs.h"
#include "main.h"
#include "shared_state.h"
#include <cmath>
#include <etl/message.h>

namespace NSCtrlr
{

void CtrlStateMachine::log_unknown_event(const etl::fsm_state_id_t state, const etl::imessage &msg) const
{
    LOG_WARNING("Received unknown event %d from state %d", state, msg.get_message_id());
}

void CtrlStateMachine::notify_success()
{
    xEventGroupSetBits(this->ctrl_event_handle, CTRL_MOVE_COMPLETED_BIT);
}

bool CtrlStateMachine::is_object_close() const
{
    bool bAnObjectIsClose = false;

    // * Time-of-flights
    constexpr float tof_limit = 0.17f;

    for (const auto &sensor : this->shared_state->proximity_sensors.sensor)
    {
        if (!sensor.is_available)
        {
            continue;
        }
        if (sensor.distance <= tof_limit)
        {
            // one of the tof detected an object
            bAnObjectIsClose = true;
            break;
        }
    }

    // * Lidar
    if (!bAnObjectIsClose)
    {
        bAnObjectIsClose = get_shared_lidar_sensor().an_object_is_close;
    }

    // * Default : no sensors have seen a close object
    return bAnObjectIsClose;
}

bool CtrlStateMachine::lidar_says_object_is_in_front() const
{
    LidarSensor lidar_sensor = get_shared_lidar_sensor();
    return lidar_sensor.proximity[0] <= 0.4 or lidar_sensor.proximity[7] <= 0.4;
}

bool CtrlStateMachine::lidar_says_object_is_behind() const
{
    LidarSensor lidar_sensor = get_shared_lidar_sensor();
    return lidar_sensor.proximity[3] <= 0.4 or lidar_sensor.proximity[4] <= 0.4;
}

// Init

etl::fsm_state_id_t Init::on_enter_state()
{
    abs_gpio_write_pin(LED_DEBUG_2_GPIO_Port, LED_DEBUG_2_Pin, GPIO_PIN_RESET);
    abs_gpio_write_pin(LED_DEBUG_3_GPIO_Port, LED_DEBUG_3_Pin, GPIO_PIN_SET);

    return StateId::INIT;
}

etl::fsm_state_id_t Init::on_event_unknown(const etl::imessage &msg)
{
    this->get_fsm_context().log_unknown_event(this->STATE_ID, msg);
    return StateId::INIT;
}

etl::fsm_state_id_t Init::on_event(const Step &event)
{
    auto &ctrlr = this->get_fsm_context().ctrlr;
    ctrlr.m_output.u_1 = 0;
    ctrlr.m_output.u_2 = 0;

    // Wait for the match to start
    const bool match_started =
        xEventGroupGetBits(this->get_fsm_context().ctrl_event_handle) & (1 << CTRL_MATCH_START_BIT);
    if (!match_started)
    {
        return StateId::INIT;
    }

    abs_gpio_write_pin(LED_DEBUG_3_GPIO_Port, LED_DEBUG_3_Pin, GPIO_PIN_RESET);
    ctrlr.reset(); // DEBUG for calib
    TIM2->CNT = 0;
    TIM5->CNT = 0;

    return StateId::IDLE;
}

// Idle

etl::fsm_state_id_t Idle::on_event(const Step &event)
{
    auto &ctrlr = this->get_fsm_context().ctrlr;
    ctrlr.step();
    return StateId::IDLE;
}

etl::fsm_state_id_t Idle::on_event(const Move &event) const
{
    return StateId::MOVING;
}

etl::fsm_state_id_t Idle::on_event(const Rotate &event) const
{
    return StateId::ROTATING;
}

etl::fsm_state_id_t Idle::on_event(const Abort &event) const
{
    return StateId::IDLE;
}

etl::fsm_state_id_t Idle::on_event_unknown(const etl::imessage &msg)
{
    this->get_fsm_context().log_unknown_event(this->STATE_ID, msg);
    return StateId::IDLE;
}

// Moving

etl::fsm_state_id_t Moving::on_enter_state()
{
    return StateId::MOVING;
}

void Moving::init_scurve(float requested_x, float requested_y)
{
    auto &ctrlr = this->get_fsm_context().ctrlr;

    etl::array<float, 2> pos = {ctrlr.m_pose.x, ctrlr.m_pose.y};
    etl::array<float, 2> requested = {requested_x, requested_y};

    this->scurve = SCurve<2>(ctrlr.m_ts, ctrlr.m_phy.xDot_max, ctrlr.m_phy.xDotDot_max, 2e-2f);

    this->scurve.init(pos, requested, ctrlr.m_time);
    this->is_angle_control_disabled = false;
}

etl::fsm_state_id_t Moving::on_event(const SetMoveTarget &event)
{
    this->init_scurve(event.requested_x, event.requested_y);
    this->is_angle_control_disabled = false;

    return StateId::MOVING;
}

etl::fsm_state_id_t Moving::on_event(const Step &event)
{
    auto &ctrlr = this->get_fsm_context().ctrlr;
    ctrlr.step();
    etl::array<float, 2> pose = {ctrlr.m_pose.x, ctrlr.m_pose.y};

    const bool is_stable = this->scurve.isStable(pose, ctrlr.m_time);
    if (is_stable)
    {
        this->get_fsm_context().notify_success();
        return StateId::IDLE;
    }

    if ((ctrlr.m_direction == Forward and this->get_fsm_context().lidar_says_object_is_in_front()) or
        (ctrlr.m_direction == Backward and this->get_fsm_context().lidar_says_object_is_behind()))
    {
        HAL_GPIO_WritePin(LED_DEBUG_3_GPIO_Port, LED_DEBUG_3_Pin, GPIO_PIN_SET);
        etl::array<float, 2> ref_pose = this->scurve.getTarget();
        this->init_scurve(ref_pose[0], ref_pose[1]);
        return StateId::MOVING;
    }
    HAL_GPIO_WritePin(LED_DEBUG_3_GPIO_Port, LED_DEBUG_3_Pin, GPIO_PIN_RESET);

    this->scurve.step(ctrlr.m_time);

    etl::array<float, 2> ref_pose = this->scurve.getOutput();
    ctrlr.m_ref_pose.x = ref_pose[0];
    ctrlr.m_ref_pose.y = ref_pose[1];

    const float eps_x = ctrlr.m_ref_pose.x - ctrlr.m_pose.x;
    const float eps_y = ctrlr.m_ref_pose.y - ctrlr.m_pose.y;
    const float eps_r = sqrtf(eps_x * eps_x + eps_y * eps_y);
    if (eps_r > 1e-2f && !this->is_angle_control_disabled)
    {
        ctrlr.m_ref_pose.theta = ctrlr.computeReference(ctrlr.m_ref_pose.x, ctrlr.m_ref_pose.y);
    }
    else if (eps_r <= 1e-2f)
    {
        this->is_angle_control_disabled = true;
    }

    HAL_GPIO_WritePin(LED_DEBUG_2_GPIO_Port, LED_DEBUG_2_Pin, GPIO_PIN_RESET); // Debug LED

    return StateId::MOVING;
}

etl::fsm_state_id_t Moving::on_event(const Abort &event)
{
    this->get_fsm_context().notify_success(); // Todo faut pas faire ça
    return StateId::IDLE;
}

etl::fsm_state_id_t Moving::on_event_unknown(const etl::imessage &msg)
{
    this->get_fsm_context().log_unknown_event(this->STATE_ID, msg);
    return StateId::MOVING;
}

// Rotating

etl::fsm_state_id_t Rotating::on_enter_state()
{
    return StateId::ROTATING;
}

etl::fsm_state_id_t Rotating::on_event(const SetRotationTarget &event)
{
    this->init_scurve(event.requested_theta);
    return StateId::ROTATING;
}

void Rotating::init_scurve(float requested_theta)
{
    auto &ctrlr = this->get_fsm_context().ctrlr;
    etl::array<float, 1> theta = {ctrlr.m_pose.theta};
    etl::array<float, 1> req_theta = {requested_theta};

    this->scurve = SCurve<1>(ctrlr.m_ts, ctrlr.m_phy.w_max, ctrlr.m_phy.wDot_max, 4e-2); // 2 deg to target
    this->scurve.init(theta, req_theta, ctrlr.m_time);
}

etl::fsm_state_id_t Rotating::on_event(const Step &event)
{
    auto &ctrlr = this->get_fsm_context().ctrlr;
    ctrlr.step();

    etl::array<float, 1> theta = {ctrlr.m_pose.theta};
    if (scurve.isStable(theta, ctrlr.m_time))
    {
        this->get_fsm_context().notify_success();
        return StateId::IDLE;
    }

    scurve.step(ctrlr.m_time);
    ctrlr.m_ref_pose.theta = scurve.getOutput()[0];

    HAL_GPIO_WritePin(LED_DEBUG_2_GPIO_Port, LED_DEBUG_2_Pin, GPIO_PIN_RESET); // Debug LED

    return StateId::ROTATING;
}

etl::fsm_state_id_t Rotating::on_event(const Abort &event)
{
    this->get_fsm_context().notify_success(); // Todo faut pas faire ça
    return StateId::IDLE;
}

etl::fsm_state_id_t Rotating::on_event_unknown(const etl::imessage &msg)
{
    this->get_fsm_context().log_unknown_event(this->STATE_ID, msg);
    return StateId::ROTATING;
}

} // namespace NSCtrlr
