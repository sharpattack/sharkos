#include <cmath>

#include "Control/ctrlr.h"
#include "Control/kinematics.h"
#include "Control/pid.h"
#include "logs.h"
#include "odometry.h"
#include "queue.h"

void _putchar(char c)
{
    SEGGER_RTT_Write(LOGS_RTT_CHANNEL, &c, sizeof(c));
}

extern uint8_t g_poseUpdateInitLatch;

using namespace Kinematics;

/**
 * @brief Constrain the input torque in the range [-max, max]. Return the
 * constrained torque.
 *
 * @param T [Nm]
 * @param max [Nm]
 * @return float [Nm]
 */
float torqueSaturation(float T, float max)
{
    if (T < -max)
    {
        return -max;
    }
    else if (T > max)
    {
        return max;
    }
    else
    {
        return T;
    }
}

namespace NSCtrlr
{

Ctrlr::Ctrlr(float Ts, QueueHandle_t dds_queue)
    : m_time{0}, m_timeout{0}, m_pose{}, m_poseDot{}, m_ref_pose{}, m_output{}, m_direction{},
      // Chill PID m_pidAngle(Ts, 1.2f, 0.8f, 0.04f, 10.0f),
      m_phy{DEFAULT_PHY},
      m_pidAngle(Ts, 1.95, 1.4, 0.034, 2.0f), // I don't want default constructor so that's the price to pay
      m_pidDist(Ts, 2.2f, 1.8f, 0.1f, 3.0f)
{
    this->dds_queue = dds_queue;

    m_ts = Ts;
    m_pidAngle.setAntiWindupScheme(ANTI_WINDUP_SCHEME_CONDITIONAL_INTEGRATION);
    m_pidDist.setAntiWindupScheme(ANTI_WINDUP_SCHEME_CONDITIONAL_INTEGRATION);
    m_pose = {0, 0, 0};
    m_ref_pose = m_pose;
    m_output = {0, 0};
    m_isStable = false;

    m_isRotationEnabled = true;
    m_isTranslationEnabled = true;
}

void Ctrlr::reset()
{
    m_pidAngle.reset();
    m_pidDist.reset();
    m_ref_pose = {0.f, 0.f, 0.f};
    m_pose = {0.f, 0.f, 0.f};
    m_output = {0.f, 0.f};
    m_time = 0.f;
    m_isStable = false;
    g_poseUpdateInitLatch = 0;
}

void Ctrlr::step()
{
    // See control calculation note
    /** Convert inputs into references **/
    // float x0 = m_pose.x, y0 = m_pose.y, theta0 = m_pose.theta;
    updatePose(m_pose, m_poseDot);

    // publish pos estim to DDS
    // NO
    // OdomPoseEstim poseEstim = {.x = m_pose.x,
    //                            .y = m_pose.y,
    //                            .theta = m_pose.theta,
    //                            .xDot = m_poseDot.x,
    //                            .yDot = m_poseDot.y,
    //                            .thetaDot = m_poseDot.theta};
    // xQueueSend(this->dds_queue, &poseEstim, 0);

    /** Compute motor command **/
    float eps_x = m_ref_pose.x - m_pose.x;
    float eps_y = m_ref_pose.y - m_pose.y;
    float eps_r_st = -sinf(m_pose.theta) * eps_x + cosf(m_pose.theta) * eps_y; // eps_r_st (eps_r*) = eps_r dotprod y_1:
                                                                               // where eps_r is the real error vector

    m_direction = eps_r_st > 0 ? Forward : Backward;

    /*
    float eps_r = sqrtf(eps_x * eps_x + eps_y * eps_y);
    float sign = eps_r_st > 0 ? 1.0f : -1.0f;
    eps_r = eps_r > 1e-3f ? sign * eps_r : 0;
    */

    float u = 0.f, w = 0.f;

    // m_ref_pose.theta = toRad(0);
    w = m_pidAngle.step(m_ref_pose.theta, m_pose.theta); // Rotation command
    u = m_pidDist.step(0, eps_r_st);                     // Translation command

    u = m_isTranslationEnabled ? u : 0.0f;
    w = m_isRotationEnabled ? w : 0.0f;
    /** Thrust mixing **/
    // Using torque when rotation to have less torque when having bad floor contact.
    w = torqueSaturation(w, m_phy.torqueWhenRotatation_max);
    m_pidAngle.setOutputFeedback(w); // Get the saturated output
    // We look how much torque is left for translation
    float availableTorque =
        m_phy.torque_max - fabs(w) > 0 ? m_phy.torque_max - fabs(w) : 0; // Priority for rotation (w)
    u = torqueSaturation(u, availableTorque);
    m_pidDist.setOutputFeedback(u);

    /* Uncomment these lines to debug print pid */
    // char msg[200];
    // snprintf(msg, 200, "log.ctrlr <%ld; %f; %f; %f; %f; %f; %f; %f; %f; %f; %f>", HAL_GetTick(), m_ref_pose.x,
    // m_ref_pose.y,
    //          m_ref_pose.theta, m_pose.x, m_pose.y, m_pose.theta, m_pidAngle.m_ui[0], m_pidDist.m_ui[0],
    //          u, w);
    // printf("%s\n", msg);

    // Convert outputs in mA
    u = u / K_C * 1000.f;
    w = w / K_C * 1000.f;
    m_output.u_1 = u - w;
    m_output.u_2 = -u - w; // + not - because wheels are opposed ! Just like for odometry !!!
}

float Ctrlr::computeReference(float x_r, float y_r) const
{
    float eps_x = x_r - m_pose.x;
    float eps_y = y_r - m_pose.y;

    /** Compute references **/
    float eps_r = sqrtf(eps_x * eps_x + eps_y * eps_y);                        // Distance error
    float eps_r_st = -sinf(m_pose.theta) * eps_x + cosf(m_pose.theta) * eps_y; // eps_r_st (eps_r*) = eps_r dotprod y_1:
                                                                               // where eps_r is the real error vector
    // float theta_r = atan2f(eps_y, eps_x) - M_PI_2 + (eps_r_st > 0 ? 0 : M_PI); // Azimuth reference
    float theta_r = atan2f(eps_y, eps_x) - (float)M_PI_2; // Azimuth reference

    /* So it's a bit of try/error but it makes sense when you think about it
       We calculate the shortest angle theta_r wrt to theta, that's the relative angle we want to make.
    */
    theta_r = wrapToPi_2(theta_r - m_pose.theta);
    /* Then we add theta to this relative angle to make it absolute*/
    return theta_r + m_pose.theta;
}

void Ctrlr::setCtrlSpeedMode(CTRL_SPEED_MODE mode)
{
    switch (mode)
    {
    case NORMAL_MODE: // Normal mode
    {
        m_phy = DEFAULT_PHY;
        break;
    }
    case SAFE_MODE: // Slow mode
    {
        m_phy = SAFE_PHY;
        break;
    }

    default: {
        LOG_WARNING("Ctrl speed mode not found.");
        break;
    }
    }
}

} // namespace NSCtrlr
