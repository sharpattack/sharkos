#include "Control/pid.h"
#include "math.h"

float PID::NO_D = 1e-10f;
float PID::NO_I = 1e10f;

/*
PID::PID() : m_eps{0, 0}, m_ui{0, 0}, m_ud{0, 0}, m_y{0, 0}
{
    m_ts = 1.0f;
    m_kp = 0.0f;
    m_ti = NO_I;
    m_td = NO_D;
    m_N = 0.0f;
    m_windupThreshold = PID::NO_I;
}
*/

PID::PID(float ts, float kp, float ti, float td, float N, float y0, float u_min, float u_max)
    : m_eps{0, 0}, m_ui{0, 0}, m_ud{0, 0}, m_y{0, 0}
{

    m_ts = ts;
    m_kp = kp;
    m_td = td;
    m_ti = ti;
    m_N = N;

    // Saturation management
    m_windupThreshold = PID::NO_I;
    m_tt = m_ti; // Default value

    // For internal saturation
    m_u_min = u_min;
    m_u_max = u_max;

    // Memories
    reset();
    m_y[0] = y0;
}

float PID::step(float ref, float y)
{
    // PID scheme
    m_eps[0] = ref - y;
    m_ui[0] = m_ui[1] + m_kp * m_ts / m_ti * m_eps[1];
    m_ud[0] = 1 / (1 + m_N * (m_ts / m_td)) * m_ud[1] - m_kp * m_N / (1 + m_N * m_ts / m_td) * (y - m_y[1]);
    m_u[0] = m_kp * m_eps[0] + m_ui[0] + m_ud[0];

    // Apply saturation
    antiWindUp(); // Apply saturation on integrator
    if (m_internal_saturation == true)
    {
        if (m_u[0] > m_u_max)
        {
            m_u[0] = m_u_max;
        }
        else if (m_u[0] < m_u_min)
        {
            m_u[0] = m_u_min;
        }
        m_u_sat = m_u[0];
    }

    // update memories
    m_eps[1] = m_eps[0];
    m_ui[1] = m_ui[0];
    m_ud[1] = m_ud[0];
    m_y[1] = y;
    m_u[1] = m_u[0];
    return m_u[0];
}

void PID::reset()
{
    m_y[0] = 0;
    m_y[1] = 0;

    m_eps[0] = 0;
    m_eps[1] = 0;

    m_ui[0] = 0;
    m_ui[1] = 0;

    m_ud[0] = 0;
    m_ud[1] = 0;

    m_u[0] = 0;
    m_u[1] = 0;

    m_u_sat = 0;
}

void PID::resetIntegrator()
{
    m_ui[0] = 0;
    m_ui[1] = 0;
}

void PID::antiWindUp()
{
    switch (m_aw_scheme)
    {
    case ANTI_WINDUP_SCHEME_INTEGRATOR_RANGE:
        if (m_ui[0] < -m_windupThreshold)
        {
            m_ui[0] = -m_windupThreshold;
        }
        else if (m_ui[0] > m_windupThreshold)
        {
            m_ui[0] = m_windupThreshold;
        }
        break;
    case ANTI_WINDUP_SCHEME_CONDITIONAL_INTEGRATION:
        // If command saturated
        if (fabsf(m_u_sat - m_u[1]) > 1e-5) // I don't trust == or != operator for float
        {
            m_ui[0] = m_ui[1]; // Stop integrating
        }
        break;
    case ANTI_WINDUP_SCHEME_BACK_CALCULATION:
        if (m_tt > 0) // Just in case atm but it should not happen
        {
            m_ui[0] = m_ui[0] + 1 / m_tt * (m_u_sat - m_u[1]);
        }
        break;

    default:
        break;
    }
}
