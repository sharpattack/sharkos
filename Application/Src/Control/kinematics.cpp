#include "Control/kinematics.h"
#include "main.h"
#include <cmath>
// #include "math_helper.h"
// #include "logs.h"
#include <cmath>

uint8_t g_poseUpdateInitLatch = 0;

constexpr float F_PI = static_cast<float>(M_PI);
constexpr float F_PI_2 = static_cast<float>(M_PI_2);

namespace Kinematics
{
float toRad(const float angle)
{
    return angle * F_PI / 180.0f;
}

float toDeg(float angle)
{
    return angle * 180.0f / F_PI;
}

/**
 * @brief Constrain the input in the range [-pi,pi]. Return the constrained angle.
 *
 * @param angle [rad]
 * @return float [rad]
 */
float wrapToPi(float angle)
{
    while (angle >= F_PI)
    {
        angle -= 2 * F_PI;
    }
    while (angle <= -F_PI)
    {
        angle += 2 * F_PI;
    }
    return angle;
}

/**
 * @brief Constrain the input in the range [-pi/2,pi/2]. Return the constrained angle.
 *
 * @param angle [rad]
 * @return float [rad]
 */
float wrapToPi_2(float angle)
{
    while (angle > F_PI_2)
    {
        angle -= F_PI;
    }
    while (angle < -F_PI_2)
    {
        angle += F_PI;
    }
    return angle;
}

void updatePose(Pose &pose, Pose &poseDot)
{
    static int32_t N_3[2] = {0}; // Number of pulses for left encoder at t0 and t1 (t0 + Ts)
    static int32_t N_2[2] = {0}; // Number of pulses for right encoder at t0 and t1 (t0 + Ts)
    // const float k1 = 0.938719281981292f, k2 = 1.091399f;
    // const float k1 = 0.999956f * 0.995991f, k2 = 1.0f;
    // const float k1 = 1.0393f / 1.0055f / 1.00166f, k2 = 1.0f; // 1.09
    const float k1 = 1.02610f, k2 = 1.02295f;

    float &x = pose.x;
    float &y = pose.y;
    float &theta = pose.theta;

    if (!g_poseUpdateInitLatch)
    {
        N_2[1] = -TIM2->CNT;
        N_3[1] = TIM5->CNT; // motors have opposite rotation axis hence the (-1)
        g_poseUpdateInitLatch = 1;
    }
    else
    {
        N_2[1] = N_2[0]; // New values become old values
        N_3[1] = N_3[0];
    }
    N_2[0] = -TIM2->CNT; // New values are updated
    N_3[0] = TIM5->CNT;

    theta = k1 * K_D / (2 * K_L) * (N_3[0] - N_2[0]) * K_KS;
    // Sampling time is not used here because it's simplified during the integration later
    float phi_2Dot = (N_2[0] - N_2[1]) * K_KS / K_TS;
    float phi_3Dot = (N_3[0] - N_3[1]) * K_KS / K_TS;
    x += k2 * K_D / 4.0f * (phi_2Dot + phi_3Dot) * sinf(theta) * K_TS;
    y -= k2 * K_D / 4.0f * (phi_2Dot + phi_3Dot) * cosf(theta) * K_TS;

    poseDot.theta = k1 * (K_D / (2.0f * K_L)) * (phi_3Dot - phi_2Dot);
    poseDot.x = k2 * (K_D / 4.0f) * (phi_2Dot + phi_3Dot) * sinf(theta);
    poseDot.y = k2 * (-K_D / 4.0f) * (phi_2Dot + phi_3Dot) * cosf(theta);
}

} // namespace Kinematics
