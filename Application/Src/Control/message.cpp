#include "Control/message.h"

#include "Tasks/task_ctrl.h"
#include "queue.h"

extern QueueHandle_t _cmdQueue;

#ifdef __cplusplus
extern "C"
{
#endif

    void ctrl_move_to(float x, float y, CTRL_SPEED_MODE mode = NORMAL_MODE)
    {
        ctrl_wait_init();

        NSCtrlr::Message request = {
            .type = NSCtrlr::Message::MESSAGE_TYPE_MOVE_TO, .pos = {.x = x, .y = y}, .speed_mode = mode};

        xQueueSend(_cmdQueue, &request, 0);
    }

    void ctrl_move_of(float x, float y, CTRL_SPEED_MODE mode = NORMAL_MODE)
    {
        ctrl_wait_init();

        NSCtrlr::Message request = {
            .type = NSCtrlr::Message::MESSAGE_TYPE_MOVE_OF, .pos = {.x = x, .y = y}, .speed_mode = mode};

        xQueueSend(_cmdQueue, &request, 0);
    }

    void ctrl_rotate_to(float theta, CTRL_SPEED_MODE mode = NORMAL_MODE)
    {
        ctrl_wait_init();

        NSCtrlr::Message request = {
            .type = NSCtrlr::Message::MESSAGE_TYPE_ROTATE_TO, .theta = theta, .speed_mode = mode};

        xQueueSend(_cmdQueue, &request, 0);
    }

    void ctrl_rotate_of(float theta, CTRL_SPEED_MODE mode = NORMAL_MODE)
    {
        ctrl_wait_init();

        NSCtrlr::Message request = {
            .type = NSCtrlr::Message::MESSAGE_TYPE_ROTATE_OF, .theta = theta, .speed_mode = mode};

        xQueueSend(_cmdQueue, &request, 0);
    }

    void ctrl_rotate_toward(float x, float y, CTRL_SPEED_MODE mode = NORMAL_MODE)
    {
        ctrl_wait_init();

        NSCtrlr::Message request = {
            .type = NSCtrlr::Message::MESSAGE_TYPE_ROTATE_TOWARD, .pos = {.x = x, .y = y}, .speed_mode = mode};
        xQueueSend(_cmdQueue, &request, 0);
    }

    void ctrl_abort()
    {
        ctrl_wait_init();

        NSCtrlr::Message request{};
        request.type = NSCtrlr::Message::MESSAGE_TYPE_ABORT;

        xQueueSend(_cmdQueue, &request, 0);
    }

    void ctrl_stop()
    {
        ctrl_wait_init();

        NSCtrlr::Message request{};
        request.type = NSCtrlr::Message::MESSAGE_TYPE_STOP;

        xQueueSend(_cmdQueue, &request, 0);
    }

#ifdef __cplusplus
}
#endif
