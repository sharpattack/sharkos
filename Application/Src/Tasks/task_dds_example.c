//
// Created by madahin on 25/08/2021.
//

#include "Tasks/task_dds_example.h"

#include "Tasks/task_dds.h"
#include "hello.h"

#ifdef BUILD_DDS_PUBLISH_EXAMPLE

DDS_PREPARE_PUBLISH_TOPIC(HelloWorld);

_Noreturn void StartHelloWorldPublisherTask(void *parameters)
{
    DDS_INIT_PUBLISH_TOPIC(HelloWorld, 1);

    uint32_t count = 0;
    for (;;)
    {
        HelloWorld topic = {++count};
        DDS_PUBLISH_TOPIC_RELIABLE(HelloWorld, &topic);
    }
}

#endif

#ifdef BUILD_DDS_SUBSCRIBE_EXAMPLE

DDS_PREPARE_SUBSCRIBE_TOPIC(HelloWorld);

_Noreturn void StartHelloWorldSubscriberTask(void *parameters)
{
    DDS_INIT_SUBSCRIBE_TOPIC(HelloWorld, 3, CIRCULAR);

    for (;;)
    {
        HelloWorld topic;
        if (DDS_RETRIEVE_TOPIC_RELIABLE(HelloWorld, &topic) == pdTRUE)
        {
            LOG_INFO("index: %d", topic.index);
        }
        else
        {
            LOG_ERROR("Unable to retrieve data");
        }
        sleep(10);
    }
}

#endif
