//
// Created by ation on 19/12/2021.
//

#include "Tasks/task_pathfinding_example.h"
#include "Tasks/task_pathfinding.h"

#include "logs.h"

[[noreturn]] void StartPathFindingExampleTask(void *parameters)
{
    LOG_INFO("test pathfinding");

    pathfinding_wait_init();

    request_path(0.4, 1.5, 2.5, 1.5);
    LOG_INFO("request sent");

    for (;;)
    {
        PathFindingTarget target;
        if (get_target(&target))
        {
            LOG_INFO("target recieved: (%f, %f)", target.x, target.y);
        }
        sleep(1);
    }
}
