#include "Tasks/task_led.h"
#include <sys/cdefs.h>

_Noreturn void vStartLedTask(void *parameters)
{
    struct vLEDTaskParameters *param = (struct vLEDTaskParameters *)parameters;

    for (;;)
    {
        HAL_GPIO_WritePin(param->gpio_port, param->gpio_pin, GPIO_PIN_SET);
        sleep(LED_BLINK_MS);
        HAL_GPIO_WritePin(param->gpio_port, param->gpio_pin, GPIO_PIN_RESET);
        sleep(LED_BLINK_MS);
    }
}
