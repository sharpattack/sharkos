#include "Tasks/task_strat.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "projdefs.h"
#include "IDL/robot.h"
#include "IDL/score.h"
#include "Tasks/task_ctrl.h"
#include "Tasks/task_dds.h"
#include "Tasks/task_pathfinding.h"
#include "Strat/game_state.h"
#include "logs.h"
#include "playerside.h"
#include "profile.h"
#include "shared_state.h"
#include <portmacro.h>
#include <sys/cdefs.h>

#include "tmc/helpers/Types.h"

#include "Strat/actions.h"

DDS_PREPARE_SUBSCRIBE_TOPIC(PlayerSide);
DDS_PREPARE_PUBLISH_TOPIC(Score);

float remaining_critical_actions_duration_s(GameState state)
{
    float duration_sum = 0;
    for (int i = 0; i < ACTIONS_NB; i++)
    {
        Action action = ACTIONS[i];
        if (action.critical && !action.performed)
        {
            duration_sum += action.duration_s;
        }
    }
    return duration_sum;
}

// Return the index of the next action that should be performed
// return -1 if there is no undone action anymore
int16_t get_next_action(GameState state)
{

    for (int16_t i = 0; i < ACTIONS_NB; i++)
    {
        Action action = ACTIONS[i];

        // the action was already performed, or we tried to perform it
        // too many times: ignore
        if (action.performed || action.tries_nb >= MAX_TRIES_NB)
        {
            continue;
        }

        float critical_duration_s = remaining_critical_actions_duration_s(state);
        float remaining_duration_s = MATCH_DURATION_S - state.time_s;

        // the action takes too much time: ignore
        if (remaining_duration_s - action.duration_s < critical_duration_s)
        {
            continue;
        }

        return i;
    }

    return -1;
}

_Noreturn void StartStratTask(void *parameters)
{

    DDS_INIT_SUBSCRIBE_TOPIC(PlayerSide, 1, LAST_ONLY);
    DDS_INIT_PUBLISH_TOPIC(Score, 1);

    actions_init();

    LOG_INFO("Strat: waiting for ctrl to initialize.");
    ctrl_wait_init();
    LOG_INFO("Strat: waiting for match to start.");
    ctrl_wait_match_start();
    LOG_INFO("Strat: starting!");

    // init claw wall correctly
    /* Claws, solar panel and elevator are unavailable without canopen */

    // recup side
    LOG_INFO("Strat: waiting for side.")
    Side side;
#ifdef PROFILE_BYPASS_DDS
    side = GREEN_SIDE;
#else
    PlayerSide pside;
    DDS_RETRIEVE_TOPIC_RELIABLE(PlayerSide, &pside);
    side = pside.value ? GREEN_SIDE : BLUE_SIDE;
    set_shared_side(side);
#endif
    const float start_time_s = (float32_t)pdTICKS_TO_MS(xTaskGetTickCount()) / 1000.0f;
    // start with 5 points from one ALI
    GameState state = {.earned_points = 5, .time_s = 0.0f};
    // update score (reset to 0)
    Score score = {state.earned_points};
    DDS_PUBLISH_TOPIC_TIMEOUT(Score, &score, 100);

    while (true)
    {

        // * Update gamestate time
        float current_time_s = (float32_t)pdTICKS_TO_MS(xTaskGetTickCount()) / 1000.0f;
        state.time_s = current_time_s - start_time_s;

        // * Get next action to perform
        int16_t action_i = get_next_action(state);
        if (action_i == -1)
        {
            LOG_INFO("Strat: no more actions to perform.");
            for (int action_i = 0; action_i < ACTIONS_NB; action_i++)
            {
                Action action = ACTIONS[action_i];
                LOG_INFO("Attempted action %s %d times. Success: %d. Reward: %d.", action.name, action.tries_nb,
                         action.performed, action.performed ? action.reward : 0);
            }

            while (true)
            {
                sleep(portMAX_DELAY);
            }
        }

        Action *action = &ACTIONS[action_i];
        LOG_INFO("Strat: performing action %s.", action->name);

        // * Perform action
        Result result = action->func(side);
        action->tries_nb++;

        // * Update gamestate according to action result
        if (result == ACTION_FAIL || result == ACTION_ABORT)
        {
            LOG_WARNING("Strat: action %s failed.", action->name);
        }
        else
        {
            // TODO: we consider that actions fully succeed every time
            state.earned_points += action->reward;
            LOG_INFO("Strat: current points: %d.", state.earned_points);

            action->performed = true;

            // update score on the DDS world
            Score updatedScore = {state.earned_points};
            if (DDS_PUBLISH_TOPIC_TIMEOUT(Score, &updatedScore, 100) != pdTRUE)
            {
                LOG_WARNING("Strat: could not post score to DDS.");
            }
        }
    }
}
