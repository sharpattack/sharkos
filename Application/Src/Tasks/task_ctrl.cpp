#include "Tasks/task_ctrl.h"
#include "Control/ctrlr.h"
#include "FreeRTOS.h" // Freertos
#include "Tasks/task_dds.h"
#include "abs_encoder.h"
#include "abs_gpio.h"
#include "abs_motor.h"
#include "event_groups.h" // Freertos
#include "odometry.h"     // IDL
#include "profile.h"
#include "task.h" // Freertos

using namespace NSCtrlr;

extern EventGroupHandle_t ctrl_event_handle;

// Hardcoded stepper driver

#define ABS(x) ((x < 0) ? -(x) : (x))

void ctrlrTask_GPIO_Interrupt_Handler(uint16_t GPIO_Pin)
{

    if (abs_gpio_pin_start_match_interrupt(GPIO_Pin))
    {
        BaseType_t xHigherPriorityTaskWoken, xResult;
        xResult = xEventGroupSetBitsFromISR(ctrl_event_handle, CTRL_MATCH_START_BIT, &xHigherPriorityTaskWoken);
        if (xResult != pdFAIL)
        {
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
    }
}
// static uint8_t cmdQueueStorage[CMD_QUEUE_SIZE * sizeof(Message)] = {0};
static StaticQueue_t xStaticCmdQueue;
QueueHandle_t _cmdQueue = nullptr;

enum Strategy
{
    YELLOW,
    PURPLE
};

DDS_PREPARE_PUBLISH_TOPIC(OdomPoseEstim);

void startCtrlTask(void *parameters)
{

    abs_motor_init(0);
    abs_motor_init(1);

    // Timers & Encoders setup
    abs_encoder_init(0);
    abs_encoder_init(1);

    TickType_t xLastWakeTime = xTaskGetTickCount();

    uint8_t cmdQueueMem[CMD_QUEUE_SIZE * sizeof(Message)] = {0};
    _cmdQueue = xQueueCreateStatic(CMD_QUEUE_SIZE, sizeof(Message), cmdQueueMem, &xStaticCmdQueue);

    Odometry updated = {.x = 0.0, .y = 0.0, .theta = 0.0};
    set_shared_odometry(&updated);

    xEventGroupSetBits(ctrl_event_handle, CTRL_INIT_BIT);
#ifdef PROFILE_BYPASS_STARTER
    xEventGroupSetBits(ctrl_event_handle, CTRL_MATCH_START_BIT);
#endif

    DDS_INIT_PUBLISH_TOPIC(OdomPoseEstim, 3);

    // Initialize all states
    Init init;
    Idle idle;
    Moving moving;
    Rotating rotating;

    // Create states in state ID order
    etl::ifsm_state *stateList[StateId::NUMBER_OF_STATES] = {&init, &idle, &moving, &rotating};

    CtrlStateMachine stateMachine(ctrl_event_handle, K_TS, privDDSQueue_OdomPoseEstim);
    stateMachine.set_states(stateList, etl::size(stateList));
    stateMachine.start();

    while (1)
    {
        abs_motor_state_machine_step(0);
        abs_motor_state_machine_step(1);
        Message msg{};

        if (xQueueReceive(_cmdQueue, &msg, 0) == pdTRUE)
        {
            LOG_INFO("Ctrl: received order %i", msg.type);
            auto pose = stateMachine.ctrlr.m_pose;

            switch (msg.type)
            {

            case Message::MESSAGE_TYPE_MOVE_TO: {
                LOG_INFO("Message type=%d translate to %f %f", (int)msg.type, msg.pos.x, msg.pos.y);
                stateMachine.ctrlr.setCtrlSpeedMode(msg.speed_mode);
                stateMachine.receive(Move());
                stateMachine.receive(SetMoveTarget(msg.pos.x, msg.pos.y));
                break;
            }

            case Message::MESSAGE_TYPE_MOVE_OF: {
                LOG_INFO("Message type=%d translate of %f %f", (int)msg.type, msg.pos.x, msg.pos.y);
                stateMachine.ctrlr.setCtrlSpeedMode(msg.speed_mode);
                stateMachine.receive(Move());
                float x = pose.x - msg.pos.y * sinf(pose.theta);
                float y = pose.y + msg.pos.y * cosf(pose.theta);
                stateMachine.receive(SetMoveTarget(x, y));
                break;
            }

            case Message::MESSAGE_TYPE_ROTATE_TO: {
                LOG_INFO("Message type=%d rotate to %f deg", (int)msg.type, (msg.theta * 180 / M_PI));
                stateMachine.ctrlr.setCtrlSpeedMode(msg.speed_mode);
                stateMachine.receive(Rotate());
                stateMachine.receive(SetRotationTarget(msg.theta));
                break;
            }

            case Message::MESSAGE_TYPE_ROTATE_OF: {
                LOG_INFO("Message type=%d rotate of %f deg", (int)msg.type, (msg.theta * 180 / M_PI));
                stateMachine.ctrlr.setCtrlSpeedMode(msg.speed_mode);
                stateMachine.receive(Rotate());
                stateMachine.receive(SetRotationTarget(pose.theta + msg.theta));
                break;
            }

            case Message::MESSAGE_TYPE_ROTATE_TOWARD: {
                LOG_INFO("Message type=%d rotate towards %f %f", (int)msg.type, msg.pos.x, msg.pos.y);
                stateMachine.ctrlr.setCtrlSpeedMode(msg.speed_mode);
                float theta = stateMachine.ctrlr.computeReference(msg.pos.x, msg.pos.y);
                stateMachine.receive(Rotate());
                stateMachine.receive(SetRotationTarget(theta));
                break;
            }

            case Message::MESSAGE_TYPE_ABORT: {
                LOG_INFO("Message type=%d break", (int)msg.type);
                stateMachine.receive(Abort());
                break;
            }

            default: {
                LOG_WARNING("Message type=%d (unknown type)", (int)msg.type);
                break;
            }
            }
        }

        stateMachine.receive(Step());

        stateMachine.ctrlr.m_time += stateMachine.ctrlr.m_ts;

        Output out = stateMachine.ctrlr.getOutput();

        abs_motor_set_torque(0, out.u_1);
        abs_motor_set_torque(1, out.u_2);

        vTaskDelayUntil(&xLastWakeTime, 5); // 5 ticks = 5 ms = 200 Hz
    }
}

void ctrl_wait_init()
{
    EventBits_t uxBits = 0;

    while ((uxBits & CTRL_INIT_BIT) == 0)
    {
        uxBits = xEventGroupWaitBits(ctrl_event_handle, CTRL_INIT_BIT, pdFALSE, pdFALSE, 1024);
    }
}

void ctrl_wait_match_start()
{
    xEventGroupWaitBits(ctrl_event_handle, CTRL_MATCH_START_BIT, pdFALSE, pdFALSE, -1);
}

bool ctrl_wait_move_completed(TickType_t wait_tick)
{
    EventBits_t uxBits = 0;

    uxBits = xEventGroupWaitBits(ctrl_event_handle, CTRL_MOVE_COMPLETED_BIT, pdTRUE, pdFALSE, wait_tick);

    return (uxBits & CTRL_MOVE_COMPLETED_BIT) != 0;
}
