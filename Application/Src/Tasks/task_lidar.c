#include "Tasks/task_lidar.h"
#include "FreeRTOSConfig.h"
#include "Tasks/task_dds.h"
#include "lidar.h"
#include "logs.h"
#include "projdefs.h"
#include "shared_state.h"
#include "task.h"
#include <math.h>

DDS_PREPARE_SUBSCRIBE_TOPIC(LidarProximity);
DDS_PREPARE_SUBSCRIBE_TOPIC(LidarPoseEstim);

// init the shared lidar state using default values
void init_shared_lidar_state()
{
    LidarSensor *shared_lidar_state = &get_shared_state()->lidar_sensor;

    shared_lidar_state->distance = INFINITY;
    shared_lidar_state->an_object_is_close = false;
    shared_lidar_state->pose_estim_is_valid = false;
    shared_lidar_state->pose_estim_x = 0;
    shared_lidar_state->pose_estim_y = 0;
    shared_lidar_state->pose_estim_theta = 0;
}

_Noreturn void StartLidarTask(void *parameters)
{
    init_shared_lidar_state();

    DDS_INIT_SUBSCRIBE_TOPIC(LidarProximity, 1, LAST_ONLY);
    DDS_INIT_SUBSCRIBE_TOPIC(LidarPoseEstim, 1, LAST_ONLY);

    LidarProximity lidar_proximity;
    LidarPoseEstim lidar_pose_estim;

    LidarSensor *shared_lidar_state = &get_shared_state()->lidar_sensor;

    LOG_INFO("Lidar: ready");

    for (;;)
    {
        // * Proximity
        int ret = DDS_RETRIEVE_TOPIC_BEST_EFFORT(LidarProximity, &lidar_proximity);
        if (ret == pdTRUE)
        {
            taskENTER_CRITICAL();
            shared_lidar_state->distance = lidar_proximity.distance;
            shared_lidar_state->an_object_is_close = lidar_proximity.anObjectIsClose;
            for (int i = 0; i < LIDAR_SENSOR_PROXIMITY_ZONES_NB; i++)
            {
                shared_lidar_state->proximity[i] = lidar_proximity.proximity[i];
            }
            taskEXIT_CRITICAL();
        }

        // * Pose estimation
        ret = DDS_RETRIEVE_TOPIC_BEST_EFFORT(LidarPoseEstim, &lidar_pose_estim);
        if (ret == pdTRUE)
        {
            taskENTER_CRITICAL();
            shared_lidar_state->pose_estim_is_valid = lidar_pose_estim.isValid;
            shared_lidar_state->pose_estim_x = lidar_pose_estim.pos.x;
            shared_lidar_state->pose_estim_y = lidar_pose_estim.pos.y;
            shared_lidar_state->pose_estim_theta = lidar_pose_estim.angle;
            taskEXIT_CRITICAL();
        }

        sleep(LIDAR_TASK_SLEEP_DURATION);
    }
}
