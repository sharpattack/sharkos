#include "etl/string.h"

#include "embd_string.h"

#include "serial_transport_external.h"
#include "uxr/client/util/time.h"

#include "Tasks/task_dds.h"
#include "Tasks/task_dds_priv.h"

/****************************
 *     Global Variables     *
 ****************************/

// DDS
uxrSession gl_session;

uint8_t gl_input_reliable_stream_buffer[UXR_CONFIG_MAX_INPUT_RELIABLE_STREAMS][DDS_BUFFER_SIZE];
uint8_t gl_output_reliable_stream_buffer[UXR_CONFIG_MAX_OUTPUT_RELIABLE_STREAMS][DDS_BUFFER_SIZE];

uxrObjectId gl_participant_id;
uxrStreamId gl_reliable_in[UXR_CONFIG_MAX_INPUT_RELIABLE_STREAMS];
uxrStreamId gl_reliable_out[UXR_CONFIG_MAX_OUTPUT_RELIABLE_STREAMS];

uxrObjectId gl_datawriters_id[DDS_MAX_DATA_WRITER];
uxrObjectId gl_datareaders_id[DDS_MAX_DATA_READER];

static etl::map<uxrObjectIdRaw, fnDeserializeTopicCallBack *, DDS_MAX_TOPIC> gl_mapSubscriberTopicToFunc;
static etl::map<uxrObjectIdRaw, privCallBackArgs, DDS_MAX_TOPIC> gl_mapSubscriberCallbackArgs;

// FreeRTOS Queue
QueueHandle_t gl_xOperationQueue = nullptr;
static StaticQueue_t gl_xStaticOperationQueue;
uint8_t gl_u8OperationQueueStorageArea[DDS_MAX_TOPIC_OPERATION * sizeof(TopicOperation)];

/****************************
 *     Static functions     *
 ****************************/

static void privAddOperationToQueue(TopicOperation &operation, const char *szTopicName);
static void privWaitForQueueReady();
static bool privCreateTopic(uxrSession &session, const char *szTopicName);
static bool privCreateDataWriter(const uint8_t u8dataWriterIndex, uxrSession &session, const char *szTopicName);
static bool privCreateDataReader(const uint8_t u8dataReaderIndex, uxrSession &session, const char *szTopicName,
                                 TopicOperation &operation);
static uint16_t privHashTopicName(const char *szTopicName);
static void privOnTopicReceived(uxrSession *, uxrObjectId, uint16_t, uxrStreamId, struct ucdrBuffer *, uint16_t,
                                void *);

/****************************
 *        Const Data        *
 ****************************/

constexpr char szReasonHeartBeat[] = "Heart Beat failed";
constexpr char szReasonWrite[] = "Write failed";
constexpr char szReasonRead[] = "Read failed";
constexpr char szReasonUnknown[] = "Unknown";

[[noreturn]] void StartDDSTask(void *parameters)
{
    auto param = (struct vDDSTaskParameters *)parameters;
    uint64_t ullTimeLastPing;

    // Initialise the queue to communicate between tasks
    gl_xOperationQueue = xQueueCreateStatic(DDS_MAX_TOPIC_OPERATION, sizeof(TopicOperation),
                                            gl_u8OperationQueueStorageArea, &gl_xStaticOperationQueue);
    assert_and_log(gl_xOperationQueue, "Unable to create DDS operation queue");
    LOG_INFO("DDS - Created communication queue");
    sleep(1000);

    uxrExternalSerialTransport transport;

    uxr_set_external_serial_transport_callbacks(&transport, my_custom_transport_open, my_custom_transport_close,
                                                my_custom_transport_write, my_custom_transport_read);

    assert_and_log(uxr_init_external_serial_transport(&transport, param->selected_uart),
                   "DDS - Failed to init serial transport");

    LOG_INFO("DDS - Successfully init serial transport");

    TickType_t ulStartLoop = xTaskGetTickCount();

    // if we lose the com', we have to redo the whole process
    do
    {

        LOG_INFO("DDS - Waiting for agent");
        // Sleep a bit to add randomness to the gl_session key
        sleep(10);
        bool bAgentFound = false;
        while (!bAgentFound)
        {
            for (int i = 0; (i < 10) && !bAgentFound; ++i)
            {
                bAgentFound = static_cast<bool>(uxr_ping_agent_attempts(&transport.comm, 100, 5));
            }
            if (!bAgentFound)
            {
                LOG_DEBUG("DDS - ping timeout");
                sleep(1000);
            }
        }
        LOG_INFO("DDS - Success! Agent Found");
        memset(&gl_session, 0, sizeof(gl_session));
        {
            const auto sessionKey = static_cast<uint32_t>(xTaskGetTickCount() - ulStartLoop);
            LOG_INFO("DDS - Initialise gl_session with key %lu", sessionKey);
            uxr_init_session(&gl_session, &transport.comm, sessionKey);
            LOG_INFO("DDS - Finished initializing gl_session");
        }

        uxr_set_topic_callback(&gl_session, privOnTopicReceived, nullptr);
        if (!uxr_create_session(&gl_session))
        {
            LOG_ERROR("DDS - Error at creating a gl_session.");
            continue;
        }
        ullTimeLastPing = (xTaskGetTickCount() / portTICK_PERIOD_MS);
        LOG_INFO("DDS - Session created");
        // Streams
        for (uint8_t i = 0; i < UXR_CONFIG_MAX_OUTPUT_RELIABLE_STREAMS; ++i)
        {
            gl_reliable_out[i] = uxr_create_output_reliable_stream(&gl_session, gl_output_reliable_stream_buffer[i],
                                                                   DDS_BUFFER_SIZE, DDS_STREAM_HISTORY);
            // gl_reliable_out[i] = uxr_create_output_best_effort_stream(&gl_session,
            // gl_output_reliable_stream_buffer[i], DDS_BUFFER_SIZE);
            LOG_INFO("DDS - Created output stream %d of type %d", gl_reliable_out[i].index, gl_reliable_out[i].type);
        }

        for (uint8_t i = 0; i < UXR_CONFIG_MAX_INPUT_RELIABLE_STREAMS; ++i)
        {
            gl_reliable_in[i] = uxr_create_input_reliable_stream(&gl_session, gl_input_reliable_stream_buffer[i],
                                                                 DDS_BUFFER_SIZE, DDS_STREAM_HISTORY);
            // gl_reliable_in[i] = uxr_create_input_best_effort_stream(&gl_session);
            LOG_INFO("DDS - Created input stream %d of type %d", gl_reliable_in[i].index, gl_reliable_in[i].type);
        }

        {
            gl_participant_id = uxr_object_id(0x01, UXR_PARTICIPANT_ID);
            const char *participant_xml = DDS_PARTICIPANT_XML;
            uint16_t participant_req = uxr_buffer_create_participant_xml(
                &gl_session, gl_reliable_out[0], gl_participant_id, 0, participant_xml, UXR_REPLACE);

            uint8_t status[1];
            uint16_t requests[1] = {participant_req};

            if (!uxr_run_session_until_all_status(&gl_session, 1000, requests, status, sizeof(status)))
            {
                LOG_ERROR("DDS - Error at create entity participant: %d", status[0]);
                uxr_delete_session(&gl_session);
                sleep(100);
                continue;
            }

            // Wait a bit before continuing
            sleep(100);
            LOG_INFO("DDS - Ready");
        }

        etl::vector<TopicOperation, DDS_MAX_TOPIC_OPERATION> vecOperations;
        etl::map<uint16_t, bool, DDS_MAX_TOPIC> mapKnownTopic;
        etl::map<etl::string<TOPIC_MAX_LEN>, uxrObjectId, DDS_MAX_DATA_WRITER> mapKnownDataWriter;
        etl::map<etl::string<TOPIC_MAX_LEN>, uint16_t, DDS_MAX_DATA_READER> mapKnownDataReader;
        bool bIsConnected = true;
        uint8_t u8FailedCounter = 0;
        const char *szConnectionLostReason = szReasonUnknown;
        while (bIsConnected)
        {
            // Firstly, we process new operations
            if (uxQueueMessagesWaiting(gl_xOperationQueue) > 0) [[unlikely]]
            {
                TopicOperation operation{};
                while (xQueueReceive(gl_xOperationQueue, &operation, 0) == pdTRUE)
                {
                    // Add the topic if we don't already now it
                    uint16_t u16TopicHash = privHashTopicName(operation.szTopicName);
                    if ((mapKnownTopic.find(u16TopicHash) == mapKnownTopic.end()) || (!mapKnownTopic[u16TopicHash]))
                    {
                        bool bResult = privCreateTopic(gl_session, operation.szTopicName);
                        mapKnownTopic[u16TopicHash] = bResult;
                        if (!bResult)
                        {
                            LOG_ERROR("DDS - Unable to add topic %s", operation.szTopicName);
                            continue;
                        }
                        ullTimeLastPing = (xTaskGetTickCount() / portTICK_PERIOD_MS);
                    }

                    if (operation.eType == PUBLISH)
                    {
                        if (mapKnownDataWriter.find(operation.szTopicName) != mapKnownDataWriter.end())
                        {
                            LOG_WARNING("DDS: Trying to add a data writer for topic %s but it already exist",
                                        operation.szTopicName);
                            continue;
                        }
                        assert_and_log(
                            privCreateDataWriter(mapKnownDataWriter.size(), gl_session, operation.szTopicName),
                            "DDS: Unable to create data writer");
                        ullTimeLastPing = (xTaskGetTickCount() / portTICK_PERIOD_MS);
                        mapKnownDataWriter[operation.szTopicName] = gl_datawriters_id[mapKnownDataWriter.size()];
                        LOG_INFO("DDS: Created data writer for topic %s", operation.szTopicName);
                    }
                    else if (operation.eType == SUBSCRIBE)
                    {
                        if (mapKnownDataReader.find(operation.szTopicName) != mapKnownDataReader.end())
                        {
                            LOG_WARNING("DDS: Trying to add an already known data reader");
                            continue;
                        }
                        assert_and_log(privCreateDataReader(mapKnownDataReader.size(), gl_session,
                                                            operation.szTopicName, operation),
                                       "DDS: Unable to create data reader");
                        ullTimeLastPing = (xTaskGetTickCount() / portTICK_PERIOD_MS);

                        uxrDeliveryControl delivery_control = {};
                        delivery_control.max_samples = UXR_MAX_SAMPLES_UNLIMITED;
                        mapKnownDataReader[operation.szTopicName] = uxr_buffer_request_data(
                            &gl_session, gl_reliable_out[0], gl_datareaders_id[mapKnownDataReader.size()],
                            gl_reliable_in[0], &delivery_control);

                        LOG_INFO("DDS: Created data reader for topic %s", operation.szTopicName);
                    }
                    else [[unlikely]]
                    {
                        LOG_ERROR("DDS - Unknown operation type");
                        continue;
                    }
                    vecOperations.push_back(operation);
                    // Wait a bit before continuing
                    sleep(100);
                }
            }
            // No one subscribed to a topic yet, so we simply check that the agent is still here
            if (vecOperations.empty()) [[unlikely]]
            {
                bIsConnected = uxr_sync_session(&gl_session, 1000);
                if (bIsConnected)
                {
                    ullTimeLastPing = (xTaskGetTickCount() / portTICK_PERIOD_MS);
                }
                else
                {
                    szConnectionLostReason = szReasonHeartBeat;
                }
                sleep(500);
            }
            else [[likely]]
            {
                bool bContainSubscribe = false;
                for (auto &operation : vecOperations)
                {
                    if (operation.eType == PUBLISH)
                    {
                        const DDSSerializeResult bSuccess = operation.topicData.publish.pFnSerializeTopic(
                            operation.xTopicQueue, &gl_reliable_out[0], &mapKnownDataWriter[operation.szTopicName],
                            &gl_session);
                        if (bSuccess != DDS_SER_NOTHING)
                        {
                            u8FailedCounter = (bSuccess) ? 0 : u8FailedCounter + 1;
                            if (bSuccess == DDS_SER_FAILURE)
                            {
                                LOG_WARNING("Failed to send topic");
                                szConnectionLostReason = szReasonWrite;
                            }
                        }
                    }
                    else [[likely]] // SUBSCRIBE
                    {
                        bContainSubscribe = true;
                    }
                }

                if (bContainSubscribe)
                {
                    uint8_t read_data_status;
                    // We cannot set bIsConnected with this methode because it return false on timeout,
                    // but it doesn't always mean that the agent is dead. It can also mean that no data was ready yet.
                    bool bSuccess = uxr_run_session_time(&gl_session, 30);
                    u8FailedCounter = (bSuccess) ? 0 : u8FailedCounter + 1;
                    if (!bSuccess)
                    {
                        szConnectionLostReason = szReasonWrite;
                    }
                    // LOG_INFO("read %s u8FailedCounter=%d error=%d", ((bSuccess) ? "success" : "failure"),
                    // u8FailedCounter, transport.comm.comm_error());
                }

                // If we failed to communicate to many times, we send a ping
                if ((u8FailedCounter > DDS_MAX_COM_ERROR) ||
                    ((xTaskGetTickCount() / portTICK_PERIOD_MS) - ullTimeLastPing > DDS_HEARTBEAT_TIME_MS))
                {
                    constexpr uint16_t NB_RETRY_CONNECT = 4;
                    constexpr uint16_t RETRY_CONNECT_TIMEOUT = 40;
                    // RETRY_CONNECT_TIMEOUT);
                    bIsConnected = uxr_sync_session(&gl_session, RETRY_CONNECT_TIMEOUT / NB_RETRY_CONNECT);

                    LOG_DEBUG("Free stack space = %u bytes", uxTaskGetStackHighWaterMark(nullptr) * 4);

                    for (uint16_t i = 0; !bIsConnected && (i < (NB_RETRY_CONNECT - 1)); ++i)
                    {
                        bIsConnected = uxr_sync_session(&gl_session, RETRY_CONNECT_TIMEOUT / NB_RETRY_CONNECT);
                    }
                    if (bIsConnected)
                    {
                        ullTimeLastPing = (xTaskGetTickCount() / portTICK_PERIOD_MS);
                        u8FailedCounter = 0;
                    }
                    else
                    {
                        // Last ditch effort, a good ol' ping
                        constexpr int PING_TIMOUT = 250;
                        constexpr uint8_t PING_ATTEMPT = 4;
                        LOG_WARNING("DDS - Try a ping as a last ditch effort - %d attempt in %dms", PING_ATTEMPT,
                                    PING_TIMOUT);
                        bIsConnected = uxr_ping_agent_session(&gl_session, PING_TIMOUT, PING_ATTEMPT);
                        if (!bIsConnected)
                        {
                            if (szConnectionLostReason == szReasonUnknown)
                            {

                                szConnectionLostReason = szReasonHeartBeat;
                            }
                        }
                    }
                }
            }
            sleep(1);
        }
        LOG_ERROR("DDS - Connexion lost. Reason: %s", szConnectionLostReason);

        // We save our "operation" to replay them at the next connexion
        for (auto &operation : vecOperations)
        {
            xQueueSend(gl_xOperationQueue, &operation, 0);
        }

        LOG_INFO("DDS - Deleting current gl_session");
        uxr_delete_session_retries(&gl_session, 1);
    } while (true);
}

static bool privCreateTopic(uxrSession &session, const char *szTopicName)
{
    char szTopicXML[sizeof(DDS_TOPIC_XML) + TOPIC_MAX_LEN * 2];

    snprintf(szTopicXML, sizeof(szTopicXML), DDS_TOPIC_XML, szTopicName, szTopicName);

    uxrObjectId topic_id = uxr_object_id(0x01, UXR_TOPIC_ID);
    uint16_t topic_req =
        uxr_buffer_create_topic_xml(&session, gl_reliable_out[0], topic_id, gl_participant_id, szTopicXML, UXR_REPLACE);

    uint8_t status[1];
    uint16_t requests[1] = {topic_req};

    return uxr_run_session_until_all_status(&session, 1000, requests, status, sizeof(status));
}

static bool privCreateDataWriter(const uint8_t u8dataWriterIndex, uxrSession &session, const char *szTopicName)
{
    char szDataWriterXML[sizeof(DDS_DATA_WRITER_XML) + TOPIC_MAX_LEN * 2];

    snprintf(szDataWriterXML, sizeof(szDataWriterXML), DDS_DATA_WRITER_XML, szTopicName, szTopicName);

    uxrObjectId publisher_id = uxr_object_id(DDS_PUBLISHER_ID_BEGIN + u8dataWriterIndex, UXR_PUBLISHER_ID);
    uint16_t publisher_req =
        uxr_buffer_create_publisher_xml(&session, gl_reliable_out[0], publisher_id, gl_participant_id, "", UXR_REPLACE);

    gl_datawriters_id[u8dataWriterIndex] =
        uxr_object_id(DDS_DATA_WRITER_ID_BEGIN + u8dataWriterIndex, UXR_DATAWRITER_ID);
    uint16_t datawriters_req = uxr_buffer_create_datawriter_xml(
        &session, gl_reliable_out[0], gl_datawriters_id[u8dataWriterIndex], publisher_id, szDataWriterXML, UXR_REPLACE);

    uint8_t status[2];
    uint16_t requests[2] = {publisher_req, datawriters_req};

    return uxr_run_session_until_all_status(&session, 1000, requests, status, sizeof(status));
}

static bool privCreateDataReader(const uint8_t u8dataReaderIndex, uxrSession &session, const char *szTopicName,
                                 TopicOperation &operation)
{
    bool bResult;
    char szDataReaderXML[sizeof(DDS_DATA_READER_XML) + TOPIC_MAX_LEN * 2];

    snprintf(szDataReaderXML, sizeof(szDataReaderXML), DDS_DATA_READER_XML, szTopicName, szTopicName);

    uxrObjectId subscriber_id = uxr_object_id(DDS_SUBSCRIBER_ID_BEGIN + u8dataReaderIndex, UXR_SUBSCRIBER_ID);
    uint16_t subscriber_req = uxr_buffer_create_subscriber_xml(&session, gl_reliable_out[0], subscriber_id,
                                                               gl_participant_id, "", UXR_REPLACE);

    gl_datareaders_id[u8dataReaderIndex] =
        uxr_object_id(DDS_DATA_READER_ID_BEGIN + u8dataReaderIndex, UXR_DATAREADER_ID);
    uint16_t datareaders_req =
        uxr_buffer_create_datareader_xml(&session, gl_reliable_out[0], gl_datareaders_id[u8dataReaderIndex],
                                         subscriber_id, szDataReaderXML, UXR_REPLACE);

    uint8_t status[2];
    uint16_t requests[2] = {subscriber_req, datareaders_req};

    bResult = uxr_run_session_until_all_status(&session, 1000, requests, status, sizeof(status));

    if (bResult)
    {
        uxrObjectIdRaw rawObjectID{0};

        uxr_object_id_to_raw(gl_datareaders_id[u8dataReaderIndex], (uint8_t *)&rawObjectID);
        gl_mapSubscriberTopicToFunc[rawObjectID] = operation.topicData.subscribe.pFnDeserializeTopic;
        gl_mapSubscriberCallbackArgs[rawObjectID] = {operation.topicData.subscribe.ePolicy, operation.xTopicQueue};
    }

    return bResult;
}

void SubscribeToTopic(QueueHandle_t xTopicQueue, const char *szTopicName, const QueuePolicy ePolicy,
                      fnDeserializeTopicCallBack pFnDeserialize)
{
    TopicOperation operation{};

    operation.eType = SUBSCRIBE;
    operation.xTopicQueue = xTopicQueue;
    operation.topicData.subscribe.ePolicy = ePolicy;
    operation.topicData.subscribe.pFnDeserializeTopic = pFnDeserialize;

    privAddOperationToQueue(operation, szTopicName);
}

void PublishATopic(QueueHandle_t xTopicQueue, const char *szTopicName, fnSerializeTopic pFnSerializeTopic)
{
    TopicOperation operation{};

    operation.eType = PUBLISH;
    operation.xTopicQueue = xTopicQueue;
    operation.topicData.publish.pFnSerializeTopic = pFnSerializeTopic;

    privAddOperationToQueue(operation, szTopicName);
}

int64_t GetDDSTimestamp()
{
    int64_t llTimestamp{0};
    taskENTER_CRITICAL();
    llTimestamp = uxr_epoch_nanos(&gl_session);
    taskEXIT_CRITICAL();
    return llTimestamp;
}

static void privAddOperationToQueue(TopicOperation &operation, const char *szTopicName)
{
    assert_and_log(operation.xTopicQueue != nullptr, "xTopicQueue is null");
    assert_and_log(strlen(szTopicName) < TOPIC_MAX_LEN, "Topic name too long");

    snprintf(operation.szTopicName, sizeof(char) * TOPIC_MAX_LEN, "%s", szTopicName);

    privWaitForQueueReady();

    xQueueSend(gl_xOperationQueue, static_cast<const void *>(&operation), portMAX_DELAY);
}

static void privWaitForQueueReady()
{
    // The queue isn't created yet, or it's currently full
    while ((gl_xOperationQueue == nullptr) || (uxQueueMessagesWaiting(gl_xOperationQueue) == DDS_MAX_TOPIC_OPERATION))
    {
        sleep(10);
    }
}

static uint16_t privHashTopicName(const char *szTopicName)
{
    etl::checksum<uint16_t> hash;

    for (uint32_t i = 0; szTopicName[i] != '\0'; ++i)
    {
        hash.add(szTopicName[i]);
    }

    return hash.value();
}

void privOnTopicReceived(uxrSession *session, uxrObjectId object_id, uint16_t request_id, uxrStreamId stream_id,
                         struct ucdrBuffer *ub, uint16_t length, void *args)
{
    uxrObjectIdRaw rawObjectID{0};
    uxr_object_id_to_raw(object_id, (uint8_t *)&rawObjectID);

    if (gl_mapSubscriberTopicToFunc.find(rawObjectID) != gl_mapSubscriberTopicToFunc.end())
    {
        auto pFunc = gl_mapSubscriberTopicToFunc[rawObjectID];
        (pFunc)(session, object_id, request_id, stream_id, ub, length, &(gl_mapSubscriberCallbackArgs[rawObjectID]));
    }
    else
    {
        // Probably data from an old gl_session
        LOG_DEBUG("DDS - Subscribe callback called from unknown topic");
    }
}
