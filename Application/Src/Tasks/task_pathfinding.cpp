//
// Created by ation on 19/12/2021.
//

#include "Tasks/task_pathfinding.h"

#include "logs.h"

#include "Pathfinding/navmesh.hpp"
#include "Pathfinding/sparse.hpp"

#include "event_groups.h"

#include <etl/array.h>

#define PATHFINDING_REQUEST_QUEUE_DEPTH 3
#define PATHFINDING_RESPONSE_QUEUE_DEPTH 64

using namespace navmesh;

void build_map_2022(NavMesh<128, 256, 128> &navmesh);

QueueHandle_t path_rqst_queue = NULL;
QueueHandle_t path_resp_queue = NULL;
static StaticQueue_t path_static_rqst_queue;
static StaticQueue_t path_static_resp_queue;

extern EventGroupHandle_t pathfinding_event_handle;

[[noreturn]] void StartPathFindingTask(void *parameters)
{
    static uint8_t path_rqst_queue_storage[PATHFINDING_REQUEST_QUEUE_DEPTH * sizeof(PathFindingRequest)];
    static uint8_t path_resp_queue_storage[PATHFINDING_RESPONSE_QUEUE_DEPTH * sizeof(PathFindingTarget)];

    path_rqst_queue = xQueueCreateStatic(PATHFINDING_REQUEST_QUEUE_DEPTH, sizeof(PathFindingRequest),
                                         path_rqst_queue_storage, &path_static_rqst_queue);
    path_resp_queue = xQueueCreateStatic(PATHFINDING_RESPONSE_QUEUE_DEPTH, sizeof(PathFindingTarget),
                                         path_resp_queue_storage, &path_static_resp_queue);

    assert_and_log(path_rqst_queue, "Unable to create pathfinding request queue");
    assert_and_log(path_resp_queue, "Unable to create pathfinding response queue");

    NavMesh<128, 256, 128> navmesh;

    TickType_t t0, t_build, t_path;

    t0 = xTaskGetTickCount();
    build_map_2022(navmesh);
    t_build = xTaskGetTickCount() - t0;

    LOG_INFO("navmesh build in %lu ms (points=%d, edges=%d, faces=%d)", t_build, navmesh.count_points(),
             navmesh.count_edges(), navmesh.count_faces());

    xEventGroupSetBits(pathfinding_event_handle, PATHFINDING_INIT_BIT);

    for (;;)
    {
        PathFindingRequest rqst;
        while (xQueueReceive(path_rqst_queue, &rqst, 100) == pdTRUE)
        {
            t0 = xTaskGetTickCount();

            etl::list<Coords, 128> path;
            navmesh.pathfinding(Coords{(Number)(rqst.start.x * 1000), (Number)(rqst.start.y * 1000)},
                                Coords{(Number)(rqst.target.x * 1000), (Number)(rqst.target.y * 1000)}, path, 1.0);

            for (const auto &target : path)
            {
                PathFindingTarget current_target = {.x = ((float)target[0] / 1000), .y = ((float)target[1] / 1000)};
                xQueueSend(path_resp_queue, &current_target, 0);
            }

            t_path = xTaskGetTickCount() - t0;

            LOG_INFO("request for path (%f, %f) to (%f, %f) found in %lu ms (length=%d)", rqst.start.x, rqst.start.y,
                     rqst.target.x, rqst.target.y, t_path, path.size());
        }
    }
}

void request_path(float start_x, float start_y, float target_x, float target_y)
{
    PathFindingRequest request = {.start = {.x = ((float)(start_x)), .y = ((float)(start_y))},
                                  .target = {.x = ((float)(target_x)), .y = ((float)(target_y))}};
    xQueueSend(path_rqst_queue, &request, 0);
}

bool get_target(PathFindingTarget *target, TickType_t tick_wait)
{
    return xQueueReceive(path_resp_queue, target, tick_wait) == pdTRUE;
}

bool has_target()
{
    PathFindingTarget target;
    return xQueuePeek(path_resp_queue, &target, (TickType_t)0) == pdTRUE;
}

void build_map_2022(NavMesh<128, 256, 128> &navmesh)
{
    etl::array<Coords, 18> border{
        Coords{200, 200},  Coords{250, 200},  Coords{250, 300},   Coords{1300, 300},  Coords{1300, 500},
        Coords{1700, 500}, Coords{1700, 300}, Coords{2750, 300},  Coords{2750, 200},  Coords{2800, 200},
        Coords{2800, 975}, Coords{2696, 975}, Coords{2698, 1509}, Coords{2407, 1800}, Coords{593, 1800},
        Coords{302, 1509}, Coords{302, 975},  Coords{200, 975},
    };
    etl::array<Coords, 8> object1{
        Coords{1020, 330}, Coords{1120, 430}, Coords{1120, 920}, Coords{1020, 1020},
        Coords{710, 1020}, Coords{610, 920},  Coords{610, 430},  Coords{710, 330},
    };
    etl::array<Coords, 8> object2{
        Coords{2290, 330},  Coords{2390, 430}, Coords{2390, 920}, Coords{2290, 1020},
        Coords{1980, 1020}, Coords{1880, 920}, Coords{1880, 430}, Coords{1980, 330},
    };
    etl::array<Coords, 8> object3{
        Coords{1200, 1050}, Coords{1300, 1150}, Coords{1300, 1600}, Coords{1200, 1700},
        Coords{750, 1700},  Coords{650, 1600},  Coords{650, 1150},  Coords{750, 1050},
    };
    etl::array<Coords, 8> object4{
        Coords{2250, 1050}, Coords{2350, 1150}, Coords{2350, 1600}, Coords{2250, 1700},
        Coords{1800, 1700}, Coords{1700, 1600}, Coords{1700, 1150}, Coords{1800, 1050},
    };

    navmesh.add_poly<18>(border);

    navmesh.add_poly<8>(object1);
    navmesh.add_poly<8>(object2);
    navmesh.add_poly<8>(object3);
    navmesh.add_poly<8>(object4);
}

void pathfinding_wait_init()
{
    EventBits_t uxBits = 0;

    while ((uxBits & PATHFINDING_INIT_BIT) == 0)
    {
        uxBits = xEventGroupWaitBits(pathfinding_event_handle, PATHFINDING_INIT_BIT, pdFALSE, pdFALSE, 1024);
    }
}
