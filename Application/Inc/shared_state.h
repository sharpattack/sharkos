#ifndef SHARED_STATE_H_INCLUDED
#define SHARED_STATE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

#include "FreeRTOS.h"
#include "task.h"
#include <stdbool.h>
#include <stdint.h>

/* Disable sensors now that can has been removed */
#define MAX_PROXIMITY 0

#define NUMBER_OF_SENSORS MAX_PROXIMITY

    typedef enum
    {
        BLUE_SIDE,
        GREEN_SIDE,
        // default value for when side is not known
        UNKNOWN_SIDE,
        // test side: used in test conditions, when absolute
        // coordinate of the robot in the table frame is not needed
        TEST_SIDE
    } Side;

    typedef struct
    {
        uint8_t available_sensors;
        struct
        {
            bool is_available;
            float distance; /* Distance in meters */
        } sensor[NUMBER_OF_SENSORS];
    } ProximitySensors;

#define LIDAR_SENSOR_PROXIMITY_ZONES_NB 8

    typedef struct
    {
        // * Proximity
        //   distance to the closest object
        float distance;
        //   does the lidar consider the closest object as too close ?
        bool an_object_is_close;
        // An array of min distance divided by angle zones. Zones are 1/8th
        // of a full angle, starting from 0 rad and increasing, as in the
        // following diagram:
        //
        //               y
        //               ^
        //            \  |  /
        //            3\2|1/0
        //           --- o ---> x
        //            4/5|6\7
        //            /  |  \
	//
        // The x and y axis are in the *local* coordinates of the
        // robot (CEPENDANT ! the robot has y in front and x right,
        // but the diagram assumed x front and y left. Front is 0/7,
        // back is 3/4. please advise.)
        float proximity[LIDAR_SENSOR_PROXIMITY_ZONES_NB];

        // * Pose estimation
        //   whether the pose estimation should be trusted or not (if
        //   not, the lidar might be lost because it lost too much
        //   beacons)
        bool pose_estim_is_valid;
        //   pose estimation
        float pose_estim_x;
        float pose_estim_y;
        float pose_estim_theta;

        // WARNING: the strat can DISABLE readings from the
        // lidar. This is done so we can ignore some known objects,
        // and is an ugly hack. If activated, get_shared_lidar_sensor
        // will return false for all proximity detection variables.
        bool disabled_by_strat;

    } LidarSensor;

    typedef struct
    {
        float x;
        float y;
        float theta;
    } Odometry;

    typedef struct
    {
        ProximitySensors proximity_sensors;
        LidarSensor lidar_sensor;
        Odometry odometry;
        Side side;
    } SharedState;

    SharedState *get_shared_state();

    void set_shared_odometry(Odometry *odometry);
    Odometry get_shared_odometry();

    LidarSensor get_shared_lidar_sensor();
    void strat_disable_shared_lidar_sensor();
    void strat_enable_shared_lidar_sensor();

    void set_shared_side(Side side);
    Side get_shared_side();

#ifdef __cplusplus
}
#endif

#endif // SHARED_STATE_H_INCLUDED
