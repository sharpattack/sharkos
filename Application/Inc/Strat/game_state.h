#ifndef GAME_STATE_H
#define GAME_STATE_H

#include <stdbool.h>
#include <stdint.h>

typedef struct
{

    // elapsed time (sec)
    float time_s;

    // total reward
    uint16_t earned_points;

} GameState;

#endif
