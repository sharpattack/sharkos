#ifndef ACTIONS_H
#define ACTIONS_H

#include "shared_state.h"
#include <stdbool.h>
#include <stdint.h>

#define ACTION_MAX 32
#define ACTION_NAME_LENGTH 32

#define CONCAT_(a, b) a##b
#define CONCAT(a, b) CONCAT_(a, b)
#define MOVE_RESULT_IDENTIFIER CONCAT(r_, __LINE__)
#define TRY_MOVE(action)                                                                                               \
    Result MOVE_RESULT_IDENTIFIER = action;                                                                            \
    if (MOVE_RESULT_IDENTIFIER != ACTION_SUCCESS)                                                                      \
    {                                                                                                                  \
        return MOVE_RESULT_IDENTIFIER;                                                                                 \
    }

typedef enum
{
    ACTION_SUCCESS,
    ACTION_ABORT,
    ACTION_FAIL
} Result;

typedef struct
{
    // action's name
    char *name;
    // expected duration in sec
    float duration_s;
    // expected reward
    uint16_t reward;
    // function pointer
    Result (*func)(Side);
    // was action performed ?
    bool performed;
    // how many tries have been spent trying to perform this action
    int tries_nb;
    // wether the action *must* be done or not
    bool critical;
} Action;

#define ACTION_REGISTER(func, duration, reward, critical) actions_add(#func, duration, reward, &func, critical)

void actions_add(char *name, float duration_s, uint16_t reward, Result (*func)(Side), bool critical);

extern Action ACTIONS[ACTION_MAX];
extern uint16_t ACTIONS_NB;

void actions_init();

#endif
