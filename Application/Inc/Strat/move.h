#ifndef MOVE_H
#define MOVE_H

#include "FreeRTOS.h"
#include "actions.h"
#include <portmacro.h>

#define MS_2_TICK(ms) ((TickType_t)ms * portTICK_PERIOD_MS)

#define CONCAT_(a, b) a##b
#define CONCAT(a, b) CONCAT_(a, b)
#define CANOPEN_RESULT_IDENTIFIER CONCAT(r_, __LINE__)

#define TRY_COCMD_OR_FAIL(CMD, FORMAT, ...)                                                                            \
    BaseType_t CANOPEN_RESULT_IDENTIFIER = CMD;                                                                        \
    if (CANOPEN_RESULT_IDENTIFIER != HAL_OK)                                                                           \
    {                                                                                                                  \
        LOG_WARNING(FORMAT, ##__VA_ARGS__);                                                                            \
        return ACTION_FAIL;                                                                                            \
    }

#define TRY_COCMD_OR_FAIL_(CMD)                                                                                        \
    BaseType_t CANOPEN_RESULT_IDENTIFIER = CMD;                                                                        \
    if (CANOPEN_RESULT_IDENTIFIER != HAL_OK)                                                                           \
    {                                                                                                                  \
        return ACTION_FAIL;                                                                                            \
    }

#define TRY_COCMD_OR_CONTINUE(CMD, FORMAT, ...)                                                                        \
    BaseType_t CANOPEN_RESULT_IDENTIFIER = CMD;                                                                        \
    if (CANOPEN_RESULT_IDENTIFIER != HAL_OK)                                                                           \
    {                                                                                                                  \
        LOG_WARNING(FORMAT, ##__VA_ARGS__);                                                                            \
        continue;                                                                                                      \
    }

#define WAIT_ABORT (1000 * portTICK_PERIOD_MS)

Result move_to(float target_x, float target_y, TickType_t timeout);
Result move_to_safe(float target_x, float target_y, TickType_t timeout);
Result move_of(float target_x, float target_y, TickType_t timeout);
Result move_of_safe(float target_x, float target_y, TickType_t timeout);
Result rotate_to(float target_theta, TickType_t timeout);
Result rotate_to_safe(float target_theta, TickType_t timeout);
Result rotate_of(float target_theta, TickType_t timeout);
Result rotate_of_safe(float target_theta, TickType_t timeout);
Result rotate_toward(float x, float y, TickType_t timeout);
Result rotate_toward_safe(float x, float y, TickType_t timeout);

// Mouvements 2024
// ---------------

// "smartly" try to take a plant in front of the robot
Result take_plant(TickType_t timeout);

Result place_garden_plant(TickType_t timeout);

// rotate a solar panel to the correct position
Result rotate_solar_panel_blue(TickType_t timeout);
Result rotate_solar_panel_yellow(TickType_t timeout);

Result up_floor1_elevator(TickType_t timeout);

Result push_collect(TickType_t timeout);

#endif
