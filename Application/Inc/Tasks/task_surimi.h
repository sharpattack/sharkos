#ifndef TASK_SURIMI_H_INCLUDED
#define TASK_SURIMI_H_INCLUDED

#include "portmacro.h"
#include <stdbool.h>
#include <stdint.h>
#ifdef __cplusplus
extern "C"
{
#endif
    typedef uint8_t Floor;

    _Noreturn void StartSurimiTask(void *parameters);
    bool Elevator_has_reached_position(Floor floor, int16_t heigth_mm);
    BaseType_t Elevator_move_to(Floor floor, int16_t heigth_mm, TickType_t timeout);
#ifdef __cplusplus
}
#endif

#endif // TASK_SURIMI_H_INCLUDED
