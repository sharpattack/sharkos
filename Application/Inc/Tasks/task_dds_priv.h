//
// Created by madahin on 08/08/2021.
//

#ifndef SHARKOS_TASK_DDS_PRIV_H
#define SHARKOS_TASK_DDS_PRIV_H

#include <functional>

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "projdefs.h"
#include "queue.h"

#include "etl/checksum.h"
#include "etl/map.h"
#include "etl/random.h"
#include "etl/vector.h"

/* Topic :
 *  - LidarProximity
 *  - PoseEstim
 *  - OdometryPoseEstim
 *  - PlayerSide
 *  - Proximity
 *  - Score
 *  - VisionInfo
 */
#define DDS_MAX_TOPIC 7
#define DDS_MAX_TOPIC_OPERATION 7

#define DDS_PUBLISHER_ID_BEGIN 0x1
#define DDS_DATA_WRITER_ID_BEGIN 0x1

#define DDS_SUBSCRIBER_ID_BEGIN 0x1
#define DDS_DATA_READER_ID_BEGIN 0x1

/* Topic publies :
 * - OdometryPoseEstim
 * - Proximity
 * - Score
 */
#define DDS_MAX_DATA_WRITER 3

/* Topic inscris :
 * - LidarProximity
 * - OdometryPoseEstim
 * - PlayerSide
 * - VisionInfo
 */
#define DDS_MAX_DATA_READER 4

#define DDS_MAX_COM_ERROR 3

#define DDS_HEARTBEAT_TIME_MS 500

struct TopicOperation
{
    OperationType eType;
    QueueHandle_t xTopicQueue;
    char szTopicName[TOPIC_MAX_LEN];
    union {
        struct
        {
            QueuePolicy ePolicy;
            fnDeserializeTopicCallBack *pFnDeserializeTopic;
        } subscribe;
        struct
        {
            fnSerializeTopic *pFnSerializeTopic;
        } publish;
    } topicData;
};

using uxrObjectIdRaw = uint16_t;

/****************************
 *        Shitty XML        *
 ****************************/

#define DDS_PARTICIPANT_XML                                                                                            \
    "<dds>"                                                                                                            \
    "<participant>"                                                                                                    \
    "<rtps>"                                                                                                           \
    "<name>SharkOS</name>"                                                                                             \
    "</rtps>"                                                                                                          \
    "</participant>"                                                                                                   \
    "</dds>"

#define DDS_TOPIC_XML                                                                                                  \
    "<dds>"                                                                                                            \
    "<topic>"                                                                                                          \
    "<name>%sTopic</name>"                                                                                             \
    "<dataType>%s</dataType>"                                                                                          \
    "</topic>"                                                                                                         \
    "</dds>"

#define DDS_DATA_WRITER_XML                                                                                            \
    "<dds>"                                                                                                            \
    "<data_writer>"                                                                                                    \
    "<topic>"                                                                                                          \
    "<kind>NO_KEY</kind>"                                                                                              \
    "<name>%sTopic</name>"                                                                                             \
    "<dataType>%s</dataType>"                                                                                          \
    "</topic>"                                                                                                         \
    "</data_writer>"                                                                                                   \
    "</dds>"

#define DDS_DATA_READER_XML                                                                                            \
    "<dds>"                                                                                                            \
    "<data_reader>"                                                                                                    \
    "<topic>"                                                                                                          \
    "<kind>NO_KEY</kind>"                                                                                              \
    "<name>%sTopic</name>"                                                                                             \
    "<dataType>%s</dataType>"                                                                                          \
    "</topic>"                                                                                                         \
    "</data_reader>"                                                                                                   \
    "</dds>"

#endif // SHARKOS_TASK_DDS_PRIV_H
