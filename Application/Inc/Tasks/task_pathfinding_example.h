//
// Created by ation on 19/12/2021.
//

#ifndef PATHFINDING_EXAMPLE_H_INCLUDED
#define PATHFINDING_EXAMPLE_H_INCLUDED

#include "config.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef BUILD_PATHFINDING_EXAMPLE
#ifdef __cplusplus
    [[noreturn]]
#else
    _Noreturn
#endif

    void
    StartPathFindingExampleTask(void *parameters);
#endif

#ifdef __cplusplus
}
#endif

#endif // PATHFINDING_EXAMPLE_H_INCLUDED
