#ifndef DDS_H_INCLUDED
#define DDS_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "projdefs.h"
#include "queue.h"
// #include "stm32f4xx_hal.h"

// #include <uxr/client/profile/transport/serial/serial_transport_platform.h>
// #include <uxr/client/profile/transport/serial/serial_transport_external.h>
#include <ucdr/microcdr.h>
#include <uxr/client/client.h>
#include <uxr/client/util/ping.h>

#include "logs.h"

#define DDS_STREAM_HISTORY 4
#define DDS_BUFFER_SIZE UXR_CONFIG_SERIAL_TRANSPORT_MTU *DDS_STREAM_HISTORY
#define TOPIC_MAX_LEN 15

#define _ASSERT_TOPIC_NAME_SIZE(topicName) compileTimeASSERT(sizeof(#topicName) - 1 < TOPIC_MAX_LEN)

    typedef enum
    {
        SUBSCRIBE,
        PUBLISH
    } OperationType;

    typedef enum
    {
        CIRCULAR,   //< Use the queue as a circular buffer. It means that if the queue is full, older data are discarded
        LAST_ONLY,  //< Use only the first emplacement of the queue, which will contain the most recent data
        BEST_EFFORT //< Don't add data to the queue if it's full
    } QueuePolicy;

    typedef enum
    {
        DDS_SER_SUCCESS,
        DDS_SER_FAILURE,
        DDS_SER_NOTHING
    } DDSSerializeResult;

    typedef struct
    {
        QueuePolicy policy;
        xQueueHandle xQueue;
    } privCallBackArgs;

    typedef DDSSerializeResult fnSerializeTopic(QueueHandle_t, uxrStreamId *, uxrObjectId *, uxrSession *);
    typedef void fnDeserializeTopicCallBack(uxrSession *session, uxrObjectId object_id, uint16_t request_id,
                                            uxrStreamId stream_id, struct ucdrBuffer *ub, uint16_t length, void *args);

    struct vDDSTaskParameters
    {
        void* selected_uart;
    };

// Hack to appease both C and C++ compiler
#ifdef __cplusplus
    [[noreturn]]
#else
_Noreturn
#endif
    void
    StartDDSTask(void *parameters);

    void SubscribeToTopic(QueueHandle_t xTopicQueue, const char *szTopicName, const QueuePolicy ePolicy,
                          fnDeserializeTopicCallBack pFnDeserialize);
    void PublishATopic(QueueHandle_t xTopicQueue, const char *szTopicName, fnSerializeTopic pFnSerializeTopic);

#ifdef __cplusplus
    [[nodiscard]]
#endif
    int64_t
    GetDDSTimestamp();

    /*
     * bool HelloWorld_serialize_topic(struct ucdrBuffer* writer, const HelloWorld* topic);
    bool HelloWorld_deserialize_topic(struct ucdrBuffer* reader, HelloWorld* topic);
    uint32_t HelloWorld_size_of_topic(const HelloWorld* topic, uint32_t size);
     */

#define DDS_PREPARE_PUBLISH_TOPIC(topicName)                                                                           \
    DDSSerializeResult privAnonSerialize_##topicName(QueueHandle_t queueHandle, uxrStreamId *stream,                   \
                                                     uxrObjectId *datawriter_id, uxrSession *session)                  \
    {                                                                                                                  \
        _ASSERT_TOPIC_NAME_SIZE(topicName);                                                                            \
        topicName topic;                                                                                               \
        DDSSerializeResult res = DDS_SER_NOTHING;                                                                      \
        if (xQueueReceive(queueHandle, &topic, 0) == pdTRUE)                                                           \
        {                                                                                                              \
            LOG_INFO("Send topic " #topicName)                                                                         \
            ucdrBuffer ub;                                                                                             \
            uint32_t topic_size = topicName##_size_of_topic(&topic, 0);                                                \
            uxr_prepare_output_stream(session, *stream, *datawriter_id, &ub, topic_size);                              \
            topicName##_serialize_topic(&ub, &topic);                                                                  \
            res = (uxr_run_session_until_confirm_delivery(session, 1000) ? DDS_SER_SUCCESS : DDS_SER_FAILURE);         \
        }                                                                                                              \
        return res;                                                                                                    \
    }

#define DDS_PREPARE_SUBSCRIBE_TOPIC(topicName)                                                                         \
    void privAnonDeserialize_##topicName(uxrSession *session, uxrObjectId object_id, uint16_t request_id,              \
                                         uxrStreamId stream_id, struct ucdrBuffer *ub, uint16_t length, void *args)    \
    {                                                                                                                  \
        _ASSERT_TOPIC_NAME_SIZE(topicName);                                                                            \
        (void)session;                                                                                                 \
        (void)object_id;                                                                                               \
        (void)request_id;                                                                                              \
        (void)stream_id;                                                                                               \
        (void)length;                                                                                                  \
        privCallBackArgs *callbackargs = (privCallBackArgs *)args;                                                     \
        topicName topic;                                                                                               \
        topicName##_deserialize_topic(ub, &topic);                                                                     \
        switch (callbackargs->policy)                                                                                  \
        {                                                                                                              \
        case CIRCULAR: {                                                                                               \
            topicName trash;                                                                                           \
            if (uxQueueSpacesAvailable(callbackargs->xQueue) == 0)                                                     \
            {                                                                                                          \
                xQueueReceive(callbackargs->xQueue, &trash, 0);                                                        \
            }                                                                                                          \
            xQueueSend(callbackargs->xQueue, &topic, 0);                                                               \
            break;                                                                                                     \
        }                                                                                                              \
        case LAST_ONLY: {                                                                                              \
            xQueueOverwrite(callbackargs->xQueue, &topic);                                                             \
            break;                                                                                                     \
        }                                                                                                              \
        case BEST_EFFORT: {                                                                                            \
            xQueueSend(callbackargs->xQueue, &topic, 0);                                                               \
            break;                                                                                                     \
        }                                                                                                              \
        }                                                                                                              \
    }

#define PRIV_DDS_INIT_TOPIC(topicName, queueSize)                                                                      \
    QueueHandle_t privDDSQueue_##topicName = NULL;                                                                     \
    static StaticQueue_t privStaticDDSQueue_##topicName;                                                               \
    uint8_t privDDSQueueStorageArea_##topicName[(queueSize) * sizeof(topicName)];

#define PRIV_DDS_INIT_TOPIC_QUEUE(topicName, queueSize)                                                                \
    privDDSQueue_##topicName = xQueueCreateStatic((queueSize), sizeof(topicName), privDDSQueueStorageArea_##topicName, \
                                                  &privStaticDDSQueue_##topicName);

#define PRIV_DDS_INIT_PUBLISH_TOPIC(topicName, queueSize)                                                              \
    PRIV_DDS_INIT_TOPIC_QUEUE(topicName, queueSize)                                                                    \
    PublishATopic((privDDSQueue_##topicName), (#topicName), (privAnonSerialize_##topicName));

#define PRIV_DDS_INIT_SUBSCRIBE_TOPIC(topicName, queueSize, policy)                                                    \
    PRIV_DDS_INIT_TOPIC_QUEUE(topicName, queueSize)                                                                    \
    SubscribeToTopic((privDDSQueue_##topicName), (#topicName), policy, privAnonDeserialize_##topicName);

#define DDS_INIT_PUBLISH_TOPIC(topicName, queueSize)                                                                   \
    PRIV_DDS_INIT_TOPIC(topicName, queueSize)                                                                          \
    PRIV_DDS_INIT_PUBLISH_TOPIC(topicName, queueSize)

#define DDS_INIT_SUBSCRIBE_TOPIC(topicName, queueSize, policy)                                                         \
    PRIV_DDS_INIT_TOPIC(topicName, queueSize)                                                                          \
    PRIV_DDS_INIT_SUBSCRIBE_TOPIC(topicName, queueSize, policy)

#define DDS_PUBLISH_TOPIC_BEST_EFFORT(topicName, topic) xQueueSend((privDDSQueue_##topicName), (topic), 0)

#define DDS_PUBLISH_TOPIC_RELIABLE(topicName, topic) xQueueSend((privDDSQueue_##topicName), (topic), portMAX_DELAY)

#define DDS_PUBLISH_TOPIC_TIMEOUT(topicName, topic, timeout)                                                           \
    xQueueSend((privDDSQueue_##topicName), (topic), (timeout) / portTICK_PERIOD_MS)

#define DDS_RETRIEVE_TOPIC_TIMEOUT(topicName, topic, timeout)                                                          \
    xQueueReceive((privDDSQueue_##topicName), (topic), (timeout) / portTICK_PERIOD_MS)

#define DDS_RETRIEVE_TOPIC_RELIABLE(topicName, topic) xQueueReceive((privDDSQueue_##topicName), (topic), portMAX_DELAY)

#define DDS_RETRIEVE_TOPIC_BEST_EFFORT(topicName, topic) xQueueReceive((privDDSQueue_##topicName), (topic), 0)

#ifdef __cplusplus
}
#endif

#endif // DDS_H_INCLUDED
