#include <sys/cdefs.h>

#ifndef STRAT_H_INCLUDED
#define STRAT_H_INCLUDED

#include "FreeRTOS.h"
// #include "cmsis_os.h"
#include "projdefs.h"
// #include "stm32f4xx_hal.h"
// #include "helpers.h"

#define MATCH_DURATION_S 95.f
#define MATCH_DURATION_MS 100000
#define MAX_TRIES_NB 3

_Noreturn void StartStratTask(void *parameters);

#endif // STRAT_H_INCLUDED
