#ifndef TASK_CTRL_H
#define TASK_CTRL_H
#include "FreeRTOS.h"
#include <stdbool.h>

#define CTRL_MATCH_START_BIT (0b00000001)
#define CTRL_INIT_BIT (0b00000010)
#define CTRL_MOVE_COMPLETED_BIT (0b00000100)

#ifdef __cplusplus
extern "C"
{
#endif

    typedef struct vCtrlTaskParameters vCtrlTaskParameters;
    struct vCtrlTaskParameters
    {
    };

    void ctrlrTask_GPIO_Interrupt_Handler(uint16_t GPIO_Pin);
    void startCtrlTask(void *parameters);

    void ctrl_wait_init();
    void ctrl_wait_match_start();
    bool ctrl_wait_move_completed(TickType_t wait_tick);

#ifdef __cplusplus
}
#endif

#endif
