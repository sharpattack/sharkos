//
// Created by madahin on 25/08/2021.
//

#ifndef SHARKOS_TASK_DDS_EXAMPLE_H
#define SHARKOS_TASK_DDS_EXAMPLE_H

#include "config.h"

#ifdef BUILD_DDS_PUBLISH_EXAMPLE
_Noreturn void StartHelloWorldPublisherTask(void *parameters);
#endif

#ifdef BUILD_DDS_SUBSCRIBE_EXAMPLE
_Noreturn void StartHelloWorldSubscriberTask(void *parameters);
#endif

#endif // SHARKOS_TASK_DDS_EXAMPLE_H
