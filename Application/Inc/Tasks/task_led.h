#include <sys/cdefs.h>

#ifndef LED_H_INCLUDED
#define LED_H_INCLUDED

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "projdefs.h"
#include "stm32f4xx_hal.h"

#define LED_BLINK_MS 500

struct vLEDTaskParameters
{
    GPIO_TypeDef *gpio_port;
    uint16_t gpio_pin;
};

_Noreturn void vStartLedTask(void *parameters);

#endif // LED_H_INCLUDED
