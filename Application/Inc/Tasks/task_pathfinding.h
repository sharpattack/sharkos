//
// Created by ation on 19/12/2021.
//

#ifndef PATHFINDING_H_INCLUDED
#define PATHFINDING_H_INCLUDED

#define PATHFINDING_INIT_BIT (0b00000001)

#ifdef __cplusplus
extern "C"
{
#endif

#include "FreeRTOS.h"
#include "queue.h"

    struct vPathFindingTaskParameters
    {
        QueueHandle_t request;
        QueueHandle_t response;
    };

    void pathfinding_wait_init();

    typedef struct
    {
        struct
        {
            float x;
            float y;
        } start;
        struct
        {
            float x;
            float y;
        } target;
    } PathFindingRequest;

    typedef struct
    {
        float x;
        float y;
    } PathFindingTarget;

    extern QueueHandle_t path_rqst_queue;
    extern QueueHandle_t path_resp_queue;

    void request_path(float start_x, float start_y, float target_x, float target_y);

#ifdef __cplusplus
    bool get_target(PathFindingTarget *target, TickType_t tick_wait = 0);
#else
bool get_target(PathFindingTarget *target, TickType_t tick_wait);
#endif

    bool has_target();

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __cplusplus
    [[noreturn]]
#else
_Noreturn
#endif
    void
    StartPathFindingTask(void *parameters);

#ifdef __cplusplus
}
#endif

#endif // PATHFINDING_H_INCLUDED
