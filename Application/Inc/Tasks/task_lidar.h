#ifndef LIDAR_H_INCLUDED
#define LIDAR_H_INCLUDED

#define LIDAR_TASK_SLEEP_DURATION 50

_Noreturn void StartLidarTask(void *parameters);

#endif // LIDAR_H_INCLUDED
