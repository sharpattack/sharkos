#ifndef CTRLR_HPP
#define CTRLR_HPP

#include "Control/message.h"
#include "FreeRTOS.h"
#include "event_groups.h"
#include "kinematics.h"
#include "pid.h"
#include "queue.h" // FreeRTOS
#include "scurve.h"
#include "shared_state.h"
#include <cmath>
#include <etl/fsm.h>
#include <etl/map.h>
#include <etl/message.h>
#include <etl/message_types.h>
#include <etl/queue.h>

#define K_C 0.036f // Nm/A
#define CMD_QUEUE_SIZE 8

#include <math.h>

/// NEEED REFACTO INTERNAL TO EXTERNAL
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    //

    typedef struct
    {
        float kp;
        float ti;
        float td;
        float N;
    } ctrlr_pid_parameters_t;

    typedef struct
    {
        float torque_max;               // Max torque applied to wheels [N.m]
        float torqueWhenRotatation_max; // Max toque applied to wheels

        float xDot_max;    // Front max speed [m/s]
        float xDotDot_max; // Front max accel [m/s^2]

        float w_max;    // Rotation max speed [rad/s]
        float wDot_max; // Rotation max accel [m/s^2]
    } ctrlr_physical_parameters_t;

    // Ctrlr class constants inits
    const ctrlr_pid_parameters_t DEFAULT_PID_ANGLE = {.kp = 1.95f, .ti = 1.4f, .td = 0.034f, .N = 2.0f};
    const ctrlr_pid_parameters_t DEFAULT_PID_DIST = {.kp = 2.2f, .ti = 1.8f, .td = 0.1f, .N = 3.0f};

    const ctrlr_physical_parameters_t DEFAULT_PHY = {
        .torque_max = 0.0725f,
        .torqueWhenRotatation_max = 0.0725f,
        .xDot_max = 1.2f,
        .xDotDot_max = 0.5f,
        .w_max = 3.0 * M_PI / 2.0f,
        .wDot_max = 3.5f,
    };

    const ctrlr_physical_parameters_t SAFE_PHY = {
        .torque_max = 0.0435f,
        .torqueWhenRotatation_max = 0.04f,
        .xDot_max = 1.0f,
        .xDotDot_max = 0.30f,
        .w_max = 3.0f * M_PI / 2.0f,
        .wDot_max = 3.0f * M_PI / 2.0f,
    };
#ifdef __cplusplus
}

/**
 *     Value-Time constraint
 *     Return true if the value remain between
 *     LOW_TH and HIGH_TH during ELAPSED_TIME [s].
 *     Return false otherwise
 */
#define REGISTER_VT_WINDOW(NAME, LOW_TH, HIGH_TH, ELAPSED_TIME)                                                        \
    inline bool IS_VT_CONSTRAINED_##NAME(float value, float t)                                                         \
    {                                                                                                                  \
        static float t0 = 0;                                                                                           \
        static bool latch = false;                                                                                     \
        if ((float)LOW_TH <= value && value <= (float)HIGH_TH)                                                         \
        {                                                                                                              \
            if (!latch)                                                                                                \
            {                                                                                                          \
                latch = true;                                                                                          \
                t0 = t;                                                                                                \
            }                                                                                                          \
            if (t - t0 >= (float)ELAPSED_TIME)                                                                         \
            {                                                                                                          \
                return true;                                                                                           \
            }                                                                                                          \
        }                                                                                                              \
        else                                                                                                           \
        {                                                                                                              \
            latch = false;                                                                                             \
        }                                                                                                              \
        return false;                                                                                                  \
    }

namespace NSCtrlr
{

enum Direction
{
    Forward,
    Backward
};

struct Output
{
    float u_1;
    float u_2;
};

class Ctrlr
{

  public:
    // NOTE: This class should be implemented as a singleton.
    Ctrlr(float ts, QueueHandle_t dds_queue);
    virtual ~Ctrlr() = default;

    // Accessors
    [[nodiscard]] inline Output getOutput() const
    {
        return m_output;
    }

    [[nodiscard]] inline Kinematics::Pose getPose() const
    {
        return m_pose;
    }

    [[nodiscard]] inline float getTime() const
    {
        return m_time;
    }

    // Ctrlr
    [[nodiscard]] float computeReference(float x_r, float y_r) const;
    void setCtrlSpeedMode(CTRL_SPEED_MODE speed_mode);
    void reset();
    void step();

    inline void disableRotationCtrl()
    {
        m_isRotationEnabled = false;
    }
    inline void enableRotationCtrlr()
    {
        m_isRotationEnabled = true;
    }
    inline void disableTranslationCtrl()
    {
        m_isTranslationEnabled = false;
    }
    inline void enableTranslationCtrl()
    {
        m_isTranslationEnabled = true;
    }

    float interpolateVMax(float dist);
    float interpolateWMax(float theta);

    QueueHandle_t dds_queue;

    float m_time;

    PID m_pidAngle;
    PID m_pidDist;

    ctrlr_physical_parameters_t m_phy;

    float m_ts; // Sampling time
    float m_timeout;

    bool m_isStable;
    bool m_isTranslationEnabled;
    bool m_isRotationEnabled;

    Kinematics::Pose m_pose;
    Kinematics::Pose m_poseDot;
    Kinematics::Pose m_ref_pose;
    Output m_output;

    Direction m_direction;
};

constexpr etl::message_router_id_t MOTOR_CONTROL = 0;

// Events
struct EventId
{
    enum
    {
        MOVE,
        ROTATE,
        SET_ROTATION_TARGET,
        SET_MOVE_TARGET,
        ABORT,
        STEP
    };
};

class Abort : public etl::message<EventId::ABORT>
{
};

class Move : public etl::message<EventId::MOVE>
{
};

class Rotate : public etl::message<EventId::ROTATE>
{
};

class SetRotationTarget : public etl::message<EventId::SET_ROTATION_TARGET>
{
  public:
    const float requested_theta;

    explicit SetRotationTarget(float theta) : requested_theta(theta)
    {
    }
};

class SetMoveTarget : public etl::message<EventId::SET_MOVE_TARGET>
{
  public:
    const float requested_x;
    const float requested_y;

    SetMoveTarget(float x, float y) : requested_x(x), requested_y(y)
    {
    }
};

class Step : public etl::message<EventId::STEP>
{
};

// States
struct StateId
{
    enum
    {
        INIT,
        IDLE,
        MOVING,
        ROTATING,
        NUMBER_OF_STATES
    };
};

class CtrlStateMachine : public etl::fsm
{
  public:
    Ctrlr ctrlr;
    EventGroupHandle_t ctrl_event_handle;
    SharedState *shared_state;

    CtrlStateMachine(EventGroupHandle_t ctrl_event_handle, float ts, QueueHandle_t dds_queue)
        : fsm(MOTOR_CONTROL), ctrlr(ts, dds_queue), ctrl_event_handle(ctrl_event_handle),
          shared_state(get_shared_state())
    {
    }

    void log_unknown_event(const etl::fsm_state_id_t state, const etl::imessage &msg) const;
    void notify_success();
    [[nodiscard]] bool is_object_close() const;
    [[nodiscard]] bool lidar_says_object_is_in_front() const;
    [[nodiscard]] bool lidar_says_object_is_behind() const;
};

class Init : public etl::fsm_state<CtrlStateMachine, Init, StateId::INIT, Step>
{
  public:
    etl::fsm_state_id_t on_enter_state() override;
    etl::fsm_state_id_t on_event_unknown(const etl::imessage &msg);
    etl::fsm_state_id_t on_event(const Step &event);
};

class Idle : public etl::fsm_state<CtrlStateMachine, Idle, StateId::IDLE, Move, Rotate, Abort, Step>
{
  public:
    etl::fsm_state_id_t on_event(const Step &event);
    [[nodiscard]] etl::fsm_state_id_t on_event(const Move &event) const;
    [[nodiscard]] etl::fsm_state_id_t on_event(const Rotate &event) const;
    [[nodiscard]] etl::fsm_state_id_t on_event(const Abort &event) const;
    etl::fsm_state_id_t on_event_unknown(const etl::imessage &msg);
};

class Moving : public etl::fsm_state<CtrlStateMachine, Moving, StateId::MOVING, Abort, SetMoveTarget, Step>
{
  public:
    SCurve<2> scurve;
    bool is_angle_control_disabled;

    Moving()
        : scurve(0, 0, 0, 0), // Managed on on_enter_state
          is_angle_control_disabled(false)
    {
    }
    etl::fsm_state_id_t on_enter_state() override;
    etl::fsm_state_id_t on_event(const SetMoveTarget &event);
    etl::fsm_state_id_t on_event(const Step &event);
    etl::fsm_state_id_t on_event(const Abort &event);
    etl::fsm_state_id_t on_event_unknown(const etl::imessage &msg);

  private:
    void init_scurve(float requested_x, float requested_y);
};

class Rotating : public etl::fsm_state<CtrlStateMachine, Rotating, StateId::ROTATING, Abort, SetRotationTarget, Step>
{
  public:
    SCurve<1> scurve;

    Rotating() : scurve(0, 0, 0, 0) // Managed on on_enter_state
    {
    }

    etl::fsm_state_id_t on_enter_state() override;
    etl::fsm_state_id_t on_event(const SetRotationTarget &event);
    etl::fsm_state_id_t on_event(const Step &event);
    etl::fsm_state_id_t on_event(const Abort &event);
    etl::fsm_state_id_t on_event_unknown(const etl::imessage &msg);

  private:
    void init_scurve(float requested_theta);
};

} // namespace NSCtrlr

#endif

#endif
