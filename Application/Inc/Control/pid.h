#ifndef PID_HPP
#define PID_HPP
#include <limits>

enum AntiWindupScheme
{
    ANTI_WINDUP_SCHEME_INTEGRATOR_RANGE,
    ANTI_WINDUP_SCHEME_CONDITIONAL_INTEGRATION,
    ANTI_WINDUP_SCHEME_BACK_CALCULATION
};

class PID
{
  public:
    static float NO_I;
    static float NO_D;

    explicit PID(float Ts, float kp = 0, float ti = NO_I, float td = NO_D, float N = 0, float y0 = 0,
                 float u_min = -std::numeric_limits<float>::infinity(),
                 float u_max = std::numeric_limits<float>::infinity());

    float step(float ref, float y);
    void reset();
    void resetIntegrator();

    void setOutputFeedback(float u)
    {
        m_u_sat = u;
    }

    inline void setAntiWindupScheme(AntiWindupScheme scheme)
    {
        m_aw_scheme = scheme;
    }

    inline void setWindupThreshold(float threshold)
    {
        m_windupThreshold = threshold;
    }

    inline void setTimeStep(float ts)
    {
        m_ts = ts;
    }

    inline void setInternalSaturation(bool enabled)
    {
        m_internal_saturation = enabled;
    }

  protected:
    void antiWindUp();

    float m_kp, m_td, m_ti, m_N; // m_N is the derivative filter coefficent
    float m_ts;                  // sampling time
    float m_windupThreshold;     // integrator windup threshold

    // State memories
    float m_eps[2];

  public:
    float m_ui[2];

  protected:
    float m_ud[2];
    float m_y[2];

    // Windup management
    bool m_internal_saturation;
    float m_u_max;
    float m_u_min;
    float m_u[2];  // Needed for saturation management
    float m_u_sat; // Output feedback
    float m_tt;    // antiwindup coefficient for back calculation

    AntiWindupScheme m_aw_scheme;
};

#endif
