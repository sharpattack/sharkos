#ifndef KINEMATICS_H
#define KINEMATICS_H

#ifdef __cplusplus
extern "C"
{
#endif

    constexpr float K_L = 0.340f;                   // [m]
    constexpr float K_D = 0.071f;                   // [m]
    constexpr float K_KS = 0.00153398078788564123f; // [rad/count]
    constexpr float K_N = 4096.f;                   // [count/rev]
    constexpr float K_TS = 0.005f;

    namespace Kinematics
    {
    struct Pose
    {
        float x;
        float y;
        float theta;
    };

    [[nodiscard]] float toRad(float angle);
    [[nodiscard]] float toDeg(float angle);
    [[nodiscard]] float wrapToPi(float angle);
    [[nodiscard]] float wrapToPi_2(float angle);
    void updatePose(Pose &pose, Pose &poseDot);
    } // namespace Kinematics

#ifdef __cplusplus
}
#endif

#endif
