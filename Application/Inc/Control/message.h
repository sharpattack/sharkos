#ifndef CTRLR_MESSAGE_H
#define CTRLR_MESSAGE_H

#ifdef __cplusplus
extern "C"
{
#endif
    typedef enum
    {
        NORMAL_MODE = 0, // High speed, high accel (too high in angular). Works well in most cases.
        SAFE_MODE = 1    // Low speed mode, low torque. Will not drift.
    } CTRL_SPEED_MODE;

    void ctrl_move_to(float x, float y, CTRL_SPEED_MODE mode);
    void ctrl_move_of(float x, float y, CTRL_SPEED_MODE mode);
    void ctrl_rotate_to(float theta, CTRL_SPEED_MODE mode);
    void ctrl_rotate_of(float theta, CTRL_SPEED_MODE mode);
    void ctrl_rotate_toward(float x, float y, CTRL_SPEED_MODE mode);
    void ctrl_abort();
    void ctrl_stop();

#ifdef __cplusplus
}
#endif

#endif

#ifdef __cplusplus
namespace NSCtrlr
{
struct Message
{
    enum Type
    {
        MESSAGE_TYPE_MOVE_TO,
        MESSAGE_TYPE_MOVE_OF,
        MESSAGE_TYPE_ROTATE_TO,
        MESSAGE_TYPE_ROTATE_OF,
        MESSAGE_TYPE_ROTATE_TOWARD,
        MESSAGE_TYPE_ABORT,
        MESSAGE_TYPE_STOP,
        MESSAGE_TYPE_SIZE
    };

    Type type;
    union {
        struct
        {
            float x;
            float y;
        } pos;
        float theta;
    };
    CTRL_SPEED_MODE speed_mode;
};

} // namespace NSCtrlr

#endif
