#ifndef SCURVE_HPP
#define SCURVE_HPP

#include "etl/array.h"
#include <cmath>
#include <stdint.h>

namespace NSCtrlr
{

template <uint8_t N> class SCurve
{
    typedef etl::array<float, N> pointN_t;

  public:
    SCurve(float ts, float posDot_max, float posDotDot_max, float eps = 1e-1f)
        : m_ts{ts}, m_t0{0}, m_t{0}, m_x_i{0}, m_x_f{0}, m_xDot_max{posDot_max}, m_xDotDot_max{posDotDot_max}, m_x_r{0},
          m_latch{false}, m_latchDuration{0}, m_eps{eps}, m_tr{0} {};

    void init(pointN_t x_i, pointN_t x_f, float t0)
    {
        // Phase duration calculation
        m_t0 = t0;
        m_x_i = pointN_t(x_i);
        m_x_f = pointN_t(x_f);
        m_latch = false;
        m_latchDuration = 1.0f;
        correctSplineDuration();
    };

    void step(float t)
    {
        float tau = (t - m_t0) / m_t;
        m_x_r = polynom(tau);
    };

    [[nodiscard]] inline pointN_t getOutput() const
    {
        return m_x_r;
    };

    bool isStable(pointN_t x, float t)
    {
        if (pointN_norm2(m_x_f, x) <= m_eps)
        {
            if (!m_latch)
            {
                m_latch = true;
                m_tr = t;
            }
            if (t - m_tr >= m_latchDuration)
            {
                return true;
            }
        }
        else
        {
            m_latch = false;
        }
        return false;
    };

    [[nodiscard]] inline pointN_t getTarget() const
    {
        return m_x_f;
    };

  protected:
    void correctSplineDuration()
    {
        if (pointN_norm2(m_x_i, m_x_f) < 1e-4 // I don't trust float too
            || m_xDot_max <= 0 || m_xDotDot_max <= 0)
        {
            return; // Dropping scaling due input error
        }

        const float tau_xDot_max = 0.5f;
        const float tau_xDotDot_max = 0.5f - 1.0f / (2.0f * sqrtf(3.0f));

        // Find the longest m_x_i - m_x_f dimension and computes max accel according to it
        float dist = pointN_norm2(m_x_i, m_x_f);

        const float scurveDot_max_at_taumax_norm = polynomDot(tau_xDot_max);
        const float scurveDotDot_max_at_taumax_norm = polynomDotDot(tau_xDotDot_max);

        float scurveDotDot_norm =
            (scurveDotDot_max_at_taumax_norm * powf(m_xDot_max, 2)) / (powf(scurveDot_max_at_taumax_norm, 2));

        if (scurveDotDot_norm > m_xDotDot_max)
        {
            m_t = sqrtf(scurveDotDot_max_at_taumax_norm / (powf(scurveDot_max_at_taumax_norm, 2) * m_xDotDot_max)) *
                  scurveDot_max_at_taumax_norm;
        }
        else
        {
            m_t = scurveDot_max_at_taumax_norm / m_xDot_max;
        }
    };

    float pointN_norm2(pointN_t p1, pointN_t p2) const
    {
        float norm2 = 0;
        for (int i = 0; i < N; i++)
        {
            norm2 += powf(p1[i] - p2[i], 2);
        }
        return sqrtf(norm2);
    }

    float pointN_norm2(pointN_t p) const
    {
        float norm2 = 0;
        for (int i = 0; i < N; i++)
        {
            norm2 += powf(p[i], 2);
        }
        return sqrtf(norm2);
    }

    pointN_t polynom(const float tau) const
    {
        pointN_t p;
        for (uint8_t i = 0; i < N; i++)
        {
            if (tau <= 1)
                p[i] = m_x_i[i] +
                       (m_x_f[i] - m_x_i[i]) * (10.0f * powf(tau, 3) - 15.0f * powf(tau, 4) + 6.0f * powf(tau, 5));
            else
                p[i] = m_x_f[i];
        }
        return p;
    };

    float polynomDot(const float tau) const
    {
        pointN_t p;
        for (uint8_t i = 0; i < N; i++)
        {
            if (tau <= 1)
                p[i] = (m_x_f[i] - m_x_i[i]) * (30.0f * powf(tau, 2) - 60.0f * powf(tau, 3) + 30.0f * powf(tau, 4));
            else
                p[i] = 0;
        }
        return pointN_norm2(p);
    };

    /// @param dim Dim to compute polyDotDot. Because we only need to compute the max amax for the longest dim.
    float polynomDotDot(const float tau) const
    {
        pointN_t p;
        for (uint8_t i = 0; i < N; i++)
        {
            if (tau <= 1)
                p[i] = (m_x_f[i] - m_x_i[i]) * (60.0f * tau - 180.0f * powf(tau, 2) + 120.0f * powf(tau, 3));
            else
                p[i] = 0;
        }
        return pointN_norm2(p);
    };

  protected:
    float m_ts;
    float m_t0;
    float m_t; // Phase duration

    pointN_t m_x_i, m_x_f;
    float m_xDot_max, m_xDotDot_max;

    pointN_t m_x_r;

    // curve analysis
    bool m_latch;
    float m_latchDuration;
    float m_eps; // Tolerance
    float m_tr;  // response time with respect to eps threshold
};

} // namespace NSCtrlr

#endif
