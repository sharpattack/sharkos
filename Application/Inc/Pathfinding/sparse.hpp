#ifndef NAVMESH_SPARCE_INCLUDED
#define NAVMESH_SPARCE_INCLUDED

#include <cstdint>

#include "etl/error_handler.h"
#include "etl/exception.h"
#include "etl/map.h"
#include "etl/pool.h"
#include "math/vector.hpp"

#define MY_ASSERT(cond, mess)                                                                                          \
    {                                                                                                                  \
        if (!(cond))                                                                                                   \
        {                                                                                                              \
            LOG_ERROR(mess);                                                                                           \
            taskDISABLE_INTERRUPTS();                                                                                  \
            for (;;)                                                                                                   \
                sleep(1000);                                                                                           \
        }                                                                                                              \
    }

namespace navmesh
{

typedef int16_t Index;
typedef int16_t Number;
typedef math::vector<Number, 2> Coords;

struct face;

typedef struct edge
{
    Index i;
    Index j;
    struct edge *next_i;
    struct edge *next_j;
    struct face *face_i;
    struct face *face_j;
} Edge;

typedef struct
{
    Coords coords;
    Edge *row;
    Edge *colum;
} Point;

typedef struct face
{
    Edge *edge_i;
    Edge *edge_j;
    Edge *edge_k;
    int gen;
    float cost;
} Face;

template <const unsigned int POINTS, const unsigned int EDGES, const unsigned int FACES> class Sparse
{
  public:
    Sparse()
    {
        this->size = 0;
    }

    Index add_point(Number x, Number y)
    {
        MY_ASSERT(this->size < POINTS, "can't add points, the buffer is full");

        // get the next point and add size
        Point *p = &this->points[this->size];

        // setup the point
        p->coords[0] = x;
        p->coords[1] = y;
        p->row = nullptr;
        p->colum = nullptr;

        return this->size++;
    }

    void remove_point(Index idx)
    {
        MY_ASSERT(0 <= idx && idx < this->size, "invalid point index");

        // remove the connected edges
        Point *point = &this->points[idx];

        for (Edge *edge = point->row; edge != nullptr;)
        {
            Edge *next = edge->next_j;
            this->remove_edge(edge->i, edge->j);
            edge = next;
        }

        for (Edge *edge = point->colum; edge != nullptr;)
        {
            Edge *next = edge->next_i;
            this->remove_edge(edge->i, edge->j);
            edge = next;
        }

        // if the points is not the last on the array, overwrite it with the
        // last
        if (idx < (this->size - 1))
        {
            Point *src = &this->points[this->size - 1];
            Point *dst = &this->points[idx];

            dst->coords[0] = src->coords[0];
            dst->coords[1] = src->coords[1];

            // copy the edges of the last point
            for (Edge *edge = src->row; edge != nullptr; edge = edge->next_j)
                this->add_edge(idx, edge->j);

            for (Edge *edge = src->colum; edge != nullptr; edge = edge->next_i)
                this->add_edge(edge->i, idx);

            // remove the edges from the last point
            for (Edge *edge = src->row; edge != nullptr;)
            {
                Edge *next = edge->next_j;
                this->remove_edge(edge->i, edge->j);
                edge = next;
            }

            for (Edge *edge = src->colum; edge != nullptr;)
            {
                Edge *next = edge->next_i;
                this->remove_edge(edge->i, edge->j);
                edge = next;
            }
        }

        // remove the last point
        this->size--;
    }

    Point *get_point(Index idx)
    {
        MY_ASSERT(idx < static_cast<Index>(this->size), "invalid point index");
        return &this->points[idx];
    }

    const Point *get_point(Index idx) const
    {
        MY_ASSERT(idx < static_cast<Index>(this->size), "invalid point index");
        return &this->points[idx];
    }

    Edge *add_edge(Index i, Index j, bool exception_if_exist = true)
    {
        MY_ASSERT(i != j, "can't connect a point with itself");

        // force i < j
        if (i > j)
        {
            Index tmp = i;
            i = j;
            j = tmp;
        }

        // allocate an edge and setup the points pair
        Edge *edge = this->edges.allocate();
        edge->i = i;
        edge->j = j;
        edge->face_i = nullptr;
        edge->face_j = nullptr;

        // search where the edge must be inserted (row of the sparse matrix)
        Edge *prev = nullptr;
        Edge *current = this->points[i].row;

        for (; current != nullptr && current->j < j; current = current->next_j)
        {
            prev = current;
        }

        // if the edge already exist, raise an exception or do nothing
        if (current != nullptr && current->i == i && current->j == j)
        {
            // MY_ASSERT(!exception_if_exist,
            //            "can't add an existing edge");
            return nullptr;
        }

        // insert the edge on the list
        edge->next_j = current;
        if (prev == nullptr)
            this->points[i].row = edge;
        else
            prev->next_j = edge;

        // search where the edge must be inserted (colum of the sparse matrix)
        prev = nullptr;
        current = this->points[j].colum;

        for (; current != nullptr && current->i < i; current = current->next_i)
        {
            prev = current;
        }
        // insert the edge on the list
        edge->next_i = current;
        if (prev == nullptr)
            this->points[j].colum = edge;
        else
            prev->next_i = edge;

        return edge;
    }

    void remove_edge(Index i, Index j)
    {
        MY_ASSERT(i != j, "can't remove an edge between the same index i and j");

        // force i < j
        if (i > j)
        {
            Index tmp = i;
            i = j;
            j = tmp;
        }

        // search edge (row)
        Edge *prev = nullptr;
        Edge *current = this->points[i].row;

        for (; current != nullptr && current->j < j; current = current->next_j)
        {
            prev = current;
        }

        // if the edge doesn't exist, raise an exception
        MY_ASSERT(current != nullptr && current->i == i && current->j == j, "edge not found");

        // remove from the list
        if (prev == nullptr)
            this->points[i].row = current->next_j;
        else
            prev->next_j = current->next_j;

        // search edge (colum)
        prev = nullptr;
        current = this->points[j].colum;

        for (; current != nullptr && current->i < i; current = current->next_i)
        {
            prev = current;
        }

        // if the edge doesn't exist, raise an exception
        MY_ASSERT(current != nullptr && current->i == i && current->j == j, "edge not found");

        // remove faces
        if (current->face_i != nullptr)
            this->remove_face(current->face_i);
        if (current->face_j != nullptr)
            this->remove_face(current->face_j);

        // remove from the list
        if (prev == nullptr)
            this->points[j].colum = current->next_i;
        else
            prev->next_i = current->next_i;

        // free edge
        this->edges.release(current);
    }

    bool has_edge(Index i, Index j)
    {
        if (i == j)
            return false;
        // MY_ASSERT(i != j, "can't test an edge between the same index i and j");
        //  force i < j
        if (i > j)
        {
            Index tmp = i;
            i = j;
            j = tmp;
        }

        // search edge (row)
        Edge *prev = nullptr;
        Edge *current = this->points[i].row;

        for (; current != nullptr && current->j < j; current = current->next_j)
        {
            prev = current;
        }

        return current != nullptr && current->i == i && current->j == j;
    }

    Edge *get_edge(Index i, Index j)
    {
        if (i > j)
        {
            Index tmp = i;
            i = j;
            j = tmp;
        }

        Edge *current = this->points[i].row;
        while (current != nullptr && current->j != j)
        {
            current = current->next_j;
        }

        return current;
    }

    int get_face_id(Index i, Index j, Index k)
    {
        if (j < i)
        {
            Index tmp = i;
            i = j;
            j = tmp;
        }
        if (k < j)
        {
            Index tmp = j;
            j = k;
            k = tmp;
        }
        if (j < i)
        {
            Index tmp = i;
            i = j;
            j = tmp;
        }

        return ((i * FACES) + j) * FACES + k;
    }

    Point barycenter(Face *f)
    {
        Index i = f->edge_i->i;
        Index j = f->edge_i->j;
        Index k = f->edge_j->j;

        if (f->edge_j->i != i && f->edge_j->i != j)
            k = f->edge_j->i;

        Point *a = this->get_point(i);
        Point *b = this->get_point(j);
        Point *c = this->get_point(k);
        Point res;
        res.coords[0] = (a->coords[0] + b->coords[0] + c->coords[0]) / 3;
        res.coords[1] = (a->coords[1] + b->coords[1] + c->coords[1]) / 3;

        return res;
    }

    void add_face(Edge *i, Edge *j, Edge *k)
    {
        Index n_i = i->i;
        Index n_j = i->j;
        Index n_k = j->j;

        if (j->i != n_i && j->i != n_j)
            n_k = j->i;

        // Face *face = this->faces.allocate();
        Face *face = &this->faces[this->get_face_id(n_i, n_j, n_k)];
        face->gen = 0;

        face->edge_i = i;
        if (i->face_i == nullptr)
            i->face_i = face;
        else
            i->face_j = face;

        face->edge_j = j;
        if (j->face_i == nullptr)
            j->face_i = face;
        else
            j->face_j = face;

        face->edge_k = k;
        if (k->face_i == nullptr)
            k->face_i = face;
        else
            k->face_j = face;
    }

    void remove_face(Face *f)
    {
        if (f->edge_i->face_i == f)
            f->edge_i->face_i = nullptr;
        else
            f->edge_i->face_j = nullptr;

        if (f->edge_j->face_i == f)
            f->edge_j->face_i = nullptr;
        else
            f->edge_j->face_j = nullptr;

        if (f->edge_k->face_i == f)
            f->edge_k->face_i = nullptr;
        else
            f->edge_k->face_j = nullptr;

        Index n_i = f->edge_i->i;
        Index n_j = f->edge_i->j;
        Index n_k = f->edge_j->j;

        if (f->edge_j->i != n_i && f->edge_j->i != n_j)
            n_k = f->edge_j->i;

        this->faces.erase(this->get_face_id(n_i, n_j, n_k));
    }

    auto faces_begin()
    {
        return this->faces.begin();
    }

    auto faces_end()
    {
        return this->faces.end();
    }

    [[nodiscard]] size_t size_points() const
    {
        return (size_t)this->size;
    }

    [[nodiscard]] size_t size_edges() const
    {
        return this->edges.size();
    }

    [[nodiscard]] size_t size_faces() const
    {
        return this->faces.size();
    }

    class IteratorEdges
    {
      public:
        IteratorEdges(const Sparse *matrix, Index point_idx) : matrix(matrix)
        {
            MY_ASSERT(point_idx < static_cast<Index>(matrix->size_points()), "invalid point idx");

            this->current_edge = matrix->points[point_idx].colum;
            this->colum = true;

            if (this->current_edge == nullptr)
            {
                this->current_edge = matrix->points[point_idx].row;
                this->colum = false;
            }
        }

        void next()
        {
            MY_ASSERT(this->current_edge != nullptr, "no remaining element");

            if (this->colum)
            {
                Index point_idx = this->current_edge->j;
                this->current_edge = this->current_edge->next_i;

                if (this->current_edge == nullptr)
                {
                    this->current_edge = this->matrix->points[point_idx].row;
                    this->colum = false;
                }
            }
            else
            {
                this->current_edge = this->current_edge->next_j;
            }
        }

        bool end()
        {
            return this->current_edge == nullptr;
        }

        Index neighbours()
        {
            if (this->colum)
            {
                return current_edge->i;
            }
            else
            {
                return current_edge->j;
            }
        }

        Index i()
        {
            if (current_edge != nullptr)
                return current_edge->i;
            return -1;
        }
        Index j()
        {
            if (current_edge != nullptr)
                return current_edge->j;
            return -1;
        }

      private:
        const Sparse *matrix;
        Edge *current_edge;
        bool colum;
    };

    class IteratorAllEdges
    {
      public:
        IteratorAllEdges(const Sparse *matrix) : matrix(matrix)
        {
            this->current_edge = nullptr;

            for (int i = 0; this->current_edge == nullptr && i < static_cast<Index>(matrix->size_points()); i++)
            {
                this->current_edge = matrix->get_point(i)->row;
            }
        }

        void next()
        {
            MY_ASSERT(this->current_edge != nullptr, "no remaining element");

            Index point_idx = this->current_edge->i;
            this->current_edge = this->current_edge->next_j;

            for (Index i = point_idx + 1;
                 this->current_edge == nullptr && i < static_cast<Index>(matrix->size_points()); i++)
            {
                this->current_edge = matrix->get_point(i)->row;
            }
        }

        bool end()
        {
            return this->current_edge == nullptr;
        }

        Index i()
        {
            return current_edge->i;
        }
        Index j()
        {
            return current_edge->j;
        }

        Edge *current()
        {
            return this->current_edge;
        }

      private:
        const Sparse *matrix;
        Edge *current_edge;
    };

    IteratorEdges get_edges(Index idx)
    {
        return IteratorEdges(this, idx);
    }
    IteratorAllEdges get_all_edges()
    {
        return IteratorAllEdges(this);
    }
    IteratorEdges get_edges(Index idx) const
    {
        return IteratorEdges(this, idx);
    }
    IteratorAllEdges get_all_edges() const
    {
        return IteratorAllEdges(this);
    }

  protected:
    unsigned int size;
    Point points[POINTS];
    etl::pool<Edge, EDGES> edges;
    etl::map<int, Face, FACES> faces;
};

} // namespace navmesh

#endif
