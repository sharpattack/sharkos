#ifndef NAVMESH_NAVMESH_INCLUDED
#define NAVMESH_NAVMESH_INCLUDED

#include <etl/array.h>
#include <etl/error_handler.h>
#include <etl/exception.h>
#include <etl/list.h>

#include <cmath>

#include "math/intersect.hpp"
#include "sparse.hpp"

namespace navmesh
{

template <const unsigned int POINTS, const unsigned int EDGES, const unsigned int FACES> class NavMesh
{
  public:
    NavMesh() : matrix()
    {
    }

    template <const unsigned int N> void add_poly(etl::array<Coords, N> &poly)
    {
        etl::array<Index, N> inner_poly;

        bool connect_outer = this->matrix.size_points() > 0;

        for (size_t i = 0; i < poly.size(); i++)
            inner_poly[i] = this->matrix.add_point(poly[i][0], poly[i][1]);

        this->remove_intersection<N>(inner_poly);

        for (size_t i = 0; i < inner_poly.size(); i++)
            this->matrix.add_edge(inner_poly[i], inner_poly[(i + 1) % N]);

        if (connect_outer)
        {
            Edge closest;
            this->connect_outer_poly<N>(inner_poly, &closest);
        }
        else
        {
            this->connect_lonely_poly<N>(inner_poly);
        }
    }

    template <const unsigned int N> void remove_intersection(etl::array<Index, N> &poly)
    {
        auto it = this->matrix.get_all_edges();
        while (!it.end())
        {
            auto current = it;
            it.next();

            for (size_t i = 0; i < poly.size(); i++)
            {
                if (this->intersect(current.i(), current.j(), poly[i], poly[(i + 1) % N]))
                {
                    this->matrix.remove_edge(current.i(), current.j());
                    break;
                }
            }
        }
    }

    template <const unsigned int N> void connect_lonely_poly(etl::array<Index, N> &poly)
    {
        etl::list<Index, POINTS> list;
        Index node_i, node_j, node_k;

        for (int i = 0; i < static_cast<Index>(poly.size()); i++)
            list.push_back(poly[i]);

        for (auto it = list.begin(); it != list.end(); it++)
        {
            Point *p = this->matrix.get_point(*it);
        }

        int i = 0;

        while (list.size() > 3)
        {
            auto it = list.begin();

            node_i = *(--list.end());
            node_j = *it++;
            node_k = *it++;

            Point *pos_i = this->matrix.get_point(node_i);
            Point *pos_j = this->matrix.get_point(node_j);
            Point *pos_k = this->matrix.get_point(node_k);

            math::vector<int, 2> u = {pos_j->coords[0] - pos_i->coords[0], pos_j->coords[1] - pos_i->coords[1]};
            math::vector<int, 2> v = {pos_k->coords[0] - pos_i->coords[0], pos_k->coords[1] - pos_i->coords[1]};

            bool is_ok = false;
            // rotate clockwish or conter-clockwish?
            int det = (u[0] * v[1]) - (v[0] * u[1]);
            if (det >= 0)
            {
                is_ok = true;
                auto stop = --(--list.end());
                for (; it != stop;)
                {
                    auto current = it++;
                    if (this->intersect(node_i, node_k, *current, *it))
                    {
                        is_ok = false;
                        break;
                    }
                }
            }
            if (is_ok)
            {
                Edge *e = this->matrix.add_edge(node_i, node_k);
                this->matrix.add_face(e, this->matrix.get_edge(node_i, node_j), this->matrix.get_edge(node_k, node_j));
            }
            else
            {
                list.push_back(node_j);
            }
            list.pop_front();
        }

        auto it = list.begin();
        node_i = *it++;
        node_j = *it++;
        node_k = *it;
        this->matrix.add_face(this->matrix.get_edge(node_i, node_j), this->matrix.get_edge(node_j, node_k),
                              this->matrix.get_edge(node_k, node_i));

        return;
    }

    template <const unsigned int N> void connect_outer_poly(etl::array<Index, N> &inner_poly, Edge *edge)
    {
        etl::list<Index, POINTS> list;
        bool mask[POINTS];
        bool first_edge = true;
        // list.clear();

        // find vertices of the polygone
        for (Index i = 0; i < static_cast<Index>(this->matrix.size_points() - N); i++)
        {
            bool i_valid = false;
            for (Index j_idx = 0; j_idx < static_cast<Index>(inner_poly.size()) && (!i_valid); j_idx++)
            {
                bool valid = true;
                Index j = inner_poly[j_idx];
                auto it = this->matrix.get_all_edges();

                while ((!it.end()) && valid)
                {
                    if (i != it.i() && i != it.j() && j != it.j() && j != it.i())
                    {
                        valid = valid && (!this->intersect(i, j, it.i(), it.j()));
                    }
                    it.next();
                }

                if (first_edge && valid)
                {
                    edge->i = i;
                    edge->j = j;
                    edge->next_i = nullptr;
                    edge->next_j = nullptr;
                    first_edge = false;
                }

                i_valid = i_valid || valid;
            }
            mask[i] = i_valid;
        }
        for (Index i = static_cast<Index>(this->matrix.size_points()) - N;
             i < static_cast<Index>(this->matrix.size_points()); i++)
        {
            mask[i] = false;
        }

        Index i0;

        for (i0 = 0; i0 < static_cast<Index>(this->matrix.size_points() - N); i0++)
            if (mask[i0])
                break;

        // ETL_ASSERT(i0 != (this->matrix.size_points() - N));

        list.push_back(i0);

        this->matrix.add_edge(edge->i, edge->j);

        Index node_i = edge->i;
        Index node_j = edge->j;

        Index offset;
        for (offset = 0; inner_poly[offset] != node_j && (offset < static_cast<Index>(inner_poly.size())); offset++)
            ;
        // ETL_ASSERT(offset < inner_poly.size());

        Edge *last_edge = nullptr;
        Index node = -1;
        for (int j = 0; j < static_cast<Index>(inner_poly.size());)
        {
            Index node_j = inner_poly[(j + offset) % inner_poly.size()];

            Point *pos_i = this->matrix.get_point(node_i);
            Point *pos_j = this->matrix.get_point(node_j);

            node = -1;
            float cosa_max = -1;
            for (auto it = this->matrix.get_edges(node_i); !it.end(); it.next())
            {
                Index node_k = it.neighbours();

                if (this->matrix.has_edge(node_j, node_k) || (!mask[node_k]))
                    continue;

                bool insersected = false;
                for (auto e = this->matrix.get_all_edges(); !e.end(); e.next())
                {
                    if (node_j != e.i() && node_j != e.j() && node_k != e.i() && node_k != e.j() &&
                        this->intersect(node_j, node_k, e.i(), e.j()))
                    {
                        insersected = true;
                        break;
                    }
                }

                if (insersected)
                    continue;

                Point *pos_k = this->matrix.get_point(node_k);

                math::vector<int, 2> u = {pos_k->coords[0] - pos_j->coords[0], pos_k->coords[1] - pos_j->coords[1]};
                math::vector<int, 2> v = {pos_i->coords[0] - pos_j->coords[0], pos_i->coords[1] - pos_j->coords[1]};

                // rotate clockwish or conter-clockwish?
                int det = u[0] * v[1] - v[0] * u[1];
                if (det >= 0)
                    continue;

                // calculate the cos of the angle
                float cosa = math::dot(u, v) / (u.norm() * v.norm());
                if (cosa > cosa_max)
                {
                    cosa_max = cosa;
                    node = node_k;
                }
            }

            if (node != -1)
            {
                Edge *new_edge = this->matrix.add_edge(node_j, node);

                mask[node] = false;
                if (new_edge != nullptr)
                {
                    this->matrix.add_face(new_edge, this->matrix.get_edge(new_edge->i, node_i),
                                          this->matrix.get_edge(new_edge->j, node_i));
                    last_edge = new_edge;
                }
                node_i = node;
            }
            else
            {
                Index prev_j = node_j;
                j++;
                node_j = inner_poly[(j + offset) % inner_poly.size()];
                Edge *new_edge = this->matrix.add_edge(node_j, node_i);
                if (new_edge != nullptr)
                {
                    this->matrix.add_face(new_edge, this->matrix.get_edge(new_edge->i, prev_j),
                                          this->matrix.get_edge(new_edge->j, prev_j));
                    last_edge = new_edge;
                }
            }
        }

        for (size_t i = 0; i < (this->matrix.size_points() - N); i++)
        {
            if (mask[i])
            {
                Edge *new_edge = this->matrix.add_edge(node_j, i);
                if (new_edge != nullptr)
                {
                    this->matrix.add_face(new_edge, this->matrix.get_edge(new_edge->i, node_i),
                                          this->matrix.get_edge(new_edge->j, node_i));
                    last_edge = new_edge;
                }
            }
        }
        if (last_edge != nullptr)
        {
            Index node_k;
            if ((edge->i == last_edge->i) || (edge->i == last_edge->j))
                node_k = edge->j;
            else
                node_k = edge->i;

            this->matrix.add_face(last_edge, this->matrix.get_edge(last_edge->i, node_k),
                                  this->matrix.get_edge(last_edge->j, node_k));
        }
    }

    bool intersect(Index i, Index j, Index I, Index J)
    {
        Coords pi = this->get_point(i);
        Coords pj = this->get_point(j);
        Coords pI = this->get_point(I);
        Coords pJ = this->get_point(J);

        math::segment<float, 2> seg0{pi.to<float>(), pj.to<float>()};
        math::segment<float, 2> seg1{pI.to<float>(), pJ.to<float>()};

        return math::intersect(seg0, seg1);
    }

    bool intersect(Coords pi, Index j, Index I, Index J)
    {
        Coords pj = this->get_point(j);
        Coords pI = this->get_point(I);
        Coords pJ = this->get_point(J);

        math::segment<float, 2> seg0{pi.to<float>(), pj.to<float>()};
        math::segment<float, 2> seg1{pI.to<float>(), pJ.to<float>()};

        return math::intersect(seg0, seg1);
    }

    int sign(Coords a, Coords b, Coords c)
    {
        return ((int)a[0] - c[0]) * ((int)b[1] - c[1]) - ((int)b[0] - c[0]) * ((int)a[1] - c[1]);
    }

    bool inside(Coords pos, Index i, Index j, Index k)
    {
        Point *a = this->matrix.get_point(i);
        Point *b = this->matrix.get_point(j);
        Point *c = this->matrix.get_point(k);
        Coords vec_a{a->coords[0], a->coords[1]};
        Coords vec_b{b->coords[0], b->coords[1]};
        Coords vec_c{c->coords[0], c->coords[1]};

        int d1 = this->sign(pos, vec_a, vec_b);
        int d2 = this->sign(pos, vec_b, vec_c);
        int d3 = this->sign(pos, vec_c, vec_a);

        bool has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
        bool has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);
        return !(has_neg && has_pos);
    }

    Face *locate(Coords pos)
    {
        for (auto it = this->matrix.faces_begin(); it != this->matrix.faces_end(); it++)
        {
            Index n_i = it->second.edge_i->i;
            Index n_j = it->second.edge_i->j;
            Index n_k = it->second.edge_j->j;

            if (it->second.edge_j->i != n_i && it->second.edge_j->i != n_j)
                n_k = it->second.edge_j->i;

            if (this->inside(pos, n_i, n_j, n_k))
            {
                return &it->second;
            }
        }
        return nullptr;
    }

    Edge *get_connecting_edge(Face *prev, Face *current)
    {
        Edge *neighbours[3] = {prev->edge_i, prev->edge_j, prev->edge_k};
        for (int i = 0; i < 3; i++)
        {
            Face *n = (neighbours[i]->face_i == prev) ? neighbours[i]->face_j : neighbours[i]->face_i;
            if (n == current)
                return neighbours[i];
        }

        return nullptr;
    }

    Edge *get_connecting_edge(etl::ilist<Face *>::iterator it)
    {
        Face *current = *it;
        Face *prev = *(--it);
        return get_connecting_edge(prev, current);
    }

    // etl::ilist<face*>::iterator&
    Edge *get_border_edge(etl::ilist<Face *>::iterator it)
    {
        auto cpy = it;
        Face *prev = *(--cpy);
        Face *current = *it;
        Face *next = *(++it);

        Edge *neighbours[3];
        neighbours[0] = current->edge_i;
        neighbours[1] = current->edge_j;
        neighbours[2] = current->edge_k;

        for (int i = 0; i < 3; i++)
        {
            Face *n = (neighbours[i]->face_i == current) ? neighbours[i]->face_j : neighbours[i]->face_i;
            if (n != prev && n != next)
                return neighbours[i];
        }
        return nullptr;
    }

    Index get_third_point(Face *face, Index i, Index j)
    {
        Index next_p, current_p;

        current_p = face->edge_i->i;
        if (current_p != i && current_p != j)
        {
            next_p = current_p;
        }
        else
        {
            current_p = face->edge_i->j;
            if (current_p != i && current_p != j)
            {
                next_p = current_p;
            }
            else
            {
                current_p = face->edge_j->i;
                if (current_p != i && current_p != j)
                {
                    next_p = current_p;
                }
                else
                {
                    next_p = face->edge_j->j;
                }
            }
        }

        return next_p;
    }

    bool is_in(Face *face, Index idx)
    {
        Index i = face->edge_i->i, j = face->edge_i->j, k;

        if (face->edge_j->i == i || face->edge_j->i == j)
            k = face->edge_j->j;
        else
            k = face->edge_j->i;

        return idx == i || idx == j || idx == k;
    }

    bool is_valid_move(Coords &start, Index target, Coords &border1, Coords &border2,
                       etl::list<Edge *, FACES> &to_check)
    {
        Point *target_point = this->matrix.get_point(target);
        Coords target_coords{target_point->coords[0], target_point->coords[1]};

        bool angle1 = this->sign(border1, target_coords, start) >= 0;
        bool angle2 = this->sign(target_coords, border2, start) >= 0;
        if (angle1 != angle2)
        {
            return false;
        }

        for (auto it = to_check.begin(); it != to_check.end(); it++)
            if (this->intersect(start, target, (*it)->i, (*it)->j))
                return false;

        return true;
    }

    bool is_valid_move(Coords &start, Index target, etl::list<Edge *, FACES> &to_check)
    {
        for (auto it = to_check.begin(); it != to_check.end(); it++)
            if (this->intersect(start, target, (*it)->i, (*it)->j))
                return false;

        return true;
    }

    Coords get_point(Index i)
    {
        Point *point = this->matrix.get_point(i);
        return Coords{point->coords[0], point->coords[1]};
    }

    void simplify_path(Coords start, Coords target, etl::list<Face *, FACES> &path,
                       etl::list<Coords, FACES> &simplified_path)
    {
        etl::list<Edge *, FACES> to_check;
        simplified_path.clear();

        simplified_path.push_back(start);

        auto it = ++path.begin();

        Index right, left, portal_right, portal_left;

        Edge *connection_edge;

        Coords portal_apex_coords = start;
        Coords portal_right_coords;
        Coords portal_left_coords;
        Coords right_coords;
        Coords left_coords;

        connection_edge = get_connecting_edge(it);

        if (this->sign(this->get_point(connection_edge->i), this->get_point(connection_edge->j), portal_apex_coords) >=
            0)
        {
            right = connection_edge->i;
            left = connection_edge->j;
        }
        else
        {
            right = connection_edge->j;
            left = connection_edge->i;
        }
        portal_right = right;
        portal_left = left;
        portal_right_coords = this->get_point(portal_right);
        portal_left_coords = this->get_point(portal_left);

        for (it++;; it++)
        {
            if (it != path.end())
            {
                connection_edge = this->get_connecting_edge(it);
                if ((connection_edge->i == right) || (connection_edge->j == right))
                {
                    left = connection_edge->i == right ? connection_edge->j : connection_edge->i;
                    right = connection_edge->i == right ? connection_edge->i : connection_edge->j;
                }
                else
                {
                    right = connection_edge->i == left ? connection_edge->j : connection_edge->i;
                    left = connection_edge->i == left ? connection_edge->i : connection_edge->j;
                }
                right_coords = this->get_point(right);
                left_coords = this->get_point(left);
            }
            else
            {
                right_coords = target;
                left_coords = target;
            }

            if (this->sign(portal_right_coords, right_coords, portal_apex_coords) >= 0.0)
            {
                if ((portal_apex_coords - portal_right_coords).norm() < 0.001 ||
                    this->sign(right_coords, portal_left_coords, portal_apex_coords) >= 0.0)
                {
                    portal_right = right;
                    portal_right_coords = this->get_point(portal_right);
                }
                else
                {
                    simplified_path.push_back(portal_left_coords);

                    Index apex_idx = portal_left;
                    portal_apex_coords = portal_left_coords;

                    portal_left_coords = portal_apex_coords;
                    portal_right_coords = portal_apex_coords;
                    portal_left = apex_idx;
                    portal_right = apex_idx;

                    if (it != path.end())
                        for (; !this->is_in(*it, apex_idx); it--)
                            ;
                }
            }

            if (this->sign(portal_left_coords, left_coords, portal_apex_coords) <= 0.0)
            {
                if ((portal_apex_coords - portal_left_coords).norm() < 0.001 ||
                    this->sign(left_coords, portal_right_coords, portal_apex_coords) <= 0.0)
                {
                    portal_left = left;
                    portal_left_coords = this->get_point(portal_left);
                }
                else
                {
                    simplified_path.push_back(portal_right_coords);

                    Index apex_idx = portal_right;
                    portal_apex_coords = portal_right_coords;

                    portal_right_coords = portal_apex_coords;
                    portal_left_coords = portal_apex_coords;
                    portal_left = apex_idx;
                    portal_right = apex_idx;

                    if (it != path.end())
                        for (; !this->is_in(*it, apex_idx); it--)
                            ;
                }
            }

            if (it == path.end())
                break;
        }

        if ((target - simplified_path.back()).norm() >= 0.001)
            simplified_path.push_back(target);
    }

    void pathfinding(Coords start, Coords target, etl::list<Coords, FACES> &path, float distance_cost_factor = 1.0)
    {
        etl::list<Face *, FACES> faces_path;
        this->a_star(start, target, faces_path, distance_cost_factor);
        this->simplify_path(start, target, faces_path, path);
    }

    void a_star(Coords start, Coords target, etl::list<Face *, FACES> &path, float alpha = 1.0)
    {
        static int gen = 0;

        path.clear();
        etl::map<float, Face *, FACES> faces;
        Face *f_start = this->locate(start);
        Face *f_target = this->locate(target);

        gen++;

        f_start->gen = gen;
        f_start->cost = 0.0;
        faces[f_start->cost] = f_start;

        while (f_target->gen != gen && faces.begin() != faces.end())
        {
            auto current = faces.begin();
            Face *face = current->second;
            faces.erase(current->first);

            Point c = this->matrix.barycenter(face);
            Coords center{c.coords[0], c.coords[1]};

            Face *neighbours[3];
            neighbours[0] = (face->edge_i->face_i == face) ? face->edge_i->face_j : face->edge_i->face_i;
            neighbours[1] = (face->edge_j->face_i == face) ? face->edge_j->face_j : face->edge_j->face_i;
            neighbours[2] = (face->edge_k->face_i == face) ? face->edge_k->face_j : face->edge_k->face_i;

            for (int i = 0; i < 3; i++)
            {
                if (neighbours[i] != nullptr)
                {
                    Point n_c = this->matrix.barycenter(neighbours[i]);
                    Coords n_center{n_c.coords[0], n_c.coords[1]};
                    float new_cost = face->cost + (center - n_center).norm();
                    if (neighbours[i]->gen != gen || new_cost < neighbours[i]->cost)
                    {
                        neighbours[i]->gen = gen;
                        neighbours[i]->cost = new_cost;
                        faces[new_cost + alpha * (n_center - target).norm()] = neighbours[i];
                    }
                }
            }
        }

        if (f_target->gen != gen)
            return;

        path.push_front(f_target);
        while ((*path.begin()) != f_start)
        {
            Face *face = *path.begin();

            Face *neighbours[3];
            neighbours[0] = (face->edge_i->face_i == face) ? face->edge_i->face_j : face->edge_i->face_i;
            neighbours[1] = (face->edge_j->face_i == face) ? face->edge_j->face_j : face->edge_j->face_i;
            neighbours[2] = (face->edge_k->face_i == face) ? face->edge_k->face_j : face->edge_k->face_i;

            Face *lower = nullptr;
            float min = f_target->cost;
            for (int i = 0; i < 3; i++)
            {
                if (neighbours[i] != nullptr && neighbours[i]->gen == gen && neighbours[i]->cost < min)
                {
                    min = neighbours[i]->cost;
                    lower = neighbours[i];
                }
            }
            if (lower == nullptr)
            {
                return;
            }
            path.push_front(lower);
        }
    }

    int count_points()
    {
        return this->matrix.size_points();
    }
    int count_edges()
    {
        return this->matrix.size_edges();
    }
    int count_faces()
    {
        return this->matrix.size_faces();
    }

    Sparse<POINTS, EDGES, FACES> matrix;

  protected:
};
} // namespace navmesh

#endif
