#ifndef NAVMESH_POLY_INCLUDED
#define NAVMESH_POLY_INCLUDED

#include "etl/array.h"

#include "math/vector.hpp"
#include "sparse.hpp"

namespace navmesh
{

etl::array<Coords, 4> rectangle(Number x_min, Number y_min, Number x_max, Number y_max, bool hole = false)
{
    Coords p0{x_min, y_min};
    Coords p1{x_min, y_max};
    Coords p2{x_max, y_max};
    Coords p3{x_max, y_min};

    if (hole)
    {
        return etl::array<Coords, 4>{p0, p3, p2, p1};
    }
    return etl::array<Coords, 4>{p0, p1, p2, p3};
}

template <const unsigned int N> etl::array<Coords, N> circle(Number x, Number y, Number r, bool hole = false)
{
    Coords center{x, y};
    etl::array<Coords, N> cir;

    for (int i = 0; i < N; i++)
        cir[i] = center + (math::dir((hole ? 1 : -1) * M_PI * i * 2 / N) * (float)r).to<Number>();

    return cir;
}

} // namespace navmesh

#endif
