#include "FreeRTOS.h"
#include "queue.h"
#include "vision_objects.h"
#include "tmc/helpers/Types.h"

u8 search_vision_object(VisionObject *out_objects, u8 buffer_size, const char *searched_type, u8 max_tries_nb);
