#ifndef SERIAL_TRANSPORT_EXTERNAL
#define SERIAL_TRANSPORT_EXTERNAL

#ifdef __cplusplus
extern "C"
{
#endif

#include <uxr/client/profile/transport/serial/serial_transport_external.h>

    bool my_custom_transport_open(uxrExternalSerialTransport *transport);
    bool my_custom_transport_close(uxrExternalSerialTransport *transport);
    size_t my_custom_transport_write(void *transport, const uint8_t *buf, size_t len, uint8_t *errcode);
    size_t my_custom_transport_read(void *transport, uint8_t *buf, size_t len, int timeout, uint8_t *errcode);

#ifdef __cplusplus
}
#endif

#endif // SERIAL_TRANSPORT_EXTERNAL
