#ifndef MATH_INTERSECT_INCLUDED
#define MATH_INTERSECT_INCLUDED

#include <etl/limits.h>
#include <etl/type_traits.h>

#include <initializer_list>

#include "vector.hpp"

namespace math
{

#define EPSILON 1e-6

template <typename T, const unsigned int SIZE> class segment
{
  public:
    segment() = default;

    segment(std::initializer_list<std::initializer_list<T>> init_value)
    {
        assert(init_value.size() == SIZE);

        auto it = init_value.begin();
        std::initializer_list<T> init_p0 = *(it++);
        std::initializer_list<T> init_p1 = *it;
        p0 = math::vector<T, SIZE>(init_p0);
        p1 = math::vector<T, SIZE>(init_p1);
    }
    segment(std::initializer_list<math::vector<T, SIZE>> init_value)
    {
        assert(init_value.size() == SIZE);

        auto it = init_value.begin();
        p0 = *(it++);
        p1 = *it;
    }

    math::vector<T, SIZE> p0;
    math::vector<T, SIZE> p1;
};

float safe_divide(float a, float b)
{
    if (fabs(b) < EPSILON)
    {
        return etl::numeric_limits<float>::infinity();
    }
    return a / b;
}

bool intersect(segment<float, 2> &seg0, segment<float, 2> &seg1, float epsilon = 0.0)
{
    math::vector<float, 2> base_x = seg0.p1 - seg0.p0;
    base_x /= base_x.norm2();

    math::vector<float, 2> base_y{-base_x[1], base_x[0]};

    segment<float, 2> seg{seg1.p0 - seg0.p0, seg1.p1 - seg0.p0};

    segment<float, 2> seg_trans{{math::dot(seg.p0, base_x), math::dot(seg.p0, base_y)},
                                {math::dot(seg.p1, base_x), math::dot(seg.p1, base_y)}};

    float a = safe_divide(seg_trans.p0[1], seg_trans.p0[1] - seg_trans.p1[1]);
    float b = safe_divide(seg_trans.p1[1], seg_trans.p1[1] - seg_trans.p0[1]);

    float x = a * seg_trans.p1[0] + b * seg_trans.p0[0];

    bool valid = (seg_trans.p1[1] > 0) != (seg_trans.p0[1] > 0);

    return (-epsilon) <= x && x <= (1 + epsilon) && valid;
}

} // namespace math

#endif
