#ifndef MATH_VECTOR_INCLUDED
#define MATH_VECTOR_INCLUDED

#include <etl/sqrt.h>

#include <cassert>
#include <cmath>
#include <initializer_list>
#include <type_traits>

namespace math
{

template <typename T, const unsigned int SIZE> class vector
{
  private:
    T value[SIZE];

  public:
    vector() = default;
    explicit vector(const T init_value)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] = init_value;
        }
    }
    explicit vector(const T &init_value)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] = static_cast<T>(init_value);
        }
    }
    explicit vector(const T &&init_value)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] = static_cast<T>(init_value);
        }
    }
    vector(std::initializer_list<T> init_value)
    {
        assert(init_value.size() == SIZE);

        unsigned int i = 0;
        for (auto v : init_value)
        {
            value[i++] = v;
        }
    }
    vector(const vector<T, SIZE> &init_value)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] = static_cast<T>(init_value[i]);
        }
    }
    vector(const vector<T, SIZE> &&init_value) noexcept
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] = static_cast<T>(init_value[i]);
        }
    }

    vector<T, SIZE> &operator=(const vector<T, SIZE> &src)
    {
        if (this == &src)
        {
            return *this;
        }

        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] = static_cast<T>(src[i]);
        }
        return *this;
    }

    vector<T, SIZE> &operator=(const T &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] = static_cast<T>(src);
        }
        return *this;
    }

    vector<T, SIZE> &operator+=(const vector<T, SIZE> &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] += (T)src[i];
        }
        return *this;
    }

    vector<T, SIZE> &operator+=(const T &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] += (T)src;
        }
        return *this;
    }

    vector<T, SIZE> &operator-=(const vector<T, SIZE> &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] -= (T)src[i];
        }
        return *this;
    }

    vector<T, SIZE> &operator-=(const T &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] -= (T)src;
        }
        return *this;
    }

    vector<T, SIZE> &operator*=(const vector<T, SIZE> &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] *= (T)src[i];
        }
        return *this;
    }

    vector<T, SIZE> &operator*=(const T &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] *= (T)src;
        }
        return *this;
    }

    vector<T, SIZE> &operator/=(const vector<T, SIZE> &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] /= (T)src[i];
        }
        return *this;
    }

    vector<T, SIZE> &operator/=(const T &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] /= (T)src;
        }
        return *this;
    }

    vector<int, SIZE> &operator%=(const vector<int, SIZE> &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] %= src[i];
        }
        return *this;
    }

    vector<int, SIZE> &operator%=(const int &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] %= src;
        }
        return *this;
    }

    vector<long, SIZE> &operator%=(const vector<long, SIZE> &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] %= src[i];
        }
        return *this;
    }

    vector<long, SIZE> &operator%=(const long &src)
    {
        for (unsigned int i = 0; i < SIZE; i++)
        {
            value[i] %= src;
        }
        return *this;
    }

    float norm()
    {
        float sum = 0.0;
        for (unsigned int i = 0; i < SIZE; i++)
            sum += value[i] * value[i];
        return sqrt(sum);
    }

    float norm2()
    {
        float sum = 0.0;
        for (unsigned int i = 0; i < SIZE; i++)
            sum += value[i] * value[i];
        return sum;
    }

    vector<T, SIZE> floor()
    {
        vector<T, SIZE> res;
        for (unsigned int i = 0; i < SIZE; i++)
        {
            res[i] = (T)((long)value[i]);
        }
        return res;
    }

    vector<T, SIZE> ceil()
    {
        vector<T, SIZE> res;
        for (unsigned int i = 0; i < SIZE; i++)
        {
            if (value[i] == ((long)value[i]))
                res[i] = (T)((long)value[i]);
            else if (value[i] >= 0)
                res[i] = (T)((long)value[i] + 1);
            else
                res[i] = (T)((long)value[i] - 1);
        }
        return res;
    }

    vector<T, SIZE> round()
    {
        vector<T, SIZE> res;
        for (unsigned int i = 0; i < SIZE; i++)
        {
            res[i] = (T)((long)(value[i] + 0.5));
        }
        return res;
    }

    vector<T, SIZE> normalize()
    {
        vector<T, SIZE> res;
        res.operator=(*this);
        res.operator/=(res.norm());
        return res;
    }

    template <typename U> vector<U, SIZE> to() const
    {
        vector<U, SIZE> res;

        for (unsigned int i = 0; i < SIZE; i++)
        {
            res[i] = (U)value[i];
        }

        return res;
    }

    T scalar()
    {
        static_assert(SIZE == 1);
        return value[0];
    }

    [[nodiscard]] unsigned int size() const
    {
        return SIZE;
    }

    T &operator[](int idx)
    {
        return value[idx];
    }

    const T &operator[](int idx) const
    {
        return value[idx];
    }
};

template <typename T, const unsigned int SIZE>
vector<T, SIZE> operator+(const vector<T, SIZE> &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res = a;
    res.operator+=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator+(const T &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res = b;
    res.operator+=(a);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator+(const vector<T, SIZE> &a, const T &b)
{
    vector<T, SIZE> res = a;
    res.operator+=(b);
    return res;
}

template <typename T, const unsigned int SIZE>
vector<T, SIZE> operator-(const vector<T, SIZE> &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res = a;
    res.operator-=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator-(const T &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res;
    res = a;
    res.operator-=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator-(const vector<T, SIZE> &a, const T &b)
{
    vector<T, SIZE> res = a;
    res.operator-=(b);
    return res;
}

template <typename T, const unsigned int SIZE>
vector<T, SIZE> operator*(const vector<T, SIZE> &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res = a;
    res.operator*=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator*(const T &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res;
    res = a;
    res.operator*=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator*(const vector<T, SIZE> &a, const T &b)
{
    vector<T, SIZE> res = a;
    res.operator*=(b);
    return res;
}

template <typename T, const unsigned int SIZE>
vector<T, SIZE> operator/(const vector<T, SIZE> &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res = a;
    res.operator/=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator/(const T &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res;
    res = a;
    res.operator/=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator/(const vector<T, SIZE> &a, const T &b)
{
    vector<T, SIZE> res = a;
    res.operator/=(b);
    return res;
}

template <typename T, const unsigned int SIZE>
vector<T, SIZE> operator%(const vector<T, SIZE> &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res = a;
    res.operator%=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator%(const T &a, const vector<T, SIZE> &b)
{
    vector<T, SIZE> res;
    res = a;
    res.operator%=(b);
    return res;
}

template <typename T, const unsigned int SIZE> vector<T, SIZE> operator%(const vector<T, SIZE> &a, const T &b)
{
    vector<T, SIZE> res = a;
    res.operator%=(b);
    return res;
}

template <typename T, const unsigned int SIZE> float dot(const vector<T, SIZE> &a, const vector<T, SIZE> &b)
{
    float sum = 0.0;
    for (unsigned int i = 0; i < SIZE; i++)
        sum += a[i] * b[i];
    return sum;
}

template <typename T> vector<T, 3> operator^(const vector<T, 3> &u, const vector<T, 3> &v)
{
    vector<T, 3> res;
    res[0] = u[1] * v[2] - u[2] * v[1];
    res[1] = u[2] * v[0] - u[0] * v[2];
    res[2] = u[0] * v[1] - u[1] * v[0];
    return res;
}

vector<float, 2> dir(const float angle)
{
    return {cos(angle), sin(angle)};
}

} // namespace math

#endif
