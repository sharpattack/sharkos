#ifndef MATH_MATRIX_INCLUDED
#define MATH_MATRIX_INCLUDED

#include <etl/sqrt.h>
#include <etl/type_traits.h>

#include <cmath>
#include <initializer_list>
#include <type_traits>

#include "vector.hpp"

namespace math
{

template <typename T, const unsigned int COL, const unsigned int ROW> class matrix
{
  private:
    T value[COL][ROW];

    template <typename U, const unsigned int U_COL, const unsigned int U_ROW> class const_matrix_col
    {
      public:
        const_matrix_col(const matrix<U, U_COL, U_ROW> *mat, int col_index) : mat(mat), col_index(col_index)
        {
        }

        const U &operator[](int row) const
        {
            assert(0 <= row && row < (int)U_ROW);
            return mat->value[col_index][row];
        }

      private:
        const matrix<U, U_COL, U_ROW> *mat;
        int col_index;
    };

    template <typename U, const unsigned int U_COL, const unsigned int U_ROW> class matrix_col
    {
      public:
        matrix_col(matrix<U, U_COL, U_ROW> *mat, int col_index) : mat(mat), col_index(col_index)
        {
        }

        U &operator[](int row)
        {
            assert(0 <= row && row < (int)U_ROW);
            return mat->value[col_index][row];
        }

      private:
        matrix<U, U_COL, U_ROW> *mat;
        int col_index;
    };

  public:
    matrix() = default;

    explicit matrix(const T init_value)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] = init_value;
            }
        }
    }

    matrix(std::initializer_list<std::initializer_list<T>> init_value)
    {
        assert(init_value.size() == ROW);

        unsigned int i = 0;
        for (auto row : init_value)
        {
            assert(row.size() == COL);
            unsigned int j = 0;
            for (auto v : row)
            {
                value[j++][i] = v;
            }
            i++;
        }
    }

    matrix<T, COL, ROW> &operator=(const matrix<T, COL, ROW> &a)
    {
        if (this == &a)
        {
            return *this;
        }

        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] = a.get(c, r);
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator=(const T a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] = a;
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator+=(const matrix<T, COL, ROW> &a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] += a.get(c, r);
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator+=(const T a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] += a;
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator-=(const matrix<T, COL, ROW> &a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] -= a.get(c, r);
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator-=(const T a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] -= a;
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator*=(const T a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] *= a;
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator/=(const T a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] /= a;
            }
        }
        return *this;
    }

    matrix<T, COL, ROW> &operator%=(const int a)
    {
        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                value[c][r] %= a;
            }
        }
        return *this;
    }

    matrix<T, ROW, COL> t()
    {
        matrix<T, ROW, COL> res;

        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                res.get(r, c) = value[c][r];
            }
        }

        return res;
    }

    [[nodiscard]] matrix<int, ROW, COL> to_int() const
    {
        matrix<int, ROW, COL> res;

        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                res.get(r, c) = (int)value[c][r];
            }
        }

        return res;
    }

    [[nodiscard]] matrix<float, ROW, COL> to_float() const
    {
        matrix<float, ROW, COL> res;

        for (unsigned int c = 0; c < COL; c++)
        {
            for (unsigned int r = 0; r < ROW; r++)
            {
                res.get(r, c) = (float)value[c][r];
            }
        }

        return res;
    }

    T scalar()
    {
        static_assert(COL == 1 && ROW == 1);
        return value[0][0];
    }

    [[nodiscard]] inline unsigned int cols() const
    {
        return COL;
    }

    [[nodiscard]] inline unsigned int rows() const
    {
        return ROW;
    }

    T &get(int col, int row)
    {
        assert(0 <= col && col < (int)COL && 0 <= row && row < (int)ROW);
        return value[col][row];
    }

    [[nodiscard]] const T &get(int col, int row) const
    {
        assert(0 <= col && col < (int)COL && 0 <= row && row < (int)ROW);
        return value[col][row];
    }

    matrix_col<T, COL, ROW> operator[](int col)
    {
        assert(0 <= col && col < (int)COL);
        return matrix_col<T, COL, ROW>(this, col);
    }

    const_matrix_col<T, COL, ROW> operator[](int col) const
    {
        assert(0 <= col && col < (int)COL);
        return const_matrix_col<T, COL, ROW>(this, col);
    }
};

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator+(const matrix<T, COL, ROW> &a, const matrix<T, COL, ROW> &b)
{
    matrix<T, COL, ROW> res;
    res = a;
    res += b;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator+(const matrix<T, COL, ROW> &a, const T b)
{
    matrix<T, COL, ROW> res;
    res = a;
    res += b;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator+(const T a, const matrix<T, COL, ROW> b)
{
    matrix<T, COL, ROW> res;
    res = b;
    res += a;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator-(const matrix<T, COL, ROW> &a, const matrix<T, COL, ROW> &b)
{
    matrix<T, COL, ROW> res;
    res = a;
    res -= b;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator-(const matrix<T, COL, ROW> &a, const T b)
{
    matrix<T, COL, ROW> res;
    res = a;
    res -= b;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator-(const T a, const matrix<T, COL, ROW> b)
{
    matrix<T, COL, ROW> res;
    res = b;
    res -= a;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int SHARED, const unsigned int ROW>
matrix<T, COL, ROW> operator*(const matrix<T, SHARED, ROW> &a, const matrix<T, COL, SHARED> &b)
{
    matrix<T, COL, ROW> res;
    for (unsigned int c = 0; c < COL; c++)
    {
        for (unsigned int r = 0; r < ROW; r++)
        {
            T sum = 0;
            for (unsigned int s = 0; s < SHARED; s++)
            {
                sum += a.get(s, r) * b.get(c, s);
            }
            res.get(c, r) = sum;
        }
    }
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
vector<T, ROW> operator*(const matrix<T, COL, ROW> &a, const vector<T, COL> &b)
{
    vector<T, ROW> res;
    for (unsigned int r = 0; r < ROW; r++)
    {
        T sum = 0;
        for (unsigned int c = 0; c < COL; c++)
        {
            sum += a.get(c, r) * b[c];
        }
        res[r] = sum;
    }
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
vector<T, COL> operator*(const vector<T, ROW> &a, const matrix<T, COL, ROW> &b)
{
    vector<T, COL> res;
    for (unsigned int c = 0; c < COL; c++)
    {
        T sum = 0;
        for (unsigned int r = 0; r < ROW; r++)
        {
            sum += a[r] * b.get(c, r);
        }
        res[c] = sum;
    }
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator*(const matrix<T, COL, ROW> &a, const T b)
{
    matrix<T, COL, ROW> res;
    res = a;
    res *= b;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator*(const T a, const matrix<T, COL, ROW> b)
{
    matrix<T, COL, ROW> res;
    res = b;
    res *= a;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator/(const matrix<T, COL, ROW> &a, const T b)
{
    matrix<T, COL, ROW> res;
    res = a;
    res /= b;
    return res;
}

template <typename T, const unsigned int COL, const unsigned int ROW>
matrix<T, COL, ROW> operator%(const matrix<T, COL, ROW> &a, const T b)
{
    matrix<T, COL, ROW> res;
    res = a;
    res %= b;
    return res;
}

template <typename T, const unsigned int SIZE> static matrix<T, SIZE, SIZE> I()
{
    matrix<T, SIZE, SIZE> res;

    for (unsigned int c = 0; c < SIZE; c++)
    {
        for (unsigned int r = 0; r < SIZE; r++)
        {
            res.get(c, r) = (c == r) ? 1 : 0;
        }
    }

    return res;
}

static matrix<float, 2, 2> rot(float angle)
{
    float c = cos(angle), s = sin(angle);
    matrix<float, 2, 2> res{{c, -s}, {s, c}};

    return res;
}

static matrix<float, 3, 3> rot(vector<float, 3> &axis, float angle)
{
    vector<float, 3> u{};
    float c = cos(angle), s = sin(angle);

    u = axis.normalize();

    matrix<float, 3, 3> res{{u[0] * u[0] * ((float)1.0 - c) + c, u[0] * u[1] * ((float)1.0 - c) - u[2] * s,
                             u[0] * u[2] * ((float)1.0 - c) + u[1] * s},
                            {u[0] * u[1] * ((float)1.0 - c) + u[2] * s, u[1] * u[1] * ((float)1.0 - c) + c,
                             u[1] * u[2] * ((float)1.0 - c) - u[0] * s},
                            {u[0] * u[2] * ((float)1.0 - c) - u[1] * s, u[1] * u[2] * ((float)1.0 - c) + u[0] * s,
                             u[2] * u[2] * ((float)1.0 - c) + c}};

    return res;
}

template <typename T> T det(matrix<T, 3, 3> &mat)
{
    T a = mat.get(0, 0), b = mat.get(1, 0), c = mat.get(2, 0);
    T d = mat.get(0, 1), e = mat.get(1, 1), f = mat.get(2, 1);
    T g = mat.get(0, 2), h = mat.get(1, 2), i = mat.get(2, 2);
    return a * e * i + b * f * g + c * d * h - c * e * g - b * d * i - a * f * h;
}

template <typename T> T det(matrix<T, 2, 2> &mat)
{
    return mat.get(0, 0) * mat.get(1, 1) - mat.get(0, 1) * mat.get(1, 0);
}

template <typename T> T det(matrix<T, 1, 1> &mat)
{
    return mat.get(0, 0);
}
} // namespace math

#endif
