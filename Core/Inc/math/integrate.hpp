#ifndef MATH_INTEGRATE_INCLUDED
#define MATH_INTEGRATE_INCLUDED

#include <cmath>

#include "vector.hpp"

namespace math
{

#define GAUSS_LEGENDRE_LENGTH 12

typedef struct
{
    vector<float, 2> pos;
    float dir;
} position;

position integrate(vector<float, 2> speed, vector<float, 2> accel, float duration)
{
    static float points[12][2] = {{0.995187219997021f, 0.012341229799987f}, {0.974728555971309f, 0.028531388628933f},
                                  {0.938274552002732f, 0.044277438817419f}, {0.886415527004401f, 0.059298584915436f},
                                  {0.820001985973902f, 0.073346481411080f}, {0.740124191578554f, 0.086190161531953f},
                                  {0.648093651936975f, 0.097618652104113f}, {0.545421471388839f, 0.107444270115965f},
                                  {0.433793507626045f, 0.115505668053725f}, {0.315042679696163f, 0.121670472927803f},
                                  {0.191118867473616f, 0.125837456346828f}, {0.064056892862605f, 0.127938195346752f}};

    position pos = {vector<float, 2>(0.0f), 0.0f};

    int i;
    float left;
    float right;
    float t_trans;
    float t_square;
    float direction;
    float tangent_speed;

    float m_speed = 0.5f * (speed[0] + speed[1]);
    float m_accel = 0.5f * (accel[0] + accel[1]);

    for (i = 0; i < GAUSS_LEGENDRE_LENGTH; i++)
    {
        t_trans = duration * points[i][0];

        t_square = t_trans * t_trans;
        left = speed[0] * t_trans + 0.5f * accel[0] * t_square;
        right = speed[1] * t_trans + 0.5f * accel[1] * t_square;
        direction = right - left;

        tangent_speed = m_speed + m_accel * t_trans;

        pos.pos[0] += cos(direction) * tangent_speed * points[i][1] * duration;
        pos.pos[1] += sin(direction) * tangent_speed * points[i][1] * duration;
    }

    left = speed[0] * duration + 0.5f * accel[0] * duration * duration;
    right = speed[1] * duration + 0.5f * accel[1] * duration * duration;
    pos.dir = right - left;

    return pos;
}

} // namespace math

#endif
