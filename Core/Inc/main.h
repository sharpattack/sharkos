/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

    /* Private includes ----------------------------------------------------------*/
    /* USER CODE BEGIN Includes */

    /* USER CODE END Includes */

    /* Exported types ------------------------------------------------------------*/
    /* USER CODE BEGIN ET */

    /* USER CODE END ET */

    /* Exported constants --------------------------------------------------------*/
    /* USER CODE BEGIN EC */

    /* USER CODE END EC */

    /* Exported macro ------------------------------------------------------------*/
    /* USER CODE BEGIN EM */

    /* USER CODE END EM */

    /* Exported functions prototypes ---------------------------------------------*/
    void Error_Handler(void);

/* USER CODE BEGIN EFP */
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LOAD_24V_1_Pin GPIO_PIN_2
#define LOAD_24V_1_GPIO_Port GPIOE
#define TMC_STATUS_1_INT_3_Pin GPIO_PIN_3
#define TMC_STATUS_1_INT_3_GPIO_Port GPIOE
#define TMC_STATUS_1_INT_3_EXTI_IRQn EXTI3_IRQn
#define LOAD_24V_3_Pin GPIO_PIN_4
#define LOAD_24V_3_GPIO_Port GPIOE
#define TMC_EN_1_Pin GPIO_PIN_5
#define TMC_EN_1_GPIO_Port GPIOE
#define INT_6_Pin GPIO_PIN_6
#define INT_6_GPIO_Port GPIOE
#define INT_6_EXTI_IRQn EXTI9_5_IRQn
#define INT13_Pin GPIO_PIN_13
#define INT13_GPIO_Port GPIOC
#define INT13_EXTI_IRQn EXTI15_10_IRQn
#define INT_5_Pin GPIO_PIN_5
#define INT_5_GPIO_Port GPIOF
#define INT_5_EXTI_IRQn EXTI9_5_IRQn
#define ENC_A1_Pin GPIO_PIN_0
#define ENC_A1_GPIO_Port GPIOA
#define ENC_B1_Pin GPIO_PIN_1
#define ENC_B1_GPIO_Port GPIOA
#define USART2_TX_Pin GPIO_PIN_2
#define USART2_TX_GPIO_Port GPIOA
#define USART2_RX_Pin GPIO_PIN_3
#define USART2_RX_GPIO_Port GPIOA
#define TMC_STATUS_0_INT_4_Pin GPIO_PIN_4
#define TMC_STATUS_0_INT_4_GPIO_Port GPIOA
#define LOAD_12V_2_Pin GPIO_PIN_5
#define LOAD_12V_2_GPIO_Port GPIOC
#define LOAD_12V_3_Pin GPIO_PIN_0
#define LOAD_12V_3_GPIO_Port GPIOB
#define TMC_EN_0_Pin GPIO_PIN_1
#define TMC_EN_0_GPIO_Port GPIOB
#define LOAD_12V_1_Pin GPIO_PIN_2
#define LOAD_12V_1_GPIO_Port GPIOB
#define ENC_N1_INT_14_Pin GPIO_PIN_14
#define ENC_N1_INT_14_GPIO_Port GPIOF
#define ENC_N1_INT_14_EXTI_IRQn EXTI15_10_IRQn
#define USART3_DIR_Pin GPIO_PIN_15
#define USART3_DIR_GPIO_Port GPIOF
#define TMC_DRV_0_Pin GPIO_PIN_0
#define TMC_DRV_0_GPIO_Port GPIOG
#define TMC_CTRL_0_Pin GPIO_PIN_1
#define TMC_CTRL_0_GPIO_Port GPIOG
#define UART5_RX_Pin GPIO_PIN_7
#define UART5_RX_GPIO_Port GPIOE
#define UART5_TX_Pin GPIO_PIN_8
#define UART5_TX_GPIO_Port GPIOE
#define START_MATCH_INT_10_Pin GPIO_PIN_10
#define START_MATCH_INT_10_GPIO_Port GPIOE
#define START_MATCH_INT_10_EXTI_IRQn EXTI15_10_IRQn
#define MOTORCTL_TIM12_CH2_Pin GPIO_PIN_15
#define MOTORCTL_TIM12_CH2_GPIO_Port GPIOB
#define USART3_TX_Pin GPIO_PIN_8
#define USART3_TX_GPIO_Port GPIOD
#define USART3_RX_Pin GPIO_PIN_9
#define USART3_RX_GPIO_Port GPIOD
#define INT_11_Pin GPIO_PIN_11
#define INT_11_GPIO_Port GPIOD
#define INT_11_EXTI_IRQn EXTI15_10_IRQn
#define INT_12_Pin GPIO_PIN_12
#define INT_12_GPIO_Port GPIOD
#define INT_12_EXTI_IRQn EXTI15_10_IRQn
#define GPIO_DEBUG_4_Pin GPIO_PIN_3
#define GPIO_DEBUG_4_GPIO_Port GPIOG
#define GPIO_DEBUG_3_Pin GPIO_PIN_4
#define GPIO_DEBUG_3_GPIO_Port GPIOG
#define GPIO_DEBUG_2_Pin GPIO_PIN_5
#define GPIO_DEBUG_2_GPIO_Port GPIOG
#define GPIO_DEBUG_1_Pin GPIO_PIN_6
#define GPIO_DEBUG_1_GPIO_Port GPIOG
#define INT8_Pin GPIO_PIN_8
#define INT8_GPIO_Port GPIOG
#define INT8_EXTI_IRQn EXTI9_5_IRQn
#define INT7_Pin GPIO_PIN_7
#define INT7_GPIO_Port GPIOC
#define INT7_EXTI_IRQn EXTI9_5_IRQn
#define LT3960_EN_Pin GPIO_PIN_12
#define LT3960_EN_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define ENC_A0_Pin GPIO_PIN_15
#define ENC_A0_GPIO_Port GPIOA
#define UART4_TX_Pin GPIO_PIN_10
#define UART4_TX_GPIO_Port GPIOC
#define UART4_RX_Pin GPIO_PIN_11
#define UART4_RX_GPIO_Port GPIOC
#define TMC_CTRL_1_Pin GPIO_PIN_12
#define TMC_CTRL_1_GPIO_Port GPIOC
#define LED_DEBUG_4_Pin GPIO_PIN_0
#define LED_DEBUG_4_GPIO_Port GPIOD
#define LED_DEBUG_3_Pin GPIO_PIN_1
#define LED_DEBUG_3_GPIO_Port GPIOD
#define ENC_N1_INT_2_Pin GPIO_PIN_2
#define ENC_N1_INT_2_GPIO_Port GPIOD
#define LED_DEBUG_2_Pin GPIO_PIN_3
#define LED_DEBUG_2_GPIO_Port GPIOD
#define LED_DEBUG_1_Pin GPIO_PIN_4
#define LED_DEBUG_1_GPIO_Port GPIOD
#define TMC_DRV_1_Pin GPIO_PIN_5
#define TMC_DRV_1_GPIO_Port GPIOD
#define INT_9_Pin GPIO_PIN_9
#define INT_9_GPIO_Port GPIOG
#define INT_9_EXTI_IRQn EXTI9_5_IRQn
#define INT_15_Pin GPIO_PIN_15
#define INT_15_GPIO_Port GPIOG
#define INT_15_EXTI_IRQn EXTI15_10_IRQn
#define LOAD_24V_2_Pin GPIO_PIN_8
#define LOAD_24V_2_GPIO_Port GPIOB
#define ENC_B0_Pin GPIO_PIN_9
#define ENC_B0_GPIO_Port GPIOB
#define INT_0_Pin GPIO_PIN_0
#define INT_0_GPIO_Port GPIOE
#define INT_0_EXTI_IRQn EXTI0_IRQn
#define INT_1_Pin GPIO_PIN_1
#define INT_1_GPIO_Port GPIOE

    /* USER CODE BEGIN Private defines */

    /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
