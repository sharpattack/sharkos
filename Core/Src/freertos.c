/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "main.h"
#include "task.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "Tasks/task_canard.h"
#include "Tasks/task_ctrl.h"
#include "Tasks/task_dds.h"
#include "Tasks/task_dds_example.h"
#include "Tasks/task_led.h"
#include "Tasks/task_lidar.h"
#include "Tasks/task_pathfinding.h"
#include "Tasks/task_pathfinding_example.h"
#include "Tasks/task_strat.h"
#include "Tasks/task_surimi.h"
#include "config.h"
#include "event_groups.h"
#include "gpio.h"
#include "i2c.h"
#include "logs.h"
#include "profile.h"
#include "usart.h"
#include <cmsis_os2.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define STRINGIFY(x) #x
#define TASK_HELPER(task_name, buffer_size, task_priority)                                                             \
    osThreadId_t task_name##TaskHandle;                                                                                \
    uint32_t task_name##TaskBuffer[buffer_size];                                                                       \
    osStaticThreadDef_t task_name##TaskControlBlock;                                                                   \
    const osThreadAttr_t task_name##Task_attributes = {                                                                \
        .name = STRINGIFY(task_name##Task),                                                                            \
        .cb_mem = &task_name##TaskControlBlock,                                                                        \
        .cb_size = sizeof(task_name##TaskControlBlock),                                                                \
        .stack_mem = &task_name##TaskBuffer[0],                                                                        \
        .stack_size = sizeof(task_name##TaskBuffer),                                                                   \
        .priority = (osPriority_t)(task_priority),                                                                     \
    }

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* Definitions for LogTask */
const struct vLEDTaskParameters LEDTaskParameters = {.gpio_port = LED_DEBUG_1_GPIO_Port, .gpio_pin = LED_DEBUG_1_Pin};

/* Definition of canard task */
TASK_HELPER(Canard, 1024, osPriorityNormal);

/* Definitions for DDSTask */
TASK_HELPER(DDS, 1024, osPriorityNormal);
const struct vDDSTaskParameters DDSTaskParameters = {.selected_uart = &huart4};

/* Definition of surimi task */
TASK_HELPER(Surimi, 1024, osPriorityNormal);

/* Definitions for StratTask */
TASK_HELPER(Strat, 1024, osPriorityNormal);

/* Definition of Control task */
StaticEventGroup_t ctrl_event_group;
EventGroupHandle_t ctrl_event_handle;

TASK_HELPER(Ctrl, 1024, osPriorityRealtime);

/* Definition of PathFinding task */
StaticEventGroup_t pathfinding_event_group;
EventGroupHandle_t pathfinding_event_handle;

/* TASK_HELPER(PathFinding, 1 << 13, osPriorityLow1); */

/* Definition of Lidar task */
TASK_HELPER(Lidar, 1024, osPriorityNormal);

/* Others */
#ifdef BUILD_DDS_PUBLISH_EXAMPLE
TASK_HELPER(HelloWorldPublisher, 512, osPriorityLow);
#endif

#ifdef BUILD_DDS_SUBSCRIBE_EXAMPLE
TASK_HELPER(HelloWorldSubscriber, 768, osPriorityLow);
#endif

#ifdef BUILD_PATHFINDING_EXAMPLE
TASK_HELPER(PathFindingExample, 256, osPriorityLow1);
#endif

/* Definitions for LedTask */
/* USER CODE END Variables */
/* Definitions for LedTask */
osThreadId_t LedTaskHandle;
uint32_t LedTaskBuffer[128];
osStaticThreadDef_t LedTaskControlBlock;
const osThreadAttr_t LedTask_attributes = {
    .name = "LedTask",
    .cb_mem = &LedTaskControlBlock,
    .cb_size = sizeof(LedTaskControlBlock),
    .stack_mem = &LedTaskBuffer[0],
    .stack_size = sizeof(LedTaskBuffer),
    .priority = (osPriority_t)osPriorityLow,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartLedTask(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
    /* Run time stack overflow checking is performed if
     configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
     called if a stack overflow is detected. */
    log_fatal_error("STACK OVERFLOW", xTask, pcTaskName);
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void)
{
    /* vApplicationMallocFailedHook() will only be called if
     configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
     function that will get called if a call to pvPortMalloc() fails.
     pvPortMalloc() is called internally by the kernel whenever a task, queue,
     timer or semaphore is created. It is also called by various parts of the
     demo application. If heap_1.c or heap_2.c are used, then the size of the
     heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
     FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
     to query the size of free heap space that remains (although it does not
     provide information on how the remaining heap might be fragmented). */
    log_fatal_error("MALLOC FAILED", NULL, NULL);
}
/* USER CODE END 5 */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void)
{
    /* USER CODE BEGIN Init */
    SEGGER_SYSVIEW_Conf();
    init_logs();

    /* USER CODE END Init */

    /* USER CODE BEGIN RTOS_MUTEX */
    /* add mutexes, ... */
    /* USER CODE END RTOS_MUTEX */

    /* USER CODE BEGIN RTOS_SEMAPHORES */
    /* add semaphores, ... */
    /* USER CODE END RTOS_SEMAPHORES */

    /* USER CODE BEGIN RTOS_TIMERS */
    /* start timers, add new ones, ... */
    /* USER CODE END RTOS_TIMERS */

    /* USER CODE BEGIN RTOS_QUEUES */
    /* USER CODE END RTOS_QUEUES */

    /* Create the thread(s) */
    /* creation of LedTask */
    LedTaskHandle = osThreadNew(StartLedTask, (void *)&LEDTaskParameters, &LedTask_attributes);

    /* USER CODE BEGIN RTOS_THREADS */
    StratTaskHandle = osThreadNew(StartStratTask, NULL, &StratTask_attributes);

    DDSTaskHandle = osThreadNew(StartDDSTask, (void *)&DDSTaskParameters, &DDSTask_attributes);
    CanardTaskHandle = osThreadNew(StartCanardTask, NULL, &CanardTask_attributes);
    SurimiTaskHandle = osThreadNew(StartSurimiTask, NULL, &SurimiTask_attributes);
    LidarTaskHandle = osThreadNew(StartLidarTask, NULL, &LidarTask_attributes);
    /* PathFindingTaskHandle = osThreadNew(StartPathFindingTask, NULL, &PathFindingTask_attributes); */
    CtrlTaskHandle = osThreadNew(startCtrlTask, NULL, &CtrlTask_attributes);

#ifdef BUILD_PATHFINDING_EXAMPLE
    PathFindingExampleTaskHandle = osThreadNew(StartPathFindingExampleTask, NULL, &PathFindingExampleTask_attributes);
#endif

#ifdef BUILD_DDS_PUBLISH_EXAMPLE
    HelloWorldPublisherTaskHandle =
        osThreadNew(StartHelloWorldPublisherTask, NULL, &HelloWorldPublisherTask_attributes);
#endif

#ifdef BUILD_DDS_SUBSCRIBE_EXAMPLE
    HelloWorldSubscriberTaskHandle =
        osThreadNew(StartHelloWorldSubscriberTask, NULL, &HelloWorldSubscriberTask_attributes);
#endif
    /* USER CODE END RTOS_THREADS */

    /* USER CODE BEGIN RTOS_EVENTS */
    /* add events, ... */
    pathfinding_event_handle = xEventGroupCreateStatic(&pathfinding_event_group);
    xEventGroupSetBits(pathfinding_event_handle, 0);

    ctrl_event_handle = xEventGroupCreateStatic(&ctrl_event_group);
    xEventGroupSetBits(ctrl_event_handle, 0);

    /* USER CODE END RTOS_EVENTS */
}

/* USER CODE BEGIN Header_StartLedTask */
/**
 * @brief  Function implementing the LedTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartLedTask */
void StartLedTask(void *argument)
{
    /* USER CODE BEGIN StartLedTask */
    vStartLedTask(argument);
    /* Infinite loop */
    for (;;)
    {
        osDelay(1);
    }
    /* USER CODE END StartLedTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    LOG_DEBUG("Receiving interrupt from pin %d", GPIO_Pin);

    ctrlrTask_GPIO_Interrupt_Handler(GPIO_Pin);
}
/* USER CODE END Application */
