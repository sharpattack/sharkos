#include "SEGGER/SEGGER_RTT.h"
#include "embd_string.h"
#include "critical_error_banner.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <welcome_banner.h>

#include "cmsis_os2.h"
#include "logs.h"
#include "portmacro.h"
#include "printf.h"
#include "projdefs.h"
#include "queue.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_def.h"
#include "stm32f4xx_hal_uart.h"

static char _RawUpBuffer[SEGGER_SYSVIEW_RTT_BUFFER_SIZE];

/* Stolen from FreeRTOS/Source/tasks.c */
typedef struct tskTaskControlBlock /* The old naming convention is used to prevent breaking kernel aware debuggers. */
{
    volatile StackType_t *pxTopOfStack; /*< Points to the location of the last item placed on the tasks stack.  THIS
                                           MUST BE THE FIRST MEMBER OF THE TCB STRUCT. */
#if (portUSING_MPU_WRAPPERS == 1)
    xMPU_SETTINGS xMPUSettings; /*< The MPU settings are defined as part of the port layer.  THIS MUST BE THE SECOND
                                   MEMBER OF THE TCB STRUCT. */
#endif
    ListItem_t xStateListItem;  /*< The list that the state list item of a task is reference from denotes the state of
                                   that task (Ready, Blocked, Suspended ). */
    ListItem_t xEventListItem;  /*< Used to reference a task from an event list. */
    UBaseType_t uxPriority;     /*< The priority of the task.  0 is the lowest priority. */
    StackType_t *pxStack;       /*< Points to the start of the stack. */
    char pcTaskName[configMAX_TASK_NAME_LEN];
    /*< Descriptive name given to the task when created.  Facilitates debugging only. */ /*lint !e971 Unqualified
                                                                                            char types are allowed
                                                                                            for strings and single
                                                                                            characters only. */

#if ((portSTACK_GROWTH > 0) || (configRECORD_STACK_HIGH_ADDRESS == 1))
    StackType_t *pxEndOfStack; /*< Points to the highest valid address for the stack. */
#endif
#if (portCRITICAL_NESTING_IN_TCB == 1)
    UBaseType_t uxCriticalNesting; /*< Holds the critical section nesting depth for ports that do not maintain their own
                                      count in the port layer. */
#endif
#if (configUSE_TRACE_FACILITY == 1)
    UBaseType_t uxTCBNumber;  /*< Stores a number that increments each time a TCB is created.  It allows debuggers to
                                 determine when a task has been deleted and then recreated. */
    UBaseType_t uxTaskNumber; /*< Stores a number specifically for use by third party trace code. */
#endif
#if (configUSE_MUTEXES == 1)
    UBaseType_t
        uxBasePriority; /*< The priority last assigned to the task - used by the priority inheritance mechanism. */
    UBaseType_t uxMutexesHeld;
#endif
#if (configUSE_APPLICATION_TASK_TAG == 1)
    TaskHookFunction_t pxTaskTag;
#endif
#if (configNUM_THREAD_LOCAL_STORAGE_POINTERS > 0)
    void *pvThreadLocalStoragePointers[configNUM_THREAD_LOCAL_STORAGE_POINTERS];
#endif
#if (configGENERATE_RUN_TIME_STATS == 1)
    uint32_t ulRunTimeCounter; /*< Stores the amount of time the task has spent in the Running state. */
#endif
#if (configUSE_NEWLIB_REENTRANT == 1)
    struct _reent xNewLib_reent;
#endif
#if (configUSE_TASK_NOTIFICATIONS == 1)
    volatile uint32_t ulNotifiedValue;
    volatile uint8_t ucNotifyState;
#endif
#if (tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE !=                                                                      \
     0)                            /*lint !e731 !e9029 Macro has been consolidated for readability reasons. */
    uint8_t ucStaticallyAllocated; /*< Set to pdTRUE if the task is a statically allocated to ensure no attempt is made
                                      to free the memory. */
#endif
#if (INCLUDE_xTaskAbortDelay == 1)
    uint8_t ucDelayAborted;
#endif
#if (configUSE_POSIX_ERRNO == 1)
    int iTaskErrno;
#endif
} tskTCB;
typedef tskTCB *xTaskHandle;

void _print_banner(const uint8_t *banner, const size_t banner_size, TickType_t timeout)
{
    /* This will print a banner chunk by chunk */
    TickType_t start_time = HAL_GetTick();
    size_t written = 0;
    while (written < banner_size)
    {
        if (HAL_GetTick() - start_time > timeout)
        {
            return;
        }
        size_t to_write =
            fmin((double)(banner_size - written), (double)SEGGER_RTT_GetAvailWriteSpace(LOGS_RTT_CHANNEL));
        SEGGER_RTT_Write(LOGS_RTT_CHANNEL, banner + written, to_write);
        written += to_write;
    }
    HAL_Delay(100); /* Make sure there was time for the banner to be read by the JLink if present */
}

void log_fatal_error(const char *type, xTaskHandle task, signed char *pcTaskName)
{
    portENABLE_INTERRUPTS();
    /* Print banner */
    _print_banner(critical_error_banner, sizeof(critical_error_banner), 100);
    /* Print custom message */
    SEGGER_RTT_printf(LOGS_RTT_CHANNEL, "Hai! Hai! Hai! You got a fatal error: %s", type);

    /* Print more info */
    if (pcTaskName != NULL)
    {
        SEGGER_RTT_printf(LOGS_RTT_CHANNEL, "The culprit is: %s", pcTaskName);
        SEGGER_RTT_printf(LOGS_RTT_CHANNEL, "Last location: 0x%p", (void *)&(task->pxTopOfStack));
        SEGGER_RTT_printf(LOGS_RTT_CHANNEL, "Priority: %lu", task->uxPriority);
        SEGGER_RTT_printf(LOGS_RTT_CHANNEL, "Allocated dynamically: %s",
                          (task->ucStaticallyAllocated == pdTRUE) ? "true" : "false");
        SEGGER_RTT_printf(LOGS_RTT_CHANNEL, "Task state: %d", eTaskGetState(task));
    }
    taskDISABLE_INTERRUPTS();
    for (;;)
    {
        ;
    }
}

void init_logs()
{

    /* SEGGER_RTT_Init() is called by SEGGER_SYSVIEW_Conf */
    SEGGER_RTT_AllocUpBuffer("RawLogs", &_RawUpBuffer[0], sizeof(_RawUpBuffer), SEGGER_RTT_MODE_NO_BLOCK_SKIP);
    SEGGER_RTT_ConfigUpBuffer(LOGS_RTT_CHANNEL_RAW, "RawLogs", &_RawUpBuffer[0], sizeof(_RawUpBuffer),
                              SEGGER_RTT_MODE_NO_BLOCK_SKIP);
    _print_banner(welcome_banner, sizeof(welcome_banner), 100);
    LOG_INFO("-- Init LOG Task");
    LOG_INFO("-- LOG Task initialized");
}
