#include "shared_state.h"
#include "logs.h"
#include <math.h>
#include <stdbool.h>

SharedState shared_state;

SharedState *get_shared_state()
{
    static bool is_initialized = false;
    if (is_initialized)
        return &shared_state;

    is_initialized = true;

    /* Initializes proximity sensors related vars */
    ProximitySensors *proximity_sensors = &shared_state.proximity_sensors;
    proximity_sensors->available_sensors = 0;
    /*for (uint8_t i = 0; i < NUMBER_OF_SENSORS; i++)
    {
        proximity_sensors->sensor[i].is_available = false;
        proximity_sensors->sensor[i].distance = -1.0f;
    }*/

    /*Initialize lidar vars*/
    shared_state.lidar_sensor.distance = INFINITY;
    shared_state.lidar_sensor.an_object_is_close = false;
    for (uint16_t i = 0; i < LIDAR_SENSOR_PROXIMITY_ZONES_NB; i++)
    {
        shared_state.lidar_sensor.proximity[i] = INFINITY;
    }
    shared_state.lidar_sensor.pose_estim_is_valid = false;
    shared_state.lidar_sensor.disabled_by_strat = false;

    shared_state.side = UNKNOWN_SIDE;

    return &shared_state;
}

void set_shared_odometry(Odometry *odometry)
{
    taskENTER_CRITICAL();

    shared_state.odometry.x = odometry->x;
    shared_state.odometry.y = odometry->y;
    shared_state.odometry.theta = odometry->theta;

    taskEXIT_CRITICAL();
}

Odometry get_shared_odometry()
{
    Odometry odometry;

    taskENTER_CRITICAL();

    odometry.x = shared_state.odometry.x;
    odometry.y = shared_state.odometry.y;
    odometry.theta = shared_state.odometry.theta;

    taskEXIT_CRITICAL();

    return odometry;
}

LidarSensor get_shared_lidar_sensor()
{
    LidarSensor lidar_sensor;

    taskENTER_CRITICAL();

    if (shared_state.lidar_sensor.disabled_by_strat)
    {
        lidar_sensor.an_object_is_close = false;
        lidar_sensor.distance = INFINITY;
        for (int i = 0; i < LIDAR_SENSOR_PROXIMITY_ZONES_NB; i++)
        {
            lidar_sensor.proximity[i] = INFINITY;
        }
    }
    else
    {
        lidar_sensor.distance = shared_state.lidar_sensor.distance;
        lidar_sensor.an_object_is_close = shared_state.lidar_sensor.an_object_is_close;
        lidar_sensor.pose_estim_is_valid = shared_state.lidar_sensor.pose_estim_is_valid;
        lidar_sensor.pose_estim_x = shared_state.lidar_sensor.pose_estim_x;
        lidar_sensor.pose_estim_y = shared_state.lidar_sensor.pose_estim_y;
        lidar_sensor.pose_estim_theta = shared_state.lidar_sensor.pose_estim_theta;
        for (int i = 0; i < LIDAR_SENSOR_PROXIMITY_ZONES_NB; i++)
        {
            lidar_sensor.proximity[i] = shared_state.lidar_sensor.proximity[i];
        }
    }
    taskEXIT_CRITICAL();

    return lidar_sensor;
}

void strat_disable_shared_lidar_sensor()
{
    taskENTER_CRITICAL();
    shared_state.lidar_sensor.disabled_by_strat = true;
    taskEXIT_CRITICAL();
}

void strat_enable_shared_lidar_sensor()
{
    taskENTER_CRITICAL();
    shared_state.lidar_sensor.disabled_by_strat = false;
    taskEXIT_CRITICAL();
}

void set_shared_side(Side side)
{
    if (side == UNKNOWN_SIDE)
    {
        LOG_WARNING("SharedState: trying to set side to unknown. Are you sure you're supposed to do that ?");
    }

    taskENTER_CRITICAL();
    shared_state.side = side;
    taskEXIT_CRITICAL();
}

Side get_shared_side()
{
    return shared_state.side;
}
