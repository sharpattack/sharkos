#include "Vision/vision.h"
#include "FreeRTOSConfig.h"
#include "tmc/helpers/Macros.h"
#include "Tasks/task_dds.h"
#include "logs.h"
#include "vision_objects.h"
#include <tmc/helpers/Types.h>
#include <math.h>
#include <projdefs.h>
#include <stdint.h>
#include <string.h>

DDS_PREPARE_SUBSCRIBE_TOPIC(VisionInfo);
PRIV_DDS_INIT_TOPIC(VisionInfo, 1);

bool dds_initialized = false;

void maybe_init_dds_subscribe()
{
    if (!dds_initialized)
    {
        LOG_INFO("vision.c: creating VisionInfo topic")
        PRIV_DDS_INIT_SUBSCRIBE_TOPIC(VisionInfo, 1, LAST_ONLY);
        dds_initialized = true;
    }
}

u8 search_vision_object(VisionObject *out_objects, u8 buffer_size, const char *searched_type, u8 max_tries_nb)
{
    maybe_init_dds_subscribe();

    const uint16_t WAIT_BETWEEN_TRIES_MS = 10;

    for (int i = 0; i < max_tries_nb; i++)
    {
        VisionInfo vision_info;
        vision_info.objectsNb = 0;
        int res = DDS_RETRIEVE_TOPIC_TIMEOUT(VisionInfo, &vision_info, 50);

        // discard old readings
        float local_timestamp_s = GetDDSTimestamp() / 1000000000.0;
        float delta_s = fabsf(local_timestamp_s - vision_info.acquisitionTimestamp);
        if (delta_s > 1.0)
        {
            res = pdFALSE;
        }

        if (res == pdFALSE)
        {
            continue;
        }

        u8 objectsNb = 0;
        for (int j = 0; j < MIN(vision_info.objectsNb, buffer_size); j++)
        {
            VisionObject object = vision_info.objects[j];
            if (strcmp(object.type, searched_type) == 0)
            {
                out_objects[j] = object;
                objectsNb += 1;
            }
        }

        if (objectsNb > 0)
        {
            return objectsNb;
        }

        sleep(WAIT_BETWEEN_TRIES_MS);
    }

    return 0;
}
