#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "logs.h"
#include "task.h"
#include "usart.h"
#include <time.h>

#include "serial_transport_external.h"
#include "uxr/client/profile/transport/custom/custom_transport.h"
#include "uxr/client/util/time.h"

#define UART_DMA_BUFFER_SIZE 256

static uint8_t dma_buffer[UART_DMA_BUFFER_SIZE];
static size_t dma_head = 0, dma_tail = 0;

static void DMA_stop_read(UART_HandleTypeDef *huart);

bool my_custom_transport_open(uxrExternalSerialTransport *transport)
{
    // UART initialisation is already done by FreeRTOS
    return true;
}

bool my_custom_transport_close(uxrExternalSerialTransport *transport)
{

    // We never close the serial port
    HAL_UART_DMAStop(transport->args);
    return true;
}

size_t my_custom_transport_write(void *args, const uint8_t *buf, size_t len, uint8_t *errcode)
{
    UART_HandleTypeDef *uartHandle = (UART_HandleTypeDef *)args;
    HAL_StatusTypeDef ret;
    size_t ullDataWritten = 0;

    *errcode = HAL_OK;

    if (uartHandle->gState == HAL_UART_STATE_READY)
    {
        ret = HAL_UART_Transmit_DMA(uartHandle, (uint8_t *)buf, len);

        if (ret != HAL_OK)
        {
            *errcode = ret;
        }
        else
        {
            while ((uartHandle->gState != HAL_UART_STATE_READY) && (uartHandle->gState != HAL_UART_STATE_ERROR))
            {
                sleep(1);
            }

            if (uartHandle->gState == HAL_UART_STATE_ERROR)
            {
                *errcode = HAL_ERROR;
            }

            ullDataWritten = len - __HAL_DMA_GET_COUNTER(uartHandle->hdmatx);
        }
    }

    if (*errcode != HAL_OK)
    {
        LOG_ERROR("SERIAL - %s error: %d", __FUNCTION__, *errcode);
    }
    if (ullDataWritten < len)
    {
        LOG_WARNING("SERIAL - %s Write incomplete: %d/%d", __FUNCTION__, ullDataWritten, len);
    }

    return ullDataWritten;
}

size_t my_custom_transport_read(void *args, uint8_t *buf, size_t len, int timeout, uint8_t *errcode)
{
    UART_HandleTypeDef *uartHandle = (UART_HandleTypeDef *)args;
    size_t wrote = 0;
    const TickType_t ulTime = xTaskGetTickCount();

    *errcode = HAL_OK;

    if (uartHandle->gState == HAL_UART_STATE_READY)
    {
        HAL_StatusTypeDef ret = HAL_UART_Receive_DMA(uartHandle, dma_buffer, UART_DMA_BUFFER_SIZE);
        if (ret != HAL_OK && ret != HAL_BUSY)
        {
            *errcode = ret;
            goto SafeExit;
        }
    }

    __disable_irq();
    dma_tail = UART_DMA_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(uartHandle->hdmarx);
    __enable_irq();

    while ((dma_head != dma_tail) && (wrote < len))
    {
        buf[wrote] = dma_buffer[dma_head];
        dma_head = (dma_head + 1) % UART_DMA_BUFFER_SIZE;
        wrote++;

        if ((int)((xTaskGetTickCount() - ulTime) / portTICK_PERIOD_MS) > timeout)
        {
            DMA_stop_read(uartHandle);
            *errcode = HAL_TIMEOUT;
            LOG_WARNING("SERIAL - %s timeout", __FUNCTION__);
            break;
        }
    }

SafeExit:
    if (*errcode != HAL_OK)
    {
        LOG_ERROR("SERIAL - %s error: %d", __FUNCTION__, *errcode);
    }

    return wrote;
}

// Code vole depuis Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c
static void DMA_stop_read(UART_HandleTypeDef *huart)
{
    /* Stop UART DMA Rx request if ongoing */
    const uint32_t dmarequest = HAL_IS_BIT_SET(huart->Instance->CR3, USART_CR3_DMAR);
    if ((huart->RxState == HAL_UART_STATE_BUSY_RX) && dmarequest)
    {
        ATOMIC_CLEAR_BIT(huart->Instance->CR3, USART_CR3_DMAR);

        /* Abort the UART DMA Rx stream */
        if (huart->hdmarx != NULL)
        {
            HAL_DMA_Abort(huart->hdmarx);
        }

        /* Disable RXNE, PE and ERR (Frame error, noise error, overrun error) interrupts */
        ATOMIC_CLEAR_BIT(huart->Instance->CR1, (USART_CR1_RXNEIE | USART_CR1_PEIE));
        ATOMIC_CLEAR_BIT(huart->Instance->CR3, USART_CR3_EIE);

        /* In case of reception waiting for IDLE event, disable also the IDLE IE interrupt source */
        if (huart->ReceptionType == HAL_UART_RECEPTION_TOIDLE)
        {
            ATOMIC_CLEAR_BIT(huart->Instance->CR1, USART_CR1_IDLEIE);
        }

        /* At end of Rx process, restore huart->RxState to Ready */
        huart->RxState = HAL_UART_STATE_READY;
        huart->ReceptionType = HAL_UART_RECEPTION_STANDARD;
    }
}
