#ifndef SIM_FREERTOS_H
#define SIM_FREERTOS_H

#ifdef __cplusplus
extern "C"
{
#endif

    void MX_FREERTOS_Init(void);

#ifdef __cplusplus
}
#endif

#endif