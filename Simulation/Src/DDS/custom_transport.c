#include "uxr/client/profile/transport/custom/custom_transport.h"
#include "serial_transport_external.h"
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 12345

int sockfd;

bool my_custom_transport_open(uxrExternalSerialTransport *transport)
{
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        return false;
    }

    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr);

    if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        close(sockfd);
        return false;
    }

    return true;
}

bool my_custom_transport_close(uxrExternalSerialTransport *transport)
{
    if (close(sockfd) < 0)
    {
        return false;
    }
    return true;
}

size_t my_custom_transport_write(void *args, const uint8_t *buf, size_t len, uint8_t *errcode)
{
    ssize_t bytes_sent = send(sockfd, buf, len, 0);
    if (bytes_sent < 0)
    {
        *errcode = errno;
        return 0;
    }
    return (size_t)bytes_sent;
}

size_t my_custom_transport_read(void *args, uint8_t *buf, size_t len, int timeout, uint8_t *errcode)
{
    fd_set read_fds;
    struct timeval tv;
    FD_ZERO(&read_fds);
    FD_SET(sockfd, &read_fds);

    tv.tv_sec = timeout / 1000;
    tv.tv_usec = (timeout % 1000) * 1000;

    int retval = select(sockfd + 1, &read_fds, NULL, NULL, &tv);
    if (retval == -1)
    {
        *errcode = errno;
        return 0;
    }
    else if (retval == 0)
    {
        *errcode = ETIMEDOUT;
        return 0;
    }

    ssize_t bytes_received = recv(sockfd, buf, len, 0);
    if (bytes_received < 0)
    {
        *errcode = errno;
        return 0;
    }
    return (size_t)bytes_received;
}
