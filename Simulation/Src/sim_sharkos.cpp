// Standard includes
#include <csignal> // Handle Signint
#include <iostream>
#include <pthread.h> // Pthread stack min
#include <unistd.h>  // Exit program

// SharkOS
#include "FreeRTOS.h"
#include "sim_freertos.h"
#include "task.h" // FreeRTOS

void handle_signint(int signal)
{
    exit(2);
}

int main()
{
    signal(SIGINT, handle_signint);
    std::cout << "Init SharkOS" << std::endl;
    // Init FreeRTOS
    MX_FREERTOS_Init();
    vTaskStartScheduler();

    // It shouldn't reach this loop
    while (1)
    {
    }
}
