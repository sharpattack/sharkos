#include "abs_motor.h"

void abs_motor_init(uint8_t motor)
{
}

void abs_motor_state_machine_step(uint8_t motor)
{
}

void abs_motor_set_torque(uint8_t motor, float torque)
{
}

void abs_motor_set_speed(uint8_t motor, float speed)
{
}
