#include "sim_freertos.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "Tasks/task_ctrl.h"
#include "event_groups.h" // FreeRTOS
#include "helpers.h"      // Simulation
#include "task.h"
#include <pthread.h>

#include <stdio.h>  // DEBUG
#include <stdlib.h> // DEBUG

/* When configSUPPORT_STATIC_ALLOCATION is set to 1 the application writer can
 * use a callback function to optionally provide the memory required by the idle
 * and timer tasks.  This is the stack that will be used by the timer task.  It is
 * declared here, as a global, so it can be checked by a test that is implemented
 * in a different file. */

StackType_t uxTimerTaskStack[configTIMER_TASK_STACK_DEPTH];

#define STRINGIFY(x) #x
#define TASK_HELPER(task_name, stack_depth)                                                                            \
    TaskHandle_t task_name##TaskHandle;                                                                                \
    StackType_t task_name##StackBuffer[stack_depth];                                                                   \
    StaticTask_t task_name##TaskControlBlock;                                                                          \
    const configSTACK_DEPTH_TYPE task_name##StackSize = sizeof(task_name##StackBuffer) / sizeof(StackType_t);

TASK_HELPER(ctrl, 1024)

StaticEventGroup_t ctrl_event_group;
EventGroupHandle_t ctrl_event_handle;

// Test task
void start_test_task(void *argument)
{
    while (1)
    {
        printf("Hello from test task\n");
        vTaskDelay(1000);
    }
}

void MX_FREERTOS_Init(void)
{
    // Task creation
    ctrlTaskHandle = xTaskCreateStatic(start_test_task, "testTask", ctrlStackSize, NULL, osPriorityNormal,
                                       ctrlStackBuffer, &ctrlTaskControlBlock);
    // xTaskCreate(startCtrlTask, "ctrlTask", 1024, NULL, osPriorityNormal, NULL);
    // xTaskCreate(StartStratTask, "stratTask", 1024, NULL, osPriorityNormal, NULL);

    // Event group inits
    ctrl_event_handle = xEventGroupCreateStatic(&ctrl_event_group);
    xEventGroupSetBits(ctrl_event_handle, 0);
}

void vApplicationMallocFailedHook(void)
{
    /* vApplicationMallocFailedHook() will only be called if
     * configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
     * function that will get called if a call to pvPortMalloc() fails.
     * pvPortMalloc() is called internally by the kernel whenever a task, queue,
     * timer or semaphore is created.  It is also called by various parts of the
     * demo application.  If heap_1.c, heap_2.c or heap_4.c is being used, then the
     * size of the    heap available to pvPortMalloc() is defined by
     * configTOTAL_HEAP_SIZE in FreeRTOSConfig.h, and the xPortGetFreeHeapSize()
     * API function can be used to query the size of free heap space that remains
     * (although it does not provide information on how the remaining heap might be
     * fragmented).  See http://www.freertos.org/a00111.html for more
     * information. */
    vAssertCalled(__FILE__, __LINE__);
}

/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{
    (void)pcTaskName;
    (void)pxTask;

    /* Run time stack overflow checking is performed if
     * configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
     * function is called if a stack overflow is detected.  This function is
     * provided as an example only as stack overflow checking does not function
     * when running the FreeRTOS POSIX port. */
    vAssertCalled(__FILE__, __LINE__);
}

/*-----------------------------------------------------------*/

void vApplicationTickHook(void)
{
    /* This function will be called by each tick interrupt if
     * configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
     * added here, but the tick hook is called from an interrupt context, so
     * code must not attempt to block, and only the interrupt safe FreeRTOS API
     * functions can be used (those that end in FromISR()). */
}

/*-----------------------------------------------------------*/

void vApplicationDaemonTaskStartupHook(void)
{
    /* This function will be called once only, when the daemon task starts to
     * execute (sometimes called the timer task).  This is useful if the
     * application includes initialisation code that would benefit from executing
     * after the scheduler has been started. */
}

/*-----------------------------------------------------------*/

void vAssertCalled(const char *const pcFileName, unsigned long ulLine)
{
    static BaseType_t xPrinted = pdFALSE;
    volatile uint32_t ulSetToNonZeroInDebuggerToContinue = 0;

    /* Called if an assertion passed to configASSERT() fails.  See
     * https://www.FreeRTOS.org/a00110.html#configASSERT for more information. */

    /* Parameters are not used. */
    (void)ulLine;
    (void)pcFileName;

    taskENTER_CRITICAL();
    {
        /* Stop the trace recording. */
        if (xPrinted == pdFALSE)
        {
            xPrinted = pdTRUE;

#if (projENABLE_TRACING == 1)
            {
                prvSaveTraceFile();
            }
#endif /* if ( projENABLE_TRACING == 0 ) */
        }

        /* You can step out of this function to debug the assertion by using
         * the debugger to set ulSetToNonZeroInDebuggerToContinue to a non-zero
         * value. */
        while (ulSetToNonZeroInDebuggerToContinue == 0)
        {
            __asm volatile("NOP");
            __asm volatile("NOP");
        }
    }
    taskEXIT_CRITICAL();
}

/*-----------------------------------------------------------*/

/* configUSE_STATIC_ALLOCATION is set to 1, so the application must provide an
 * implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
 * used by the Idle task. */
void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer,
                                   configSTACK_DEPTH_TYPE *pulIdleTaskStackSize)
{
    /* If the buffers to be provided to the Idle task are declared inside this
     * function then they must be declared static - otherwise they will be allocated on
     * the stack and so not exists after this function exits. */
    static StaticTask_t xIdleTaskTCB;
    static StackType_t uxIdleTaskStack[configMINIMAL_STACK_SIZE];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
     * state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task's stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
     * Note that, as the array is necessarily of type StackType_t,
     * configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

/*-----------------------------------------------------------*/

/* configUSE_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
 * application must provide an implementation of vApplicationGetTimerTaskMemory()
 * to provide the memory that is used by the Timer service task. */
void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer,
                                    configSTACK_DEPTH_TYPE *pulTimerTaskStackSize)
{
    /* If the buffers to be provided to the Timer task are declared inside this
     * function then they must be declared static - otherwise they will be allocated on
     * the stack and so not exists after this function exits. */
    static StaticTask_t xTimerTaskTCB;

    /* Pass out a pointer to the StaticTask_t structure in which the Timer
     * task's state will be stored. */
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

    /* Pass out the array that will be used as the Timer task's stack. */
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;

    /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
     * Note that, as the array is necessarily of type StackType_t,
     * configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}

/*-----------------------------------------------------------*/

static uint32_t ulEntryTime = 0;

void vTraceTimerReset(void)
{
    ulEntryTime = xTaskGetTickCount();
}

/*-----------------------------------------------------------*/

uint32_t uiTraceTimerGetFrequency(void)
{
    return configTICK_RATE_HZ;
}

/*-----------------------------------------------------------*/

uint32_t uiTraceTimerGetValue(void)
{
    return (xTaskGetTickCount() - ulEntryTime);
}

/*-----------------------------------------------------------*/
