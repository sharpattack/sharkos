#!/bin/bash

if [ $# -lt 4 -o $# -gt 4 ]; then
  echo 1>&2 "Usage: $0 <user> <address> <openocd-config> <file-to-flash>"
  exit 2
fi

user=$1
address=$2

openocd_config_path=$3
file_to_flash_path=$4

openocd_config_file=$(basename ${openocd_config_path})
file_to_flash_file=$(basename ${file_to_flash_path})

# Create persistant connection and prepare remote folder
ssh ${user}@${address} -fNMS ~/.ssh/sharkos.socket 'rm -rf /tmp/remote-flash'
ssh ${user}@${address} -S ~/.ssh/sharkos.socket "mkdir -p /tmp/remote-flash"

# Send files
scp -o 'ControlPath ~/.ssh/sharkos.socket' $openocd_config_path ${user}@${address}:/tmp/remote-flash/$openocd_config_file
scp -o 'ControlPath ~/.ssh/sharkos.socket' $file_to_flash_path ${user}@${address}:/tmp/remote-flash/$file_to_flash_file

# Flash
ssh ${user}@${address} -S ~/.ssh/sharkos.socket "openocd -f /tmp/remote-flash/$openocd_config_file -c 'program /tmp/remote-flash/$file_to_flash_file verify reset exit'"

# Delete remote folder
ssh ${user}@${address} -S ~/.ssh/sharkos.socket 'rm -rf /tmp/remote-flash'

# Close connection
ssh ${user}@${address} -S ~/.ssh/sharkos.socket -O exit
